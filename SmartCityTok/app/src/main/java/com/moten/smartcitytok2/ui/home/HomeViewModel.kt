package com.moten.smartcitytok2.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class HomeViewModel : ViewModel() {

	private val _text = MutableLiveData<String>().apply {
		value = "当前地理位置城市名称"
	}
	val text: LiveData<String> = _text
}