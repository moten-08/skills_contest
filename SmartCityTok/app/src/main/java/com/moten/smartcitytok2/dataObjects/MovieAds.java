package com.moten.smartcitytok2.dataObjects;

import com.moten.smartcitytok2.functionActivity.ThisMovieActivity;

import java.io.Serializable;
import java.util.List;

public class MovieAds implements Serializable {

	public int total;
	public List<RowsDTO> rows;
	public int code;
	public String msg;

	public static class RowsDTO implements Serializable{
		public Object searchValue;
		public String createBy;
		public String createTime;
		public String updateBy;
		public String updateTime;
		public Object remark;
		public ParamsDTO params;
		public int id;
		public String appType;
		public String status;
		public int sort;
		public String advTitle;
		public String advImg;
		public String servModule;
		public int targetId;
		public String type;

		@Override
		public String toString() {
			return "\nRowsDTO{" +
					"\nsearchValue=" + searchValue +
					",\ncreateBy='" + createBy + '\'' +
					",\ncreateTime='" + createTime + '\'' +
					",\nupdateBy='" + updateBy + '\'' +
					",\nupdateTime='" + updateTime + '\'' +
					",\nremark=" + remark +
					",\nparams=" + params +
					",\nid=" + id +
					",\nappType='" + appType + '\'' +
					",\nstatus='" + status + '\'' +
					",\nsort=" + sort +
					",\nadvTitle='" + advTitle + '\'' +
					",\nadvImg='" + advImg + '\'' +
					",\nservModule='" + servModule + '\'' +
					",\ntargetId=" + targetId +
					",\ntype='" + type + '\'' +
					'}';
		}

		public static class ParamsDTO {
		}

	}

	@Override
	public String toString() {
		return "MovieAds{" +
				"total=" + total +
				", rows=" + rows +
				", code=" + code +
				", msg='" + msg + '\'' +
				'}';
	}
}
