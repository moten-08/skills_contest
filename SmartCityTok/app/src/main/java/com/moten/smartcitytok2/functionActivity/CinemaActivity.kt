package com.moten.smartcitytok2.functionActivity

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.moten.smartcitytok2.R

class CinemaActivity : AppCompatActivity() {
	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_cinema)
		title = "影院详情"
		supportActionBar!!.setDisplayHomeAsUpEnabled(true)

	}

	override fun onOptionsItemSelected(item: MenuItem): Boolean {
		when (item.itemId) {
			android.R.id.home -> finish()
		}
		return false
	}
}