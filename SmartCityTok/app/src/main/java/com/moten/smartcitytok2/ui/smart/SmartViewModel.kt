package com.moten.smartcitytok2.ui.smart

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class SmartViewModel : ViewModel() {

	private val _text = MutableLiveData<String>().apply {
		value = "This is smart Fragment"
	}
	val text: LiveData<String> = _text
}