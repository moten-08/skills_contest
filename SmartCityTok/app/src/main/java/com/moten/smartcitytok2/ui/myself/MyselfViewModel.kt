package com.moten.smartcitytok2.ui.myself

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class MyselfViewModel : ViewModel() {

	private val _text = MutableLiveData<String>().apply {
		value = "This is myself Fragment"
	}
	val text: LiveData<String> = _text
}