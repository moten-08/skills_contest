package com.moten.smartcitytok2.dataObjects;

import java.util.List;

public class Theatre {

	public int total;
	public List<RowsDTO> rows;
	public int code;
	public String msg;

	public static class RowsDTO {
		public Object searchValue;
		public Object createBy;
		public Object createTime;
		public Object updateBy;
		public Object updateTime;
		public Object remark;
		public ParamsDTO params;
		public int id;
		public int theaterId;
		public int roomId;
		public int movieId;
		public String startTime;
		public String endTime;
		public double price;
		public String playType;
		public String playDate;
		public int saleNum;
		public int totalNum;
		public String status;
		public String theatreName;
		public String roomName;
		public String movieName;

		public static class ParamsDTO {
		}
	}
}
