package com.moten.smartcitytok2.ui.movie

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class MovieViewModel : ViewModel() {

	private val _text = MutableLiveData<String>().apply {
		value = "This is movie Fragment"
	}
	val text: LiveData<String> = _text
}