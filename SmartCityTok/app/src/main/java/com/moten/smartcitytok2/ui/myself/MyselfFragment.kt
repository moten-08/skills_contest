package com.moten.smartcitytok2.ui.myself

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.moten.smartcitytok2.R

class MyselfFragment : Fragment() {

	private lateinit var myselfViewModel: MyselfViewModel

	override fun onCreateView(
		inflater: LayoutInflater,
		container: ViewGroup?,
		savedInstanceState: Bundle?
	): View? {
		ViewModelProvider(this).get(MyselfViewModel::class.java).also { myselfViewModel = it }
		val root = inflater.inflate(R.layout.fragment_myself, container, false)
		val textView: TextView = root.findViewById(R.id.text_myself)
		myselfViewModel.text.observe(viewLifecycleOwner, Observer {
			textView.text = it
		})
		return root
	}
}