package com.moten.smartcitytok2.functionActivity

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.moten.smartcitytok2.R
import com.moten.smartcitytok2.adapters.SearchAdapter
import com.moten.smartcitytok2.dataObjects.Movie
import com.moten.smartcitytok2.databinding.ActivitySearchListAcvivityBinding
import com.moten.smartcitytok2.functions.Tool
import kotlin.concurrent.thread

class SearchListActivity : AppCompatActivity() {
	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		val binding = ActivitySearchListAcvivityBinding.inflate(layoutInflater)
		setContentView(binding.root)
		supportActionBar?.setDisplayHomeAsUpEnabled(true)
		supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_baseline_keyboard_arrow_left_24)
		title = "搜索结果"
		binding.searchList.layoutManager = GridLayoutManager(this,3)
		var searchText = intent.getStringExtra("searchText")
		thread {
			var list: MutableList<Movie.RowsDTO> = Tool.getHotMovie(searchText)
			runOnUiThread {
				binding.searchList.adapter = SearchAdapter(this, list)
			}
		}
	}

	override fun onOptionsItemSelected(item: MenuItem): Boolean {
		when (item.itemId) {
			android.R.id.home -> finish()
		}
		return false
	}
}