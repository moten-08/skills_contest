package com.moten.smartcitytok2.dataObjects;

import java.io.Serializable;

public class MovieFilm {

	public String msg;
	public int code;
	public DataDTO data;

	public static class DataDTO implements Serializable {
		public Object searchValue;
		public Object createBy;
		public Object createTime;
		public Object updateBy;
		public Object updateTime;
		public Object remark;
		public ParamsDTO params;
		public int id;
		public String name;
		public String category;
		public String cover;
		public String playType;
		public int score;
		public int duration;
		public String playDate;
		public int likeNum;
		public int favoriteNum;
		public String language;
		public String introduction;
		public Object other;
		public String recommend;

		public static class ParamsDTO implements Serializable{
		}
	}

}
