package com.moten.smartcitytok2.dataObjects;

import java.io.Serializable;
import java.util.List;

public class Movie implements Serializable{
	// 电影:热映与将映的集合
	public int total;
	public List<RowsDTO> rows;
	public int code;
	public String msg;

	public static class RowsDTO implements Serializable {
		public Object searchValue;
		public Object createBy;
		public Object createTime;
		public Object updateBy;
		public Object updateTime;
		public Object remark;
		public ParamsDTO params;
		public int id;
		public String name;
		public String category;/**/
		public String cover;
		public String playType;
		public int score;/**/
		public int duration;/**/
		public String playDate;/**/
		public int likeNum;/**/
		public int favoriteNum;/**/
		public String language;/**/
		public String introduction;
		public Object other;/**/
		public String recommend;/**/
		public String video;/**/

		@Override
		public String toString(){
			return "RowsDTO{" +
					"searchValue=" + searchValue +
					", createBy=" + createBy +
					", createTime=" + createTime +
					", updateBy=" + updateBy +
					", updateTime=" + updateTime +
					", remark=" + remark +
					", params=" + params +
					", id=" + id +
					", name='" + name + '\'' +
					", category='" + category + '\'' +
					", cover='" + cover + '\'' +
					", playType='" + playType + '\'' +
					", score=" + score +
					", duration=" + duration +
					", playDate='" + playDate + '\'' +
					", likeNum=" + likeNum +
					", favoriteNum=" + favoriteNum +
					", language='" + language + '\'' +
					", introduction='" + introduction + '\'' +
					", other=" + other +
					", recommend='" + recommend + '\'' +
					", video='" + video + '\'' +
					'}';
		}

		public static class ParamsDTO implements Serializable{
		}
	}


}
