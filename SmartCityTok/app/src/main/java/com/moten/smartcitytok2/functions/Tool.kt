package com.moten.smartcitytok2.functions

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.res.Resources
import android.util.Log
import android.view.View
import android.webkit.WebView
import android.widget.Button
import android.widget.RatingBar
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.moten.smartcitytok2.MainActivity
import com.moten.smartcitytok2.R
import com.moten.smartcitytok2.dataObjects.*
import com.moten.smartcitytok2.functionActivity.FirstActivity
import com.moten.smartcitytok2.functionActivity.ThisMovieActivity
import okhttp3.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody
import java.io.IOException

object Tool {
	const val urlHeader: String = "http://222.207.204.15:10001"
	const val tokenHeader: String = "Bearer "
	private val client = OkHttpClient.Builder().build()
	lateinit var sharedPreferences: SharedPreferences
	lateinit var editor: SharedPreferences.Editor

	// 获取屏幕宽高
	val width = Resources.getSystem().displayMetrics.widthPixels
	val height = Resources.getSystem().displayMetrics.widthPixels

	// 获取地理位置
	fun getGps(view: View) {
		// 正确操作应该是获取当前经纬值，然后通过api返回地理位置
		val request = Request.Builder()
			.url("${urlHeader}/prod-api/api/common/gps/location")
			.get()
			.build()
		val call = client.newCall(request)
		try {
			val response: Response = call.execute()
			val data = response.body!!.string()
			val result: GpsLocation = Gson().fromJson(data, GpsLocation::class.java)
			val gps = result.data.province + result.data.city + result.data.area + result.data.location
			(view.context as Activity).runOnUiThread {
				view.findViewById<TextView>(R.id.text_home).text = gps
			}
		} catch (e: Exception) {
			e.printStackTrace()
		}

	}

	// 登录
	fun postLogin(userName: String, password: String, context: Context) {
		sharedPreferences = context.getSharedPreferences("location", Context.MODE_PRIVATE)
		val body: RequestBody =
			"{\"username\":\"${userName}\",\"password\":\"${password}\"}"
				.toRequestBody("application/json".toMediaTypeOrNull())
		val request = Request.Builder()
			.url("${urlHeader}/prod-api/api/login")
			.post(body)
			.build()
		val call = client.newCall(request)
		call.enqueue(object : Callback {
			override fun onFailure(call: Call, e: IOException) {
				e.printStackTrace()
			}

			override fun onResponse(call: Call, response: Response) {
				val data = response.body!!.string()
				val result = Gson().fromJson(data, LoginResult::class.java)
				(context as Activity).runOnUiThread {
					editor = sharedPreferences.edit()
					if (result.code != 200) {
						Toast.makeText(context, result.msg, Toast.LENGTH_SHORT).show()
					} else {
						editor.putString("userName", userName)
						editor.putString("Authorization", tokenHeader + result.token)
						context.finish()
						context.startActivity(Intent(context, MainActivity::class.java))
					}
					editor.commit()
				}
			}

		})
	}

	// 注册
	fun postRegister(
		context: Context,
		userName: String,
		password: String,
		phoneNumber: String,
		sex: String,
		nikeName: String? = null,
		email: String? = null,
		idCard: String? = null
	) {
		val string = "{\"userName\":\"${userName}\"," +
				"\"password\":\"${password}\"," +
				"\"phonenumber\":\"${phoneNumber}\"," +
				"\"sex\":\"${sex}\"," +
				"\"nikeName\":\"${nikeName}\"," +
				"\"email\":\"${email}\"," +
				"\"idCard\":\"${idCard}\"" +
				"}"
		val body = string.toRequestBody("application/json".toMediaTypeOrNull())
		val request = Request.Builder()
			.url("${urlHeader}/prod-api/api/register")
			.post(body)
			.build()
		val call = client.newCall(request)
		call.enqueue(object : Callback {
			override fun onFailure(call: Call, e: IOException) {
				e.printStackTrace()
			}

			override fun onResponse(call: Call, response: Response) {
				val data = response.body!!.string()
				val result = Gson().fromJson(data, RegisterResult::class.java)
				(context as Activity).runOnUiThread {
					Toast.makeText(context, "${result.msg}", Toast.LENGTH_SHORT).show()
				}
			}

		})
	}

	// 查询广告列表
	fun getMovieRotation(): MutableList<MovieAds.RowsDTO> {
		val movies: MutableList<MovieAds.RowsDTO> = ArrayList()
		val request = Request.Builder()
			.url("${urlHeader}/prod-api/api/movie/rotation/list")
			.get()
			.build()
		val call = client.newCall(request)
		try {
			val response: Response = call.execute()
			val data = response.body!!.string()
			val movie: MovieAds = Gson().fromJson(data, MovieAds::class.java)
			movies.addAll(movie.rows)
		} catch (e: Exception) {
			e.printStackTrace()
		}
		return movies
	}

	// 查询影片详情
	fun getMovieFilm(id: Int, token: String, context: Context, type: String?) :MovieFilm?{
		var dataDTO :MovieFilm?=null
		val url =
			if (type == "hot")
				"${urlHeader}/prod-api/api/movie/film/${id}"
			else
				"${urlHeader}/prod-api/api/movie/film/preview/${id}"
		val request = Request.Builder()
			.url(url)
			.get()
			.addHeader("Authorization", "Bearer $token")
			.build()
		val call = client.newCall(request)
		val response = call.execute()
		val data = response.body?.string()
		val film = Gson().fromJson(data, MovieFilm::class.java)
		if (film.code == 200) {
			dataDTO = film
		} else {
			// 请求失败
			sharedPreferences = context.getSharedPreferences("location", Context.MODE_PRIVATE)
			editor = sharedPreferences.edit()
			editor.remove("Authorization")
			editor.commit()
			context.startActivity(Intent(context, FirstActivity::class.java))
		}
		return dataDTO
	}

	// 查询今天天气
	fun getWeather(): WeatherForToday.DataDTO {
		var weather = WeatherForToday()
		val request = Request.Builder()
			.url("${urlHeader}/prod-api/api/common/weather/today")
			.get()
			.build()
		val call = client.newCall(request)
		try {
			val response = call.execute()
			val data1 = response.body?.string()
			weather = Gson().fromJson(data1, WeatherForToday::class.java)
		} catch (e: Exception) {
			e.printStackTrace()
		}
		return weather.data
	}

	// 热映电影列表
	fun getHotMovie(name:String?=""): MutableList<Movie.RowsDTO> {
		val movie: MutableList<Movie.RowsDTO> = ArrayList()
		val request = Request.Builder()
			.url("${urlHeader}/prod-api/api/movie/film/list?name=${name}")
			.get()
			.build()
		val call = client.newCall(request)
		try {
			val response = call.execute()
			val data = response.body!!.string()
			val movie1: Movie = Gson().fromJson(data, Movie::class.java)
			movie.addAll(movie1.rows)
		} catch (e: Exception) {
			e.printStackTrace()
		}
		return movie
	}

	// 即将上映电影列表
	fun getNoticeMovie(): MutableList<Movie.RowsDTO> {
		val movie: MutableList<Movie.RowsDTO> = ArrayList()
		val request = Request.Builder()
			.url("${urlHeader}/prod-api/api/movie/film/preview/list")
			.get()
			.build()
		val call = client.newCall(request)
		try {
			val response = call.execute()
			val data = response.body!!.string()
			val movie1: Movie = Gson().fromJson(data, Movie::class.java)
			movie.addAll(movie1.rows)
		} catch (e: Exception) {
			e.printStackTrace()
		}
		return movie
	}

	// 附近电影院
	fun getNearCinema(): MutableList<NearCinema.RowsDTO> {
		val nearCinema: MutableList<NearCinema.RowsDTO> = ArrayList()
		val request = Request.Builder()
			.url("${urlHeader}/prod-api/api/movie/theatre/list")
			.get()
			.build()
		val call = client.newCall(request)
		try {
			val response = call.execute()
			val data = response.body!!.string()
			val cinema: NearCinema = Gson().fromJson(data, NearCinema::class.java)
			nearCinema.addAll(cinema.rows)
		} catch (e: Exception) {
			e.printStackTrace()
		}
		return nearCinema
	}

	// 获取评论列表
	fun getCommentList(movieName: String = "", id: Int? = null): MutableList<CommingList.RowsDTO> {
		val url =
			if (id != null) {
				"/prod-api/api/movie/film/comment/list?movieId=${id}"
			} else {
				"/prod-api/api/movie/film/comment/list?movieName=${movieName}"
			}
		val list: MutableList<CommingList.RowsDTO> = ArrayList()
		val request = Request.Builder()
			.url(urlHeader + url)
			.get()
			.build()
		val call = client.newCall(request)
		try {
			val response = call.execute()
			val data = response.body!!.string()
			val comming: CommingList = Gson().fromJson(data, CommingList::class.java)
			list.addAll(comming.rows)
		} catch (e: Exception) {
			e.printStackTrace()
		}
		return list
	}

	// 添加评论
	fun postComment(str: String, movieId: Int, score: String, activity: ThisMovieActivity) {
		val string = "{" +
				"\"content\":\"${str}\"," +
				"\"movieId\":\"${movieId}\"," +
				"\"score\":\"${score}\"" +
				"}"
		val body = string.toRequestBody("application/json".toMediaTypeOrNull())
		val token = sharedPreferences.getString("Authorization", "none")
		val request = Request.Builder()
			.url("${urlHeader}/prod-api/api/movie/film/comment")
			.post(body)
			.addHeader("Authorization", "Bearer $token")
			.build()
		val call = client.newCall(request)
		call.enqueue(object : Callback {
			override fun onFailure(call: Call, e: IOException) {
				e.printStackTrace()
			}

			override fun onResponse(call: Call, response: Response) {
				val data = response.body!!.string()
				val result: CommingList = Gson().fromJson(data, CommingList::class.java)
				activity.list.clear()
				activity.list.addAll(getCommentList(id = movieId))
				activity.runOnUiThread {
					if (result.code == 200) {
						// 成功
						activity.findViewById<TextView>(R.id.movie_comment_total).text = "评论数：${activity.list.size}"
						activity.findViewById<RecyclerView>(R.id.coming_list).adapter!!.notifyDataSetChanged()
					}
					Toast.makeText(activity, result.msg, Toast.LENGTH_SHORT).show()
				}

			}

		})
	}

	// 评论点赞
	fun postLikeComment(id: Int, context: Context, type: String, movieId: Int, movieName: String) {
		val token = sharedPreferences.getString("Authorization", "none")
		val request = Request.Builder()
			.url("${urlHeader}/prod-api/api/movie/film/comment/like/$id")
			.post("".toRequestBody("application/json".toMediaTypeOrNull()))
			.addHeader("Authorization", "Bearer $token")
			.build()
		val call = client.newCall(request)
		call.enqueue(object : Callback {
			override fun onFailure(call: Call, e: IOException) {
				e.printStackTrace()
			}

			override fun onResponse(call: Call, response: Response) {
				val data = response.body!!.string()
				val result: CommingList = Gson().fromJson(data, CommingList::class.java)
				var activity = context as ThisMovieActivity
				updateCommentList(activity, type, movieId, movieName)
			}

		})
	}

	// 更新评论列表
	private fun updateCommentList(activity: ThisMovieActivity, type: String, movieId: Int, movieName: String) {
		activity.list.clear()
		activity.list.addAll(
			if (type == "hot") {
				getCommentList(id = movieId)
			} else {
				getCommentList(movieName = movieName)
			}
		)
		activity.runOnUiThread {
			activity.findViewById<RecyclerView>(R.id.coming_list).adapter!!.notifyDataSetChanged()
		}

	}

	// 查询影片场次
	fun getTheatreTimes(id: Int, dateTime: String): MutableList<Theatre.RowsDTO> {
		val list: MutableList<Theatre.RowsDTO> = ArrayList()
		val url = "/prod-api/api/movie/theatre/times/list?movieId=${id}&playDate=$dateTime"
		val request = Request.Builder()
			.url(urlHeader + url)
			.get()
			.build()
		val call = client.newCall(request)
		val response = call.execute()
		val data = response.body!!.string()
		val result = Gson().fromJson(data, Theatre::class.java)
		list.addAll(result.rows)
		return list
	}

	// 查询影院详情
	fun getTheCiname(id: Int): String {
		val url = "/prod-api/api/movie/theatre/$id"
		val request = Request.Builder()
			.url(urlHeader + url)
			.get()
			.build()
		val call = client.newCall(request)
		val response = call.execute()
		val data = response.body!!.string()
		var result = Gson().fromJson(data, CinameOne::class.java)
		return result.data.province + result.data.city + result.data.area + result.data.address
	}
}