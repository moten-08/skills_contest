package com.moten.smartcitytok2.dataObjects;

public class LoginResult {

	public String msg;
	public int code;
	public String token;

	@Override
	public String toString() {
		return "\nLoginResult{" +
				"msg='" + msg + '\'' +
				",code=" + code +
				",token='" + token + '\'' +
				'}';
	}
}
