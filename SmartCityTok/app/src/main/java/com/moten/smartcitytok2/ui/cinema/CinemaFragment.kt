package com.moten.smartcitytok2.ui.cinema

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.moten.smartcitytok2.R

class CinemaFragment : Fragment() {

	private lateinit var cinemaViewModel: CinemaViewModel

	override fun onCreateView(
		inflater: LayoutInflater,
		container: ViewGroup?,
		savedInstanceState: Bundle?
	): View? {
		cinemaViewModel =
			ViewModelProvider(this).get(CinemaViewModel::class.java)
		val root = inflater.inflate(R.layout.fragment_cinema, container, false)
		val textView: TextView = root.findViewById(R.id.text_cinema)
		cinemaViewModel.text.observe(viewLifecycleOwner, Observer {
			textView.text = it
		})
		return root
	}
}