package com.moten.smartcitytok2.ui.movie

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.moten.smartcitytok2.R

class MovieFragment : Fragment() {

	private lateinit var movieViewModel: MovieViewModel

	override fun onCreateView(
		inflater: LayoutInflater,
		container: ViewGroup?,
		savedInstanceState: Bundle?
	): View? {
		movieViewModel =
			ViewModelProvider(this).get(MovieViewModel::class.java)
		val root = inflater.inflate(R.layout.fragment_movie, container, false)
		val textView: TextView = root.findViewById(R.id.text_movie)
		movieViewModel.text.observe(viewLifecycleOwner, Observer {
			textView.text = it
		})
		return root
	}
}