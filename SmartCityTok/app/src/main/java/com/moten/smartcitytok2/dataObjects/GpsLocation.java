package com.moten.smartcitytok2.dataObjects;

public class GpsLocation {

	public String msg;
	public int code;
	public DataDTO data;

	public static class DataDTO {
		public String area;
		public String province;
		public String city;
		public String location;
	}
}
