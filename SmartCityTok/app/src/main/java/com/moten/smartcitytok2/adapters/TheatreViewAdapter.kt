package com.moten.smartcitytok2.adapters

import android.content.Context
import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.PagerAdapter

class TheatreViewAdapter(var context: Context, var list: MutableList<View>) : PagerAdapter() {

	override fun getCount(): Int {
		return list.size
	}

	override fun isViewFromObject(view: View, `object`: Any): Boolean {
		return view == `object`
	}

	override fun instantiateItem(container: ViewGroup, position: Int): Any {
		val view = list[position]
		container.addView(view)
		return view
	}

	override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
		if (list.size > 0) {
			container.removeView(list[position])
		}
	}
}