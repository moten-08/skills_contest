package com.moten.smartcitytok2.adapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.moten.smartcitytok2.R
import com.moten.smartcitytok2.dataObjects.Movie
import com.moten.smartcitytok2.functionActivity.BuyActivity
import com.moten.smartcitytok2.functionActivity.ThisMovieActivity
import com.moten.smartcitytok2.functions.Tool

class SearchAdapter(
	val context:Context,
	val list: MutableList<Movie.RowsDTO>
) : RecyclerView.Adapter<SearchAdapter.ViewHolder>() {
	class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
		val cardView: CardView = itemView as CardView
		val movieCover: ImageView = itemView.findViewById(R.id.movie_cover)
		val movieTitle: TextView = itemView.findViewById(R.id.movie_title)
		val movieSeen: Button = itemView.findViewById(R.id.movie_seen)
	}

	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
		val view = LayoutInflater.from(context).inflate(R.layout.item_movie,parent,false)
		return ViewHolder(view)
	}

	override fun onBindViewHolder(holder: ViewHolder, position: Int) {
		var row: Movie.RowsDTO = list[position]
		Glide.with(context)
			.load(Tool.urlHeader+row.cover)
			.error(R.drawable.ic_baseline_error_24)
			.into(holder.movieCover)
		holder.movieTitle.text = row.name
		holder.movieSeen.setOnClickListener {
			val intent = Intent(context,BuyActivity::class.java)
			intent.putExtra("movieId",row.id)
			intent.putExtra("type","hot")
			context.startActivity(intent)
		}
		holder.cardView.setOnClickListener {
			val intent = Intent(context, ThisMovieActivity::class.java)
			intent.putExtra("type", "hot")
			intent.putExtra("movieId", row.id)
			intent.putExtra("movieName", row.name)
			context.startActivity(intent)
		}
	}

	override fun getItemCount(): Int {
		return list.size
	}
}