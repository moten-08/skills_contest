package com.moten.smartcitytok2.functionActivity

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.moten.smartcitytok2.databinding.ActivityGuideBinding
import com.moten.smartcitytok2.functions.Tool


class GuideActivity : AppCompatActivity() {
	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		var binding:ActivityGuideBinding = ActivityGuideBinding.inflate(layoutInflater)
		setContentView(binding.root)

		Glide.with(this).load("${Tool.urlHeader}/prod-api/profile/upload/image/2021/05/06/d2aeef1a-7c47-46bc-8534-20b3d14cd8eb.png").into(binding.imageGuide)
		Handler(Looper.getMainLooper()).postDelayed({
			startActivity(Intent(applicationContext, FirstActivity::class.java))
			finish()
		}, 1000)
	}
}