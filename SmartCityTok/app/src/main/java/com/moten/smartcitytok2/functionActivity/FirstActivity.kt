package com.moten.smartcitytok2.functionActivity

import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.RadioGroup
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.moten.smartcitytok2.MainActivity
import com.moten.smartcitytok2.R
import com.moten.smartcitytok2.databinding.ActivityFirstBinding
import com.moten.smartcitytok2.functions.Tool


class FirstActivity : AppCompatActivity(), View.OnClickListener {
	private lateinit var binding: ActivityFirstBinding
	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		binding = ActivityFirstBinding.inflate(layoutInflater)
		setContentView(binding.root)
		title = "请先登录"
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
			// 权限请求,文件写入
			requestPermissions(arrayOf("Manifest.permission.WRITE_EXTERNAL_STORAGE"), 100)
		}
		Tool.sharedPreferences = this.getSharedPreferences("location", Context.MODE_PRIVATE)
		if (!Tool.sharedPreferences.getString("Authorization", null).equals(null)) {
			finish()
			startActivity(Intent(this@FirstActivity, MainActivity::class.java))
		}
		binding.login.setOnClickListener(this)
		binding.register.setOnClickListener(this)
		binding.gotoRegister.setOnClickListener(this)
		binding.gotoLogin.setOnClickListener(this)

	}

	override fun onClick(v: View?) {
		when (v!!.id) {
			R.id.login -> Tool.postLogin(
				userName = binding.editText1.text.toString(),
				password = binding.editText2.text.toString(),
				context = this
			)
			R.id.register -> Tool.postRegister(
				context = this,
				userName = binding.editText1.text.toString(),
				password = binding.editText2.text.toString(),
				phoneNumber = binding.editText3.text.toString(),
				sex = if (binding.radio.isChecked) "0" else "1",
				nikeName = binding.editText5.text!!.toString(),
				email = binding.editText6.text!!.toString(),
				idCard = binding.editText7.text!!.toString()
			)
			R.id.gotoRegister -> {
				title = "注册账号"
				binding.login.visibility = Button.GONE
				binding.gotoRegister.visibility = Button.GONE
				binding.textView3.visibility = TextView.VISIBLE
				binding.textView4.visibility = TextView.VISIBLE
				binding.textView5.visibility = TextView.VISIBLE
				binding.textView6.visibility = TextView.VISIBLE
				binding.textView7.visibility = TextView.VISIBLE
				binding.editText3.visibility = EditText.VISIBLE
				binding.radioGroup.visibility = RadioGroup.VISIBLE
				binding.editText5.visibility = EditText.VISIBLE
				binding.editText6.visibility = EditText.VISIBLE
				binding.editText7.visibility = EditText.VISIBLE
				binding.register.visibility = Button.VISIBLE
				binding.gotoLogin.visibility = Button.VISIBLE
			}
			R.id.gotoLogin -> {
				title = "请先登录"
				binding.register.visibility = Button.GONE
				binding.gotoLogin.visibility = Button.GONE
				binding.textView3.visibility = TextView.GONE
				binding.textView4.visibility = TextView.GONE
				binding.textView5.visibility = TextView.GONE
				binding.textView6.visibility = TextView.GONE
				binding.textView7.visibility = TextView.GONE
				binding.editText3.visibility = EditText.GONE
				binding.radioGroup.visibility = RadioGroup.GONE
				binding.editText5.visibility = EditText.GONE
				binding.editText6.visibility = EditText.GONE
				binding.editText7.visibility = EditText.GONE
				binding.login.visibility = Button.VISIBLE
				binding.gotoRegister.visibility = Button.VISIBLE
			}
		}
	}
}