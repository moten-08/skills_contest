package com.moten.smartcitytok2.ui.smart

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.moten.smartcitytok2.R

class SmartFragment : Fragment() {

	private lateinit var smartViewModel: SmartViewModel

	override fun onCreateView(
		inflater: LayoutInflater,
		container: ViewGroup?,
		savedInstanceState: Bundle?
	): View? {
		smartViewModel =
			ViewModelProvider(this).get(SmartViewModel::class.java)
		val root = inflater.inflate(R.layout.fragment_smart, container, false)
		val textView: TextView = root.findViewById(R.id.text_smart)
		smartViewModel.text.observe(viewLifecycleOwner, Observer {
			textView.text = it
		})
		return root
	}
}