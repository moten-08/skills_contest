package com.moten.smartcitytok2.adapters

import android.content.Context
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.RatingBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.moten.smartcitytok2.R
import com.moten.smartcitytok2.dataObjects.CommingList
import com.moten.smartcitytok2.functions.Tool

class ComingAdapter(
	val context: Context,
	val list: MutableList<CommingList.RowsDTO>,
	val type: String,
	val movieId: Int,
	val movieName: String?
) :
	RecyclerView.Adapter<ComingAdapter.ViewHolder>() {
	class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
		val userName: TextView = itemView.findViewById(R.id.userName)
		val comTime: TextView = itemView.findViewById(R.id.comTime)
		val userScore: RatingBar = itemView.findViewById(R.id.userScore)
		val userCom: TextView = itemView.findViewById(R.id.userCom)
		val like: Button = itemView.findViewById(R.id.like)
	}

	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
		val view = LayoutInflater.from(context).inflate(R.layout.item_comming, parent, false)
		return ViewHolder(view)
	}

	override fun onBindViewHolder(holder: ViewHolder, position: Int) {
		val commingList = list[position]
		holder.userName.text = commingList.nickName
		holder.comTime.text = commingList.commentDate
		holder.userScore.rating = commingList.score.toFloat()
		holder.userCom.text = commingList.content
		var d2: Drawable = context.resources.getDrawable(R.drawable.ic_baseline_favorite2_24, null)
		d2.setBounds(0, 0, 50, 50)
		holder.like.setCompoundDrawables(d2, null, null, null)
		holder.like.text = "${commingList.likeNum}"
		holder.like.setOnClickListener {
			// 点赞接口
			if (movieName != null) {
				Tool.postLikeComment(commingList.id, context, type,movieId,movieName)
			}
			d2 = context.resources.getDrawable(R.drawable.ic_baseline_favorite3_24,null)
			d2.setBounds(0, 0, 50, 50)
			holder.like.setCompoundDrawables(d2, null, null, null)
		}
	}

	override fun getItemCount(): Int {
		return list.size
	}
}