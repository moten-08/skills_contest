package com.moten.smartcitytok2.adapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.moten.smartcitytok2.R
import com.moten.smartcitytok2.dataObjects.NearCinema
import com.moten.smartcitytok2.functionActivity.CinemaActivity
import com.moten.smartcitytok2.functions.Tool

class CinemaAdapter(private val context: Context, private val list: MutableList<NearCinema.RowsDTO>) :
	RecyclerView.Adapter<CinemaAdapter.ViewHolder>() {
	class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
		var cardView: CardView = itemView as CardView
		var cover: ImageView = itemView.findViewById(R.id.cinema_cover)
		var name: TextView = itemView.findViewById(R.id.cinema_name)
		var address: TextView = itemView.findViewById(R.id.cinema_address)
		var grade: RatingBar = itemView.findViewById(R.id.cinema_grade)
	}

	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
		return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_near_cinema, parent, false))
	}

	override fun onBindViewHolder(holder: ViewHolder, position: Int) {
		val cinema: NearCinema.RowsDTO = list[position]
		holder.name.text = cinema.name
		holder.address.text = cinema.address
		holder.grade.rating = (cinema.score / 20).toFloat()
		// 旧接口服务器没删干净。。。只能自己替换一下了
		val urlR: String = cinema.cover.replace("http://118.190.154.52:7777", "/prod-api")
		Glide.with(context).load(Tool.urlHeader + urlR).into(holder.cover)
		holder.cardView.setOnClickListener {
			var intent = Intent(context, CinemaActivity::class.java)
			intent.putExtra("cinema",cinema.id)
			context.startActivity(intent)
		}
	}

	override fun getItemCount(): Int {
		return list.size
	}
}