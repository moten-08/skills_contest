package com.moten.smartcitytok2.adapters

import android.app.Activity
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.moten.smartcitytok2.R
import com.moten.smartcitytok2.dataObjects.Theatre
import com.moten.smartcitytok2.functions.Tool
import kotlin.concurrent.thread
import kotlin.math.log

class BuyItemAdapter(val context: Context, val list: MutableList<Theatre.RowsDTO>) :
	RecyclerView.Adapter<BuyItemAdapter.ViewHolder>() {
	class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
		var name: TextView = itemView.findViewById(R.id.item_cinema_name)
		var price: TextView = itemView.findViewById(R.id.item_cinema_price)
		var address: TextView = itemView.findViewById(R.id.item_cinema_address)
	}

	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
		return ViewHolder(
			LayoutInflater.from(context).inflate(R.layout.item_theatre, parent, false)
		)
	}

	override fun onBindViewHolder(holder: ViewHolder, position: Int) {
		val theatre: Theatre.RowsDTO = list[position]
		holder.name.text = theatre.theatreName
		holder.price.text = "￥${theatre.price}"
		// 查询影院详情
		thread {
			var address = Tool.getTheCiname(theatre.theaterId)
			(context as Activity).runOnUiThread{
				holder.address.text = address
			}
		}
	}

	override fun getItemCount(): Int {
		return list.size
	}
}