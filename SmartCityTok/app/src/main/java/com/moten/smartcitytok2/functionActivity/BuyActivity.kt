package com.moten.smartcitytok2.functionActivity

import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.moten.smartcitytok2.R
import com.moten.smartcitytok2.adapters.BuyItemAdapter
import com.moten.smartcitytok2.adapters.TheatreViewAdapter
import com.moten.smartcitytok2.databinding.ActivityBuyBinding
import com.moten.smartcitytok2.functions.Tool
import java.text.SimpleDateFormat
import java.util.*
import kotlin.concurrent.thread

class BuyActivity : AppCompatActivity() {
	private val sdf = SimpleDateFormat("yyyy-MM-dd")
	private lateinit var binding: ActivityBuyBinding
	var movieId: Int = 0
	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		binding = ActivityBuyBinding.inflate(layoutInflater)
		setContentView(binding.root)
		title = "购票页面"
		supportActionBar!!.setDisplayHomeAsUpEnabled(true)
		supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_baseline_keyboard_arrow_left_24)
		movieId = intent.getIntExtra("movieId", -1)
		// 查询影片详情
		aboutMovie()
		// 加载列表
		theatreTitles()
	}

	private fun aboutMovie() {
		val token = (Tool.sharedPreferences).getString("Authorization", "none")
		val type = intent.getStringExtra("type")
		thread {
			val movie = Tool.getMovieFilm(movieId, token!!, this, type)
			runOnUiThread {
				binding.movieTitle.text = movie!!.data.name
				binding.movieType.text =
					when (movie.data.category) {
						"4" -> "影片类型：喜剧"
						"8" -> "影片类型：科幻"
						"3" -> "影片类型：动作"
						"7" -> "影片类型：战争"
						"2" -> "影片类型：爱情"
						"6" -> "影片类型：惊悚"
						"1" -> "影片类型：故事"
						"5" -> "影片类型：恐怖"
						else -> "影片类型：未知"
					}
				binding.movieUptime.text = "上映时间：${movie.data.playDate}"
				binding.movieGrade.text = "评分：${movie.data.score}"
				binding.movieTime.text = "时长：${movie.data.duration}分钟"
				val url = Tool.urlHeader + movie.data.cover
				// 旧接口服务器没删干净。。。又得自己替换一下了
				url.replace("http://118.190.154.52:7777", "/prod-api")
				Glide.with(this)
					.load(url)
					.error(R.drawable.ic_baseline_error_24)
					.into(binding.moviePoster)
			}
		}
	}

	private fun theatreTitles() {
		// 标题
		val titles: MutableList<String> = ArrayList()
		var date = Date(2021 - 1900, 4, 9)
		// 添加7个标题
		for (i in 0..6) {
			titles.add(sdf.format(date))
			date = Date(date.time + 1000 * 60 * 60 * 24)
			binding.movieTab.addTab(binding.movieTab.newTab())
		}
		// 列表
		val list: MutableList<View> = ArrayList()
		list.clear()
		for (t in 0 until titles.size) {
			val view: View = LayoutInflater.from(this).inflate(R.layout.item_recy, null)
			list.add(view)
		}
		binding.viewPage.adapter = TheatreViewAdapter(this, list)
		// 两控件相互绑定
		binding.movieTab.setupWithViewPager(binding.viewPage)
		for (i in 0 until titles.size) {
			// 标题加载
			binding.movieTab.getTabAt(i)!!.text = titles[i]
			val date: String = binding.movieTab.getTabAt(i)!!.text.toString()
			// 页面数据加载
			thread {
				val theatreTimeList = Tool.getTheatreTimes(movieId, date)
				runOnUiThread {
					list[i].findViewById<RecyclerView>(R.id.recycler).layoutManager = LinearLayoutManager(this)
					list[i].findViewById<RecyclerView>(R.id.recycler).adapter = BuyItemAdapter(this, theatreTimeList)
					if (theatreTimeList.size > 0) {
						list[i].findViewById<TextView>(R.id.textView).visibility = TextView.GONE
					}
				}
			}

		}
	}

	override fun onOptionsItemSelected(item: MenuItem): Boolean {
		when (item.itemId) {
			android.R.id.home -> finish()
		}
		return false
	}
}