package com.moten.smartcitytok2.ui.home

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import com.bumptech.glide.Glide
import com.moten.smartcitytok2.R
import com.moten.smartcitytok2.adapters.CinemaAdapter
import com.moten.smartcitytok2.adapters.MovieAdapter
import com.moten.smartcitytok2.dataObjects.MovieAds
import com.moten.smartcitytok2.dataObjects.WeatherForToday
import com.moten.smartcitytok2.databinding.FragmentHomeBinding
import com.moten.smartcitytok2.functionActivity.SearchListActivity
import com.moten.smartcitytok2.functionActivity.ThisMovieActivity
import com.moten.smartcitytok2.functions.Tool
import com.youth.banner.Banner
import com.youth.banner.adapter.BannerImageAdapter
import com.youth.banner.holder.BannerImageHolder
import com.youth.banner.indicator.RoundLinesIndicator
import com.youth.banner.util.BannerUtils
import kotlin.concurrent.thread

class HomeFragment : Fragment() {
	private lateinit var homeViewModel: HomeViewModel
	private lateinit var searchEdit: EditText
	private lateinit var banner: Banner<*, *>
	private lateinit var viewPager: ViewPager
	private lateinit var textView1: TextView
	private lateinit var recyclerView1: RecyclerView
	private lateinit var recyclerView2: RecyclerView
	private lateinit var recyclerView3: RecyclerView
	lateinit var movieList: MutableList<MovieAds.RowsDTO>
	private var _binding: FragmentHomeBinding? = null
	private val binding get() = _binding!!

	override fun onCreateView(
		inflater: LayoutInflater,
		container: ViewGroup?,
		savedInstanceState: Bundle?
	): View? {
		_binding = FragmentHomeBinding.inflate(inflater, container, false)
		homeViewModel =
			ViewModelProvider(this).get(HomeViewModel::class.java)
		return binding.root
	}

	override fun onDestroy() {
		super.onDestroy()
		_binding = null
	}

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		super.onViewCreated(view, savedInstanceState)
		init(view)
		initData(view)
	}

	private fun init(view: View) {
		thread {
			Tool.getGps(view)
		}
		searchEdit = view.findViewById(R.id.search_edit)
		banner = view.findViewById(R.id.movie_banner)
		viewPager = view.findViewById(R.id.weather_info)
		textView1 = view.findViewById(R.id.textView1)

		recyclerView1 = view.findViewById(R.id.hot_movie)
		recyclerView2 = view.findViewById(R.id.coming_movie)
		recyclerView3 = view.findViewById(R.id.nearCinemaList)

		// 搜索
		searchEdit.setOnEditorActionListener { _: TextView?, actionId: Int, _: KeyEvent? ->
			if (actionId == EditorInfo.IME_ACTION_SEARCH) {
				// 软键盘的回车键
				search(view)
				return@setOnEditorActionListener true
			}
			false
		}

	}

	override fun onResume() {
		super.onResume()
		textView1.requestFocus()
	}

	private fun search(view: View) {
		val searchText: String = searchEdit.text.toString()
		val intent = Intent(view.context, SearchListActivity::class.java)
		intent.putExtra("searchText", searchText)
		startActivity(intent)
	}

	private fun initData(view: View) {
		// 广告轮播图
		rotation(view)
		// 天气信息
		weather(view)
		// 三个模块
		threeRecycle(view)

	}

	private fun rotation(view: View) {
		thread {
			movieList = Tool.getMovieRotation()
			requireActivity().runOnUiThread {
				banner.apply {
					adapter = object : BannerImageAdapter<MovieAds.RowsDTO>(movieList) {
						override fun onBindView(
							holder: BannerImageHolder,
							data: MovieAds.RowsDTO,
							position: Int,
							size: Int
						) {
							Glide.with(view.context)
								.load(Tool.urlHeader + data.advImg)
								.placeholder(R.mipmap.loading)
								.into(holder.imageView)
						}
					}
					setLoopTime(2500)
					scrollTime = 500
					indicator = RoundLinesIndicator(view.context) // 设置指示器
					setIndicatorSelectedWidth(BannerUtils.dp2px(15f).toInt())
					setOnBannerListener { _, position ->
						if (movieList[position].targetId == 0) {
							Toast.makeText(requireActivity(), "暂无该影片详情", Toast.LENGTH_SHORT).show()
						} else {
							val intent = Intent(view.context, ThisMovieActivity::class.java)
							intent.putExtra("movieId", movieList[position].targetId)
							intent.putExtra("type", "hot")
							startActivity(intent)
						}
					}
				}
			}
		}
	}

	private fun weather(view: View) {
		val viewList: MutableList<View> = ArrayList()
		val view1: View = LayoutInflater.from(view.context).inflate(R.layout.item_weather1, null)
		val view2: View = LayoutInflater.from(view.context).inflate(R.layout.item_weather2, null)
		viewList.add(view1)
		viewList.add(view2)
		// 天气-页面适配
		thread {
			var weather: WeatherForToday.DataDTO = Tool.getWeather()
			requireActivity().runOnUiThread {
				when (weather.weather) {
					"晴" -> {
						viewPager.setBackgroundColor(Color.rgb(0x66, 0xcc, 0xff))
						view1.findViewById<ImageView>(R.id.item_weather_icon)
							.setImageResource(R.drawable.ic_sun)
					}
					"多云" -> {
						viewPager.setBackgroundColor(Color.rgb(0x00, 0x00, 0xdd))
						view1.findViewById<ImageView>(R.id.item_weather_icon)
							.setImageResource(R.drawable.ic_cloudy)
					}
					"小雨" ->
						view1.findViewById<ImageView>(R.id.item_weather_icon)
							.setImageResource(R.drawable.ic_rain)
					else -> {
						Color.rgb(0xdd, 0xdd, 0xdd)
					}
				}
				view1.findViewById<TextView>(R.id.item_weather_text).text = "${weather.weather}"
				view1.findViewById<TextView>(R.id.item_weather_temperature).text =
					"${weather.temperature}℃"
				view2.findViewById<TextView>(R.id.item_air).text = "空气质量：${weather.air}"
				view2.findViewById<TextView>(R.id.item_weather_temperatures).text =
					"${weather.maxTemperature}℃/${weather.minTemperature}℃"
				view2.findViewById<TextView>(R.id.item_humidity).text = "湿度：${weather.humidity}%"
				view2.findViewById<TextView>(R.id.item_uv).text = "紫外线：${weather.uv}"
				view2.findViewById<TextView>(R.id.item_apparentTemperature).text =
					"体感温度：${weather.apparentTemperature}℃"
			}
		}
		viewPager.adapter = object : PagerAdapter() {
			override fun getCount(): Int {
				return viewList.size
			}

			override fun isViewFromObject(view: View, `object`: Any): Boolean {
				return view === `object`
			}

			override fun instantiateItem(container: ViewGroup, position: Int): Any {
				val view = viewList[position]
				container.addView(view)
				return view
			}

			override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
				if (viewList.size > 0) {
					container.removeView(viewList[position])
				}
			}
		}
	}

	private fun threeRecycle(view: View) {
		// 当前热映
		recyclerView1.layoutManager =
			LinearLayoutManager(view.context, GridLayoutManager.HORIZONTAL, false)
		thread {
			var hotMovieList = Tool.getHotMovie()
			requireActivity().runOnUiThread {
				recyclerView1.adapter = MovieAdapter(view.context, hotMovieList, "hot")
			}
		}
		// 即将上映
		recyclerView2.layoutManager =
			LinearLayoutManager(view.context, LinearLayoutManager.HORIZONTAL, false)
		thread {
			var noticeMovie = Tool.getNoticeMovie()
			requireActivity().runOnUiThread {
				recyclerView2.adapter = MovieAdapter(view.context, noticeMovie, "want")
			}
		}
		// 附近电影院
		recyclerView3.layoutManager =
			LinearLayoutManager(view.context, LinearLayoutManager.VERTICAL, false)
		thread {
			var nearCinema = Tool.getNearCinema()
			requireActivity().runOnUiThread {
				recyclerView3.adapter = CinemaAdapter(view.context, nearCinema)
			}
		}
	}
}