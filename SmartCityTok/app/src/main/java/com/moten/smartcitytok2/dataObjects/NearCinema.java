package com.moten.smartcitytok2.dataObjects;

import java.util.List;

public class NearCinema {

	public int total;
	public List<RowsDTO> rows;
	public int code;
	public String msg;

	public static class RowsDTO {
		public Object searchValue;
		public String createBy;
		public String createTime;
		public Object updateBy;
		public Object updateTime;
		public Object remark;
		public ParamsDTO params;
		public int id;
		public String name;
		public String cover;
		public String province;
		public String city;
		public String area;
		public String address;
		public int score;
		public Object tags;
		public Object brand;
		public String distance;
		public String status;
		public Object movieId;
		public Object timesId;

		public static class ParamsDTO {
		}
	}
}
