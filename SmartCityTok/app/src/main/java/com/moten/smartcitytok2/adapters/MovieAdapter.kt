package com.moten.smartcitytok2.adapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.moten.smartcitytok2.R
import com.moten.smartcitytok2.dataObjects.Movie
import com.moten.smartcitytok2.functionActivity.BuyActivity
import com.moten.smartcitytok2.functionActivity.ThisMovieActivity
import com.moten.smartcitytok2.functions.Tool

class MovieAdapter(
	private val context: Context,
	private val movieList: List<Movie.RowsDTO>? = null,
	private val type: String
) : RecyclerView.Adapter<MovieAdapter.ViewHolder>() {

	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
		val view: View = LayoutInflater.from(context).inflate(R.layout.item_movie, parent, false)
		return ViewHolder(view)
	}

	override fun onBindViewHolder(holder: ViewHolder, position: Int) {
		if (type == "hot") {
			holder.buy.visibility = Button.VISIBLE
			holder.want.visibility = Button.GONE
		} else {
			holder.buy.visibility = Button.GONE
			holder.want.visibility = Button.VISIBLE
		}
		val movie: Movie.RowsDTO = movieList!![position]
		Glide.with(context)
			.load(Tool.urlHeader + movie.cover)
			.error(R.drawable.ic_baseline_error_24)
			.into(holder.cover)
		holder.title.text = if (movie.name.length > 8) "${movie.name.subSequence(0, 7)}……" else movie.name
		holder.cardView.setOnClickListener {
			val intent = Intent(context, ThisMovieActivity::class.java)
			intent.putExtra("type", type)
			intent.putExtra("movieId", movie.id)
			intent.putExtra("movieName", movie.name)
			context.startActivity(intent)
		}
		holder.buy.setOnClickListener {
			val intent = Intent(context, BuyActivity::class.java)
			intent.putExtra("type", "hot")
			intent.putExtra("movieId", movie.id)
			context.startActivity(intent)
		}


		holder.want.setOnClickListener {
			Toast.makeText(context, "Add to Favorites", Toast.LENGTH_SHORT).show()
		}
	}

	override fun getItemCount(): Int {
		return movieList?.size ?: 0
	}

	class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
		var cardView: CardView = itemView as CardView
		var cover: ImageView = itemView.findViewById(R.id.movie_cover)
		var title: TextView = itemView.findViewById(R.id.movie_title)
		var buy: Button = itemView.findViewById(R.id.movie_seen)
		var want: Button = itemView.findViewById(R.id.movie_want)
	}
}