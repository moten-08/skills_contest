package com.moten.smartcitytok2.dataObjects;

public class CinameOne {

	public String msg;
	public int code;
	public DataDTO data;

	public static class DataDTO {
		public Object searchValue;
		public String createBy;
		public String createTime;
		public Object updateBy;
		public Object updateTime;
		public Object remark;
		public ParamsDTO params;
		public int id;
		public String name;
		public String cover;
		public String province;
		public String city;
		public String area;
		public String address;
		public int score;
		public Object tags;
		public Object brand;
		public String distance;
		public String status;
		public Object movieId;
		public Object timesId;

		@Override
		public String toString() {
			return "DataDTO{" +
					"searchValue=" + searchValue +
					", createBy='" + createBy + '\'' +
					", createTime='" + createTime + '\'' +
					", updateBy=" + updateBy +
					", updateTime=" + updateTime +
					", remark=" + remark +
					", params=" + params +
					", id=" + id +
					", name='" + name + '\'' +
					", cover='" + cover + '\'' +
					", province='" + province + '\'' +
					", city='" + city + '\'' +
					", area='" + area + '\'' +
					", address='" + address + '\'' +
					", score=" + score +
					", tags=" + tags +
					", brand=" + brand +
					", distance='" + distance + '\'' +
					", status='" + status + '\'' +
					", movieId=" + movieId +
					", timesId=" + timesId +
					'}';
		}

		public static class ParamsDTO {
		}
	}
}
