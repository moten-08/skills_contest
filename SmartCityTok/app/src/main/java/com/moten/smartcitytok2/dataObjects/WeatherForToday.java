package com.moten.smartcitytok2.dataObjects;

public class WeatherForToday {
	public String msg;
	public int code;
	public DataDTO data;

	public static class DataDTO {
		public String maxTemperature;
		public String uv;
		public String minTemperature;
		public String temperature;
		public String weather;
		public String humidity;
		public String air;
		public String apparentTemperature;
		public String label;
		public int day;

		@Override
		public String toString() {
			return "DataDTO{" +
					"maxTemperature='" + maxTemperature + '\'' +
					", uv='" + uv + '\'' +
					", minTemperature='" + minTemperature + '\'' +
					", temperature='" + temperature + '\'' +
					", weather='" + weather + '\'' +
					", humidity='" + humidity + '\'' +
					", air='" + air + '\'' +
					", apparentTemperature='" + apparentTemperature + '\'' +
					", label='" + label + '\'' +
					", day=" + day +
					'}';
		}
	}
}
