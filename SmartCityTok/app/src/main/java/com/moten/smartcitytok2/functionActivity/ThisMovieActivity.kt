package com.moten.smartcitytok2.functionActivity

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.webkit.WebView
import android.widget.Button
import android.widget.EditText
import android.widget.RatingBar
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.moten.smartcitytok2.R
import com.moten.smartcitytok2.adapters.ComingAdapter
import com.moten.smartcitytok2.dataObjects.CommingList
import com.moten.smartcitytok2.dataObjects.MovieFilm
import com.moten.smartcitytok2.databinding.ActivityThisMovieBinding
import com.moten.smartcitytok2.functions.Tool
import java.text.DecimalFormat
import kotlin.concurrent.thread

class ThisMovieActivity : AppCompatActivity() {
	private lateinit var binding: ActivityThisMovieBinding
	var list: MutableList<CommingList.RowsDTO> = ArrayList()
	private val token = (Tool.sharedPreferences).getString("Authorization", "none")
	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		binding = ActivityThisMovieBinding.inflate(layoutInflater)
		setContentView(binding.root)

		title = "电影详情"
		supportActionBar!!.setDisplayHomeAsUpEnabled(true)
		supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_baseline_keyboard_arrow_left_24)

		val movieId = intent.getIntExtra("movieId", -1)
		val movieName = intent.getStringExtra("movieName")
		val type = intent.getStringExtra("type")
		// 添加按钮前图标
		loadLogo()

		Tool.sharedPreferences = this@ThisMovieActivity.getSharedPreferences("location", Context.MODE_PRIVATE)
		// 加载影片信息
		if (token != null) {
			thread {
				val film: MovieFilm? = Tool.getMovieFilm(movieId, token, this, type)
				runOnUiThread {
					findViewById<TextView>(R.id.movie_title).text = film!!.data.name
					findViewById<TextView>(R.id.movie_type).text =
						when (film!!.data.category) {
							"4" -> "影片类型：喜剧"
							"8" -> "影片类型：科幻"
							"3" -> "影片类型：动作"
							"7" -> "影片类型：战争"
							"2" -> "影片类型：爱情"
							"6" -> "影片类型：惊悚"
							"1" -> "影片类型：故事"
							"5" -> "影片类型：恐怖"
							else -> "影片类型：未知"
						}
					Glide.with(this)
						.load(Tool.urlHeader + film.data.cover)
						.error(R.drawable.ic_baseline_error_24)
						.into(findViewById(R.id.movie_poster))
					findViewById<TextView>(R.id.movie_uptime).text = "上映时间：${film.data.playDate}"
					findViewById<TextView>(R.id.movie_grade).text = "评分：${film.data.score}"
					if (film.data.favoriteNum == 0 && film.data.likeNum == 0) {
						findViewById<Button>(R.id.movie_seen).visibility = Button.GONE
						findViewById<Button>(R.id.wantSeeMovie).visibility = Button.GONE
					}
					findViewById<Button>(R.id.movie_seen).text = "看过：${film.data.favoriteNum}"
					findViewById<Button>(R.id.wantSeeMovie).text = "想看：${film.data.likeNum}"
					// 简介
					val filmText = "简介：${film.data.introduction}"
					Log.d("TAG", "onCreate: ${filmText.length}")
					if (filmText.length > 20) {
						findViewById<WebView>(R.id.movie_film).loadData(
							"${filmText.subSequence(0, 20)}……",
							"text/html",
							"utf-8"
						)
						// 展开
						findViewById<TextView>(R.id.more_film).setOnClickListener {
							findViewById<WebView>(R.id.movie_film).loadData(filmText, "text/html", "utf-8")
							findViewById<TextView>(R.id.more_film).visibility = TextView.GONE
							findViewById<TextView>(R.id.more_down).visibility = TextView.VISIBLE
						}
						// 收起
						findViewById<TextView>(R.id.more_down).setOnClickListener {
							findViewById<WebView>(R.id.movie_film).loadData(
								"${filmText.subSequence(0, 20)}……",
								"text/html",
								"utf-8"
							)
							findViewById<TextView>(R.id.more_film).visibility = TextView.VISIBLE
							findViewById<TextView>(R.id.more_down).visibility = TextView.GONE
						}
					} else {
						// 简介太短，没有必要折叠
						findViewById<WebView>(R.id.movie_film).loadData(filmText, "text/html", "utf-8")
						findViewById<TextView>(R.id.more_film).visibility = TextView.GONE
						findViewById<TextView>(R.id.more_down).visibility = TextView.GONE
					}
				}
			}
		}

		// 加载评论列表
		if (type != null) {
			loadCommentList(type, movieId, movieName)
		}

		thread {
			list.clear()
			list.addAll(
				if (type == "hot") {
					Tool.getCommentList(id = movieId)
				} else {
					Tool.getCommentList(movieName = movieName.toString())
				}
			)
			runOnUiThread {
				binding.comingList.adapter!!.notifyDataSetChanged()
				"评论数：${list.size}".also { binding.movieCommentTotal.text = it }
			}
		}
		dialog(movieId, type, movieName)
		if (type == "hot") {
			binding.buy.setOnClickListener {
				// 进入购票页面
				val intent = Intent(this, BuyActivity::class.java)
				intent.putExtra("movieId", movieId)
				intent.putExtra("type", type)
				startActivity(intent)
			}
		} else {
			binding.buy.visibility = Button.GONE
		}
	}

	private fun loadLogo() {
		val d1: Drawable = resources.getDrawable(R.drawable.ic_baseline_star_24, null)
		d1.setBounds(0, 0, 50, 50)
		val d2: Drawable = resources.getDrawable(R.drawable.ic_baseline_favorite_24, null)
		d2.setBounds(0, 0, 50, 50)
		binding.movieSeen.setCompoundDrawables(d1, null, null, null)
		binding.wantSeeMovie.setCompoundDrawables(d2, null, null, null)
	}

	private fun loadCommentList(type: String, movieId: Int, movieName: String?) {
		binding.comingList.layoutManager = LinearLayoutManager(this)
		binding.comingList.adapter = ComingAdapter(this, list, type, movieId, movieName)
	}

	private fun dialog(movieId: Int, type: String?, movieName: String?) {
		// 评论弹窗
		binding.dialogButton.setOnClickListener {
			val view: View = LayoutInflater.from(this@ThisMovieActivity).inflate(R.layout.dialog_create_commit, null)
			val dialog: AlertDialog = AlertDialog.Builder(this@ThisMovieActivity)
				.setView(view)
				.setTitle("发布评论")
				.create()
			dialog.show()
			view.findViewById<Button>(R.id.publish).setOnClickListener {
				val str = view.findViewById<EditText>(R.id.create).text.toString()
				// 直接调用增加评论接口
				if (type == "hot") {
					Tool.postComment(
						str,
						movieId,
						DecimalFormat("0").format(view.findViewById<RatingBar>(R.id.score).rating),
						this
					)
				} else {
					// 得先搜索电影评论列表再传id
					for (c in list) {
						if (c.movieName.trim() == movieName!!.trim()) {
							Tool.postComment(
								str,
								c.movieId,
								DecimalFormat("0").format(view.findViewById<RatingBar>(R.id.score).rating),
								this
							)
							break
						}
					}
				}
				dialog.dismiss()
			}
		}
	}

	override fun onOptionsItemSelected(item: MenuItem): Boolean {
		// 左上返回键
		when (item.itemId) {
			android.R.id.home -> finish()
		}
		return super.onOptionsItemSelected(item)
	}

}