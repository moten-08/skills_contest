package com.moten.smartcitytok2.dataObjects;

import java.util.List;

public class CommingList {

	public int total;
	public List<RowsDTO> rows;
	public int code;
	public String msg;

	public static class RowsDTO {
		public Object searchValue;
		public Object createBy;
		public Object createTime;
		public Object updateBy;
		public Object updateTime;
		public Object remark;
		public ParamsDTO params;
		public int id;
		public int movieId;
		public int userId;
		public int score;
		public String content;
		public String commentDate;
		public int likeNum;
		public String movieName;
		public String userName;
		public String nickName;

		public static class ParamsDTO {
		}

		@Override
		public String toString() {
			return "RowsDTO{" +
					"searchValue=" + searchValue +
					",createBy=" + createBy +
					",createTime=" + createTime +
					",updateBy=" + updateBy +
					",updateTime=" + updateTime +
					",remark=" + remark +
					",params=" + params +
					",id=" + id +
					",movieId=" + movieId +
					",userId=" + userId +
					",score=" + score +
					",content='" + content + '\'' +
					",commentDate='" + commentDate + '\'' +
					",likeNum=" + likeNum +
					",movieName='" + movieName + '\'' +
					",userName='" + userName + '\'' +
					",nickName='" + nickName + '\'' +
					'}';
		}
	}

	@Override
	public String toString() {
		return "CommingList{" +
				"total=" + total +
				", rows=" + rows +
				", code=" + code +
				", msg='" + msg + '\'' +
				'}';
	}
}
