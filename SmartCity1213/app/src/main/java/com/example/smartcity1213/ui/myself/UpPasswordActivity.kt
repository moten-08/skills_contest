package com.example.smartcity1213.ui.myself

import android.content.SharedPreferences
import android.os.Bundle
import com.example.smartcity1213.R
import com.example.smartcity1213.request.UpPass
import com.example.smartcity1213.result.BaseResult
import com.example.smartcity1213.tools.*
import kotlinx.android.synthetic.main.activity_up_password.*

class UpPasswordActivity : BaseActivity() {

    private lateinit var sp: SharedPreferences
    private lateinit var token: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_up_password)


        sp = getSharedPreferences("data", MODE_PRIVATE)
        token = sp.getString("token", "") ?: ""

        submit.setOnClickListener {
            val ord = orderPass.text.toString()
            val np = newPass.text.toString()
            val np2 = newPass2.text.toString()

            if (np != np2) {
                "两次输入的密码不一致".toast(this)
                return@setOnClickListener
            }
            HttpUtil.putTo(
                "api/common/user/resetPwd",
                token = token,
                requestBody = UpPass(np, ord),
                callback = MyCallBack {
                    val data = jsonToModel(it,BaseResult::class.java)
                    runOnUiThread {
                        data.msg.toast(applicationContext)
                        if (data.code == 200){
                            finish()
                        }
                    }
                })
        }
    }
}