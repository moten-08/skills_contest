package com.example.smartcity1213.tools

import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import android.content.res.Configuration
import android.content.res.Configuration.SCREENLAYOUT_SIZE_MASK
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.bumptech.glide.Glide
import com.google.android.material.snackbar.Snackbar
import java.util.regex.Pattern

fun SharedPreferences.open(block: SharedPreferences.Editor.() -> Unit) {
    val edit = edit()
    edit.block()
    edit.apply()
}

fun String.toast(context: Context) {
    Toast.makeText(context, this, Toast.LENGTH_SHORT).show()
}

fun String.snack(view: View, action: String? = "", block: () -> Unit) {
    Snackbar.make(view, this, Snackbar.LENGTH_SHORT)
        .setAction(action) { block() }
        .show()
}

fun Activity.off() {
    val imm = this.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    if (imm.isActive) imm.hideSoftInputFromWindow(
        this.currentFocus?.windowToken,
        InputMethodManager.HIDE_NOT_ALWAYS
    )
}

fun ImageView.glide(url: Any?, local: Boolean = false, circle: Boolean = false) {
    if (circle) {
        Glide.with(this.context)
            .load(if (local) url else HttpUtil.BASE_URL + url)
            .circleCrop()
            .into(this)
    } else {
        Glide.with(this.context)
            .load(if (local) url else HttpUtil.BASE_URL + url)
            .into(this)
    }
}

fun <T> jsonToModel(data: Any, clazz: Class<T>): T =
    HttpUtil.GSON.fromJson(HttpUtil.GSON.toJson(data), clazz)

fun Context.isTablet(): Boolean {
    val screenLayout = this.resources.configuration.screenLayout
    val size = Configuration.SCREENLAYOUT_SIZE_LARGE
    return (screenLayout and SCREENLAYOUT_SIZE_MASK) >= size
}

fun TextView.format(vararg arg: Any) {
    this.text = String.format(this.text.toString(), *arg)
}

fun String.removeHTML(): String = Pattern.compile("<[^>]+>")
    .matcher(this)
    .replaceAll("")
    .replace("&nbsp;", "\n")
    .replace("&ensp;", "")
    .replace("&emsp;", "")