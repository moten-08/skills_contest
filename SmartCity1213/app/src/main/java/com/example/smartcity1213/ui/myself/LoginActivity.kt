package com.example.smartcity1213.ui.myself

import android.content.Context
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import com.example.smartcity1213.R
import com.example.smartcity1213.request.LoginQ
import com.example.smartcity1213.result.Login
import com.example.smartcity1213.tools.*
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : BaseActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_login)
        title = "登录界面"

        val username = findViewById<EditText>(R.id.username)
        val password = findViewById<EditText>(R.id.password)
        val login = findViewById<Button>(R.id.login)

        login.setOnClickListener {
            val usernameStr = username.text.toString()
            val passwordStr = password.text.toString()
            if (usernameStr.trim().isEmpty()) {
                "用户名不可为空".toast(this)
                return@setOnClickListener
            }
            if (passwordStr.trim().isEmpty()) {
                "密码不可为空".toast(this)
            }
            loading.visibility = View.VISIBLE
            login(LoginQ(usernameStr, passwordStr))
        }

    }

    private fun login(login: LoginQ) {
        HttpUtil.post(
            "api/login",
            requestBody = login,
            callback = MyCallBack {
                val data = jsonToModel(it, Login::class.java)
                runOnUiThread {
                    if (data.code == 200) {
                        finish()
                        val sp = getSharedPreferences("data", Context.MODE_PRIVATE)
                        sp.open {
                            putString("token", data.token)
                        }
                    }
                    loading.visibility = View.GONE
                    data.msg.toast(applicationContext)
                }
            })
    }
}
