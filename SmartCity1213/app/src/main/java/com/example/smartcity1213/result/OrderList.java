package com.example.smartcity1213.result;

import java.util.List;

public class OrderList {

    public int total;
    public List<RowsDTO> rows;
    public int code;
    public String msg;

    public static class RowsDTO {
        public Object searchValue;
        public Object createBy;
        public Object createTime;
        public Object updateBy;
        public Object updateTime;
        public Object remark;
        public ParamsDTO params;
        public int id;
        public String orderNo;
        public double amount;
        public String orderStatus;
        public int userId;
        public String payTime;
        public String name;
        public String orderType;
        public String orderTypeName;

        public static class ParamsDTO {
        }
    }
}
