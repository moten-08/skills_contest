package com.example.smartcity1213.ui.myself

import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Bitmap
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import com.example.smartcity1213.R
import com.example.smartcity1213.request.UpUserInfo
import com.example.smartcity1213.result.BaseResult
import com.example.smartcity1213.result.FileLoad
import com.example.smartcity1213.result.UserInfo
import com.example.smartcity1213.tools.*
import kotlinx.android.synthetic.main.activity_user_info.*

class UserInfoActivity : BaseActivity() {

    private lateinit var sp: SharedPreferences
    private lateinit var token: String

    private lateinit var filePath: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_info)

        title = "个人信息"
        sp = getSharedPreferences("data", MODE_PRIVATE)
        token = sp.getString("token", "") ?: ""
        val data = intent.getSerializableExtra("userInfo") as UserInfo.UserDTO

        with(data.avatar) {
            avatarUI.glide(if ("/prod-api" in this) this else "/prod-api$this")
        }

        avatarUI.setOnClickListener {
            // 相机
//            Intent(MediaStore.ACTION_IMAGE_CAPTURE).apply {
//                startActivityForResult(this, 456)
//            }
            // 相册
            Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI).apply {
                startActivityForResult(this, 123)
            }
        }

        filePath = data.avatar
        usernameUI.format(data.userName)
        nickNameEd.setText(data.nickName)
        if (data.sex == "0") {
            sex0.isChecked = true
        } else {
            sex1.isChecked = true
        }
        var sex = data.sex.toInt()
        sexChoose.setOnCheckedChangeListener { _, checkedId ->
            sex = if (checkedId == sex0.id) 0 else 1
        }
        phoneEd.setText(data.phonenumber)
        val idcard = StringBuilder(data.idCard)
        text4.format(idcard.replace(2, idcard.length - 4, "*".repeat(idcard.length - 6)))
        text5.format(data.email)
        submit.setOnClickListener {
            val nikeName = nickNameEd.text.toString()
            val phone = phoneEd.text.toString()
            commonUser(UpUserInfo(filePath, data.email, data.idCard, nikeName, phone, sex))
        }
    }

    private fun commonUser(upUserInfo: UpUserInfo) {
        HttpUtil.putTo(
            "api/common/user",
            token, upUserInfo, MyCallBack {
                val data = jsonToModel(it, BaseResult::class.java)
                runOnUiThread {
                    data.msg.toast(this)
                    if (data.code == 200) {
                        finish()
                    }
                }
            })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK) {
            when (requestCode) {
                123 -> {
                    val uri = data?.data
                    avatarUI.glide(uri, local = true)
                    if (uri != null) {
                        val inputStream = contentResolver.openInputStream(uri)
                        val bytes = ByteArray(inputStream!!.available())
                        inputStream.read(bytes)
                        tip.visibility = View.VISIBLE
                        upFile.setOnClickListener { upFile(bytes) }

                    }
                }
                456 -> {
                    val bitmap = data?.extras?.get("data") as Bitmap
                    avatarUI.glide(bitmap, local = true)
                    // 先不扩展相机的，耗时
                }
            }
        }
    }

    private fun upFile(bytes: ByteArray) {
        HttpUtil.upload(token, bytes,
            MyCallBack {
                val data = jsonToModel(it, FileLoad::class.java)
                runOnUiThread {
                    (data.msg ?: "错误").toast(this)
                    if (data.code == 200) {
                        tip.visibility = View.GONE
                        filePath = data.fileName
                    }
                }
            })
    }

}