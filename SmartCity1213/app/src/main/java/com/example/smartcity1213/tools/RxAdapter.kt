package com.example.smartcity1213.tools

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

class RxAdapter(
    private val list: List<Any>,
    private val context: Context,
    private val resource: Int,
    val block: (root: View, position: Int) -> Unit
) : RecyclerView.Adapter<RxAdapter.ViewHolder>() {
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(LayoutInflater.from(context).inflate(resource, parent, false))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        block(holder.itemView, position)
    }

    override fun getItemCount(): Int = list.size
}