package com.example.smartcity1213.result
/**
 * 新闻内容
 */
data class NewsInfo(
    var code: Int,
    var `data`: Data,
    var msg: String
) {
    class Data(
        var appType: String,
        var commentNum: Int,
        var content: String,
        var cover: String,
        var createBy: String,
        var createTime: String,
        var hot: String,
        var id: Int,
        var likeNum: Int,
        var params: Params,
        var publishDate: String,
        var readNum: Int,
        var remark: Any?,
        var searchValue: Any?,
        var status: String,
        var subTitle: Any?,
        var tags: Any?,
        var title: String,
        var top: String,
        var type: String,
        var updateBy: String,
        var updateTime: String
    ) {
        class Params
    }
}