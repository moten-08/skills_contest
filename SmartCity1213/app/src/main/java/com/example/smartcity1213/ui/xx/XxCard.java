package com.example.smartcity1213.ui.xx;

public class XxCard {
    private int image;
    private String title;
    private String info;

    public XxCard(int image, String title, String info) {
        this.image = image;
        this.title = title;
        this.info = info;
    }

    public int getImage() {
        return image;
    }

    public String getTitle() {
        return title;
    }

    public String getInfo() {
        return info;
    }
}
