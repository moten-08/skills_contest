package com.example.smartcity1213.tools

import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*

interface FitService {
    @Headers("Content-Type:application/json;charset=UTF-8")

    @POST("/prod-api/{url}")
    fun postTo(
        @Path("url") url: String,
        @Header("Authorization") token: String = "",
        @Body body: RequestBody
    ): Call<Any>

    // 文件上传
    @Multipart
    @POST("/prod-api/common/upload")
    fun upload(
        @Header("Authorization") token: String = "",
        @Part partList: List<MultipartBody.Part>
    ): Call<Any>

    @GET("/prod-api/{url}")
    fun getTo(
        @Path("url") url: String,
        @Header("Authorization") token: String = "",
        @QueryMap map: Map<String, String> = mapOf("" to "")
    ): Call<Any>

    @PUT("/prod-api/{url}")
    fun putTo(
        @Path("url") url: String,
        @Header("Authorization") token: String = "",
        @Body body: RequestBody
    ): Call<Any>

    @DELETE("/prod-api/{url}")
    fun deleteTo(
        @Path("url") url: String,
        @Header("Authorization") token: String = "",
        @QueryMap map: Map<String, String> = mapOf("" to "")
    ): Call<Any>
}