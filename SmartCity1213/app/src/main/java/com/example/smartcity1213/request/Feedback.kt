package com.example.smartcity1213.request

data class Feedback(
    var content: String,
    var title: String
)