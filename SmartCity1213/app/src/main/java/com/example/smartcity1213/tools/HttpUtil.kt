package com.example.smartcity1213.tools

import android.util.Log
import com.google.gson.Gson
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.toRequestBody
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object HttpUtil {
    private const val baseUrl1 = "http://10.10.230.112:10001"
    private const val baseUrl2 = "http://124.93.196.45:10001"
    const val BASE_URL = baseUrl2
    private val MEDIA_TYPE = "application/json".toMediaTypeOrNull()
    private val MEDIA_FILE = "multipart/form-data".toMediaTypeOrNull()

    val GSON = Gson()
    private val service: FitService = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .build()
        .create(FitService::class.java)

    fun get(
        url: String,
        token: String = "",
        map: Map<String, String> = mapOf("" to ""),
        callback: MyCallBack
    ) {
        val result = service.getTo(url, token, map).enqueue(callback)
        Log.d("$url -- $map", "get: $result")
    }

    fun post(
        url: String,
        token: String = "",
        requestBody: Any,
        callback: MyCallBack
    ) {
        val body = GSON.toJson(requestBody)
        val result = service.postTo(url, token, body.toRequestBody(MEDIA_TYPE)).enqueue(callback)
        Log.d(url, "post: $result")
    }

    fun upload(
        token: String,
        bytes: ByteArray,
        callback: MyCallBack
    ) {
        val body = MultipartBody.Builder()
            .setType(MultipartBody.FORM)
            .addFormDataPart("file", "sc1213.jpeg", bytes.toRequestBody(MEDIA_FILE))
            .build()
        val result = service.upload(token, body.parts).enqueue(callback)
        Log.d("upload", "$result")
    }

    fun putTo(
        url: String,
        token: String = "",
        requestBody: Any? = null,
        callback: MyCallBack
    ) {
        val body = GSON.toJson(requestBody)
        val result = service.putTo(url, token, body.toRequestBody(MEDIA_TYPE)).enqueue(callback)
        Log.d(url, "put: $result")
    }

    fun deleteTo(
        url: String,
        token: String = "",
        map: Map<String, String> = mapOf("" to ""),
        callback: MyCallBack
    ) {
        val result = service.getTo(url, token, map).enqueue(callback)
        Log.d("$url -- $map", "delete: $result")
    }
}