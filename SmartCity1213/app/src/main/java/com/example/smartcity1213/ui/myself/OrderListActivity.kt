package com.example.smartcity1213.ui.myself

import android.content.SharedPreferences
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.PagerAdapter
import com.example.smartcity1213.R
import com.example.smartcity1213.result.DictData
import com.example.smartcity1213.result.OrderList
import com.example.smartcity1213.tools.*
import kotlinx.android.synthetic.main.activity_order_list.*

class OrderListActivity : BaseActivity() {

    private lateinit var sp: SharedPreferences
    private lateinit var token: String

    private val typeList = MutableLiveData<DictData>()
    private val views = ArrayList<View>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_order_list)
        title = "订单列表"

        sp = getSharedPreferences("data", MODE_PRIVATE)
        token = sp.getString("token", "") ?: ""

        getDict()
        typeList.observe(this) {
            (it.data).forEach { item ->
                val view = View.inflate(this, R.layout.item_news_f, null)
                views.add(view)
                view.findViewById<RecyclerView>(R.id.itemNF).apply {
                    layoutManager = LinearLayoutManager(this@OrderListActivity)
                    getAllOrder(item.dictLabel, this)
                }

            }
            vpOrder.adapter = pagerAdapter(it)
            tabOrder.setupWithViewPager(vpOrder)
            it.data.forEachIndexed { index, item ->
                tabOrder.getTabAt(index)?.text = item.dictLabel
            }

        }
    }

    private fun pagerAdapter(it: DictData) = object : PagerAdapter() {
        override fun getCount(): Int = it.data.size

        override fun isViewFromObject(view: View, `object`: Any): Boolean =
            `object` == view

        override fun instantiateItem(container: ViewGroup, position: Int): Any {
            val view = views[position]
            container.addView(view)
            return view
        }

        override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) =
            container.removeView(views[position])

    }

    private fun getAllOrder(type: String, recyclerView: RecyclerView) {
        HttpUtil.get(
            "api/allorder/list", token,
            mapOf("orderStatus" to type),
            callback = MyCallBack {
                val data = jsonToModel(it, OrderList::class.java)
                runOnUiThread {
                    orderAdapter(recyclerView, data)
                }
            })
    }

    private fun orderAdapter(
        recyclerView: RecyclerView,
        data: OrderList
    ) {
        recyclerView.adapter = RxAdapter(
            data.rows,
            this@OrderListActivity,
            R.layout.item_order
        ) { root, position ->
            val orderNo = root.findViewById<TextView>(R.id.orderNo)
            val orderType = root.findViewById<TextView>(R.id.orderType)
            val orderDate = root.findViewById<TextView>(R.id.createTime)
            orderNo.format(data.rows[position].orderNo)
            orderType.format(data.rows[position].orderTypeName)
            orderDate.format(data.rows[position].createTime ?: "暂无")
        }
    }

    private fun getDict() {
        HttpUtil.get(
            "system/dict/data/type/tk_order_status",
            callback = MyCallBack {
                typeList.value = jsonToModel(it, DictData::class.java)
            })
    }
}