package com.example.smartcity1213.request

data class UpUserInfo(
    var avatar: String,
    var email: String,
    var idCard: String,
    var nickName: String,
    var phonenumber: String,
    var sex: Int
)