package com.example.smartcity1213.ui.xx;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.smartcity1213.R;
import com.example.smartcity1213.tools.OtherKt;
import com.youth.banner.Banner;
import com.youth.banner.loader.ImageLoader;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class XxFragment extends Fragment {

    Banner banner;
    LinearLayout tool1;
    LinearLayout tool2;
    LinearLayout tool3;
    LinearLayout tool4;
    LinearLayout newsgroup1;
    LinearLayout newsgroup2;
    LinearLayout newsgroup3;
    View view1;
    View view2;
    View view3;
    RecyclerView newsRec;
    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
    List<Integer> imageList = Arrays.asList(R.mipmap.banner1, R.mipmap.banner2, R.mipmap.banner3);
    XxCard[] cards = {new XxCard(R.mipmap.news2, "科技激发新活力 “智慧党建”正青春", "智能化时代，互联网技术在各行各业生根发芽，影响着人们衣食住行的各个方面。各地党建工作也在积极融入“互联网 ”的潮流，开启“智慧”时代。"),
            new XxCard(R.mipmap.news1, "创新党建工作，小程序助力新时代智慧党建", "习近平总书记指出，互联网是我们面临的“最大变量”，如果党过不了互联网这一关，就过不了长期执政这一关。因此，新时代党建如何创新是每个组织都需要思考的问题。"),};
    List<XxCard> newsList = new ArrayList<>();

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_xx, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        banner = (Banner) view.findViewById(R.id.xx_banner);
        tool1 = (LinearLayout) view.findViewById(R.id.xx_main_tool1);
        tool2 = (LinearLayout) view.findViewById(R.id.xx_main_tool2);
        tool3 = (LinearLayout) view.findViewById(R.id.xx_main_tool3);
        tool4 = (LinearLayout) view.findViewById(R.id.xx_main_tool4);
        newsgroup1 = (LinearLayout) view.findViewById(R.id.linearLayout1);
        newsgroup2 = (LinearLayout) view.findViewById(R.id.linearLayout2);
        newsgroup3 = (LinearLayout) view.findViewById(R.id.linearLayout3);
        view1 = (View) view.findViewById(R.id.view1);
        view2 = (View) view.findViewById(R.id.view2);
        view3 = (View) view.findViewById(R.id.view3);
        newsRec = (RecyclerView) view.findViewById(R.id.xx_main_news);

        randomList(newsList, cards, 5);

        banner.setImages(imageList).setImageLoader(new ImageLoader() {
            @Override
            public void displayImage(Context context, Object o, ImageView imageView) {
                Glide.with(context).load(o).centerCrop().into(imageView);
            }
        });
        banner.start();

        tool1.setOnClickListener(v -> startActivity(new Intent(getContext(), toolActivity1.class)));
        tool2.setOnClickListener(v -> startActivity(new Intent(getContext(), toolActivity2.class)));
        tool3.setOnClickListener(v -> startActivity(new Intent(getContext(), toolActivity3.class)));
        tool4.setOnClickListener(v -> startActivity(new Intent(getContext(), toolActivity4.class)));

        newsInit();

        newsgroup1.setOnClickListener(v -> {
            randomList(newsList, cards, 5);
            newsInit();
            view1.setVisibility(View.VISIBLE);
            view2.setVisibility(View.INVISIBLE);
            view3.setVisibility(View.INVISIBLE);
        });
        newsgroup1.setOnClickListener(v -> {
            randomList(newsList, cards, 5);
            newsInit();
            view2.setVisibility(View.VISIBLE);
            view1.setVisibility(View.INVISIBLE);
            view3.setVisibility(View.INVISIBLE);
        });
        newsgroup1.setOnClickListener(v -> {
            if (view3.getVisibility() != View.VISIBLE) {
                randomList(newsList, cards, 5);
                newsInit();
                view3.setVisibility(View.VISIBLE);
                view2.setVisibility(View.INVISIBLE);
                view1.setVisibility(View.INVISIBLE);
            }
        });
    }

    private void newsInit() {
        newsRec.setLayoutManager(linearLayoutManager);
        XxAdapter adapter = new XxAdapter(getContext(), R.layout.xx_main_news, newsList) {
            @Override
            public void init(View itemView, int position) {
                XxCard card = newsList.get(position);
                ImageView image = (ImageView) itemView.findViewById(R.id.newsImage);
                TextView title = (TextView) itemView.findViewById(R.id.newsTitle);
                TextView info = (TextView) itemView.findViewById(R.id.newsInfo);
                Glide.with(itemView).load(card.getImage()).into(image);
                title.setText(card.getTitle());
                info.setText(card.getInfo());
                itemView.setOnClickListener(v -> {
                    startActivity(new Intent(getContext(),newsInfoActivity.class));
                });
            }
        };
        newsRec.setAdapter(adapter);
    }

    private void randomList(List<XxCard> list, XxCard[] cards, int size) {
        list.clear();
        Random random = new Random();
        for (int i = 0; i < size; i++) {
            int index = random.nextInt(cards.length);
            list.add(cards[index]);
        }
    }
}