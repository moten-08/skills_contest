package com.example.smartcity1213.ui.news

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.smartcity1213.R
import com.example.smartcity1213.result.NewsComment
import com.example.smartcity1213.tools.BaseActivity
import com.example.smartcity1213.tools.RxAdapter
import com.example.smartcity1213.tools.off
import com.example.smartcity1213.tools.toast
import com.example.smartcity1213.ui.myself.LoginActivity
import kotlinx.android.synthetic.main.activity_comment.*

class CommentActivity : BaseActivity() {
    private val viewModel by lazy { ViewModelProviders.of(this).get(NewsViewModel::class.java) }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_comment)
        val id = intent.getIntExtra("newsId", 0)
        title = "评论列表$id"

        val sp = getSharedPreferences("data", MODE_PRIVATE)
        var token: String = sp.getString("token", "")!!
        Log.d("TAG", "onCreate: $token")

        rv_comment.layoutManager = LinearLayoutManager(this)
        viewModel.getCommentList(id)
        viewModel.commentResult.observe(this) {
            rv_comment.adapter = commentRvAdapter(it.rows, token)
        }
        comment_submit.setOnClickListener {
            token = sp.getString("token", "")!!
            // 判断是否登录
            if (token == "") {
                "尚未登录，无法发表评论".toast(this@CommentActivity)
                startActivity(Intent(this@CommentActivity, LoginActivity::class.java))
                return@setOnClickListener
            }
            // 评论接口
            val commentStr: String = comment_ed.text.toString().trim()
            if (commentStr == "") {
                "内容不可为空".toast(this@CommentActivity)
                return@setOnClickListener
            }
            comment_ed.text.clear()
            viewModel.putComment(id, commentStr, token)
            viewModel.baseResult.observe(this@CommentActivity) {
                viewModel.getCommentList(id)
                it.msg.toast(this@CommentActivity)
            }
            // 关闭输入框
            this@CommentActivity.off()
        }
    }

    private fun commentRvAdapter(rows: List<NewsComment.Row>, token: String): RxAdapter {
        return RxAdapter(rows, this, R.layout.item_comment) { root, position ->
            val userName: TextView = root.findViewById(R.id.comment_username)
            val content: TextView = root.findViewById(R.id.comment_content)
            val likeNum: TextView = root.findViewById(R.id.comment_like_num)
            val date: TextView = root.findViewById(R.id.comment_date)

            val likeIcon: ImageView = root.findViewById(R.id.comment_like_icon)
            val isLike: ImageView = root.findViewById(R.id.is_like)

            userName.text = rows[position].userName
            content.text = rows[position].content
            likeNum.text = rows[position].likeNum.toString()
            date.text = rows[position].commentDate

            likeIcon.setOnClickListener {
                likeIcon.visibility = View.GONE
                isLike.visibility = View.VISIBLE
                // 点赞接口
                viewModel.putLikeComment(rows[position].id, token)
                viewModel.baseResult.observe(this@CommentActivity) {
                    if (it.code == 200) {
                        likeNum.text = (rows[position].likeNum + 1).toString()
                    }
                    it.msg.toast(this@CommentActivity)
                }

            }

        }

    }
}