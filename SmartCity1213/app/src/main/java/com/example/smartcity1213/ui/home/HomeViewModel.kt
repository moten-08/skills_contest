package com.example.smartcity1213.ui.home

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.smartcity1213.result.AllService
import com.example.smartcity1213.result.BannerHome
import com.example.smartcity1213.result.NewsList
import com.example.smartcity1213.tools.HttpUtil
import com.example.smartcity1213.tools.MyCallBack
import com.example.smartcity1213.tools.jsonToModel

class HomeViewModel : ViewModel() {

    val bannerHome = MutableLiveData<BannerHome>()
    fun getBanner() {
        HttpUtil.get(
            "api/rotation/list",
            map = mapOf("type" to "2"),
            callback = MyCallBack {
                bannerHome.value = jsonToModel(it, BannerHome::class.java)
            })
    }


    val allService = MutableLiveData<AllService>()
    fun getAllService() {
        HttpUtil.get(
            "api/service/list",
            callback = MyCallBack {
                allService.value = jsonToModel(it, AllService::class.java)
            })
    }

    val newsList = MutableLiveData<NewsList>()
    fun getHotTheme() {
        HttpUtil.get(
            "press/press/list",
            map = mapOf("hot" to "Y"),
            callback = MyCallBack {
                newsList.value = jsonToModel(it, NewsList::class.java)
            })
    }
}