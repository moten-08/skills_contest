package com.example.smartcity1213.request

data class UpPass(
    var newPassword: String,
    var oldPassword: String
)