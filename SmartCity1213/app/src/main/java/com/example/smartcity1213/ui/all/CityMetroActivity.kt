package com.example.smartcity1213.ui.all

import android.os.Bundle
import com.example.smartcity1213.R
import com.example.smartcity1213.tools.BaseActivity

class CityMetroActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_city_metro)
        title = "城市地铁"
    }
}