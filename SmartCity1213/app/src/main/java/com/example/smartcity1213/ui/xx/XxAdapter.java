package com.example.smartcity1213.ui.xx;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public abstract class XxAdapter extends RecyclerView.Adapter<XxAdapter.ViewHolder> {
    private Context context;
    private int lay;
    private List<XxCard> cardList;

    public XxAdapter(Context context, int lay, List<XxCard> cardList) {
        this.context = context;
        this.lay = lay;
        this.cardList = cardList;
    }

    @NonNull
    @Override
    public XxAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(lay,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull XxAdapter.ViewHolder holder, int position) {
        init(holder.itemView,position);
    }

    public abstract void init(View itemView, int position);

    @Override
    public int getItemCount() {
        return cardList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}
