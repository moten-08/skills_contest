package com.example.smartcity1213.ui.myself

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.smartcity1213.result.UserInfo
import com.example.smartcity1213.tools.HttpUtil
import com.example.smartcity1213.tools.MyCallBack
import com.example.smartcity1213.tools.jsonToModel

class MyselfViewModel : ViewModel() {

    val userInfo = MutableLiveData<UserInfo>()
    fun getUserInfo(token: String) {
        HttpUtil.get(
            "api/common/user/getInfo",
            token = token,
            callback = MyCallBack {
                userInfo.value = jsonToModel(it,UserInfo::class.java)
            })
    }
}