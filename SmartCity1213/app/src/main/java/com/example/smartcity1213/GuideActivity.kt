package com.example.smartcity1213

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import com.example.smartcity1213.tools.RxAdapter
import com.example.smartcity1213.tools.open
import com.example.smartcity1213.tools.snack
import kotlinx.android.synthetic.main.activity_guide.*

class GuideActivity : AppCompatActivity() {
    private val indexLive = MutableLiveData<Int>().apply {
        value = 0
    }
    private val sp by lazy { getSharedPreferences("data", MODE_PRIVATE) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_guide)

        if (!sp.getBoolean("isFirst",true)){
            Intent(this,MainActivity::class.java).apply {
                startActivity(this)
            }
        }

        val guideList = listOf(
            R.mipmap.guide1,
            R.mipmap.guide2,
            R.mipmap.guide3,
            R.mipmap.guide4,
            R.mipmap.guide5,
        )
        val viewList = ArrayList<View>()
        repeat(guideList.size) {
            viewList.add(View.inflate(this, R.layout.item_g, null))
        }
        vp_g.apply {
            adapter = pagerAdapter(guideList, viewList)
            addOnPageChangeListener(onPageChangeListener())
        }
        rv_g.apply {
            layoutManager = LinearLayoutManager(this@GuideActivity, RecyclerView.HORIZONTAL, false)
            adapter = RxAdapter(guideList, this@GuideActivity, R.layout.item_g2) { root, position ->
                val point = root.findViewById<ImageView>(R.id.item_gr_p)
                indexLive.observe(this@GuideActivity) {
                    point.setImageResource(
                        if (it == position) R.drawable.ic_baseline_fiber_manual_record_24
                        else R.drawable.ic_baseline_fiber_manual_record_25
                    )
                }
                point.setOnClickListener { vp_g.currentItem = position }

            }
        }
        indexLive.observe(this) {
            toMain.apply {
                visibility = if (it == (guideList.size - 1)) View.VISIBLE else View.GONE
                setOnClickListener {
                    if (sp.getString("ip", "").toString().isEmpty()) {
                        "暂未设置ip地址".snack(toMain, "去设置") { showAlertDialog() }
                        return@setOnClickListener
                    }
                    if (sp.getString("port", "").toString().isEmpty()) {
                        "暂未设置端口".snack(toMain, "去设置") { showAlertDialog() }
                        return@setOnClickListener
                    }
                    Intent(this@GuideActivity, MainActivity::class.java).apply {
                        sp.open {
                            putBoolean("isFirst", false)
                        }
                        finish()
                        startActivity(this)
                    }
                }
            }
            settingInternet.apply {
                visibility = if (it == (guideList.size - 1)) View.VISIBLE else View.GONE
                setOnClickListener { showAlertDialog() }
            }
        }
    }

    private fun showAlertDialog() {
        val view = View.inflate(this, R.layout.dialog_internet, null)
        val dialog = AlertDialog.Builder(this)
            .setView(view)
            .create()
        val button = view.findViewById<Button>(R.id.save)
        val ip = view.findViewById<EditText>(R.id.settingIp)
        val port = view.findViewById<EditText>(R.id.settingPort)
        ip.setText(sp.getString("ip", ""))
        port.setText(sp.getString("port", ""))
        button.setOnClickListener {
            sp.open {
                putString("ip", ip.text.toString())
                putString("port", port.text.toString())
            }
            dialog.dismiss()
        }
        dialog.show()
    }

    private fun onPageChangeListener() = object : ViewPager.OnPageChangeListener {
        override fun onPageScrolled(
            position: Int,
            positionOffset: Float,
            positionOffsetPixels: Int
        ) = Unit

        override fun onPageSelected(position: Int) {
            indexLive.value = position
        }

        override fun onPageScrollStateChanged(state: Int) = Unit

    }

    private fun pagerAdapter(
        guideList: List<Int>,
        viewList: List<View>
    ) = object : PagerAdapter() {
        override fun getCount(): Int = guideList.size

        override fun isViewFromObject(view: View, `object`: Any): Boolean =
            `object` == view

        override fun instantiateItem(container: ViewGroup, position: Int): Any {
            val view = viewList[position]
            view.findViewById<ImageView>(R.id.item_gr).setImageResource(guideList[position])
            container.addView(view)
            return view
        }

        override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
            container.removeView(viewList[position])
        }
    }
}