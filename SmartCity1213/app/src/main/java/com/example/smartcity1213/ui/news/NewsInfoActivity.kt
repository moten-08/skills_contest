package com.example.smartcity1213.ui.news

import android.content.Intent
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.ViewModelProviders
import com.example.smartcity1213.R
import com.example.smartcity1213.tools.BaseActivity
import com.example.smartcity1213.tools.HttpUtil
import com.example.smartcity1213.tools.glide
import com.example.smartcity1213.tools.toast
import com.example.smartcity1213.ui.myself.LoginActivity
import kotlinx.android.synthetic.main.activity_news_info.*

class NewsInfoActivity : BaseActivity() {
    private val viewModel by lazy { ViewModelProviders.of(this).get(NewsViewModel::class.java) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_news_info)
        init()
    }

    override fun onRestart() {
        super.onRestart()
        init()
    }

    private fun init() {
        val id = intent.getIntExtra("newsId", 0)
        val intent = Intent(this, CommentActivity::class.java)
        intent.putExtra("newsId", id)

        // 获取新闻
        getData(id)
        // 评论及列表
        comment(id, intent)
    }

    private fun comment(id: Int, intent: Intent) {
        val token = getSharedPreferences("data", MODE_PRIVATE).getString("token", "")
        comment_submit.setOnClickListener {
            // 判断是否登录
            if (token.equals("")) {
                "尚未登录，无法发表评论".toast(this@NewsInfoActivity)
                startActivity(Intent(this@NewsInfoActivity, LoginActivity::class.java))
                return@setOnClickListener
            }
            // 评论接口
            val commentStr: String = comment_ed.text.toString().trim()
            if (commentStr == "") {
                "内容不可为空".toast(this@NewsInfoActivity)
                return@setOnClickListener
            }
            viewModel.putComment(id, commentStr, token!!)
            viewModel.baseResult.observe(this@NewsInfoActivity) {
                it.msg.toast(this@NewsInfoActivity)
                // 然后跳到评论列表
                if (it.code == 200) {
                    startActivity(intent)
                    comment_ed.text.clear()
                } else {
                    startActivity(Intent(this@NewsInfoActivity, LoginActivity::class.java))
                }
            }
            findViewById<ImageView>(R.id.comment_icon).setOnClickListener { startActivity(intent) }
            findViewById<ImageView>(R.id.comment_number).setOnClickListener { startActivity(intent) }
            // 点赞
            findViewById<ImageView>(R.id.new_like).setOnClickListener { likeThisNews(id, token) }
            findViewById<TextView>(R.id.like_number).setOnClickListener { likeThisNews(id, token) }
        }
    }

    private fun likeThisNews(id: Int, token: String) {
        if (token == "") {
            "尚未登录，无法点赞".toast(this@NewsInfoActivity)
            startActivity(Intent(this@NewsInfoActivity, LoginActivity::class.java))
            return
        }
        // 不让点赞
        findViewById<ImageView>(R.id.new_like).setImageResource(R.drawable.ic_like)
        findViewById<ImageView>(R.id.new_like).setOnClickListener {}
        findViewById<TextView>(R.id.like_number).setOnClickListener {}
        viewModel.likeThisNews(id, token)
        viewModel.baseResult.observe(this) {
            it.msg.toast(this)
            if (it.code == 200) getData(id)
            else startActivity(Intent(this@NewsInfoActivity, LoginActivity::class.java))
        }

    }

    private fun getData(id: Int) {
        viewModel.getNewInfo(id)
        viewModel.result.observe(this) {
            val html = it.data.content
                // 替换src里面缺失的图片地址
                .replace("/prod-api", "${HttpUtil.BASE_URL}/prod-api")
                // 图片加上样式保证适应页面
                .replace("<img", "<img style='max-width:100%;height:auto;'")
            this@NewsInfoActivity.title = it.data.title
            cover.glide(it.data.cover)
            content.loadDataWithBaseURL(HttpUtil.BASE_URL, html, "text/html", "utf-8", null)
            comment_number.text = it.data.commentNum.toString()
            like_number.text = it.data.likeNum.toString()

        }
    }
}