package com.example.smartcity1213.ui.home

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import com.example.smartcity1213.MainActivity
import com.example.smartcity1213.R
import com.example.smartcity1213.result.AllService
import com.example.smartcity1213.result.BannerHome
import com.example.smartcity1213.tools.*
import com.example.smartcity1213.ui.all.CityMetroActivity
import com.example.smartcity1213.ui.news.NewsInfoActivity
import com.youth.banner.loader.ImageLoader
import kotlinx.android.synthetic.main.fragment_home.*

class HomeFragment : Fragment() {

    private lateinit var homeViewModel: HomeViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        homeViewModel =
            ViewModelProviders.of(this).get(HomeViewModel::class.java)
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        searchHome.search()

        homeViewModel.apply {
            banner()
            allService()
            hotTheme()
        }


    }

    private fun HomeViewModel.banner() {
        getBanner()
        bannerHome.observe(viewLifecycleOwner) {
            this@HomeFragment.bannerHome.setImages(it.rows).setImageLoader(imageLoader())
                .start()
        }
    }

    private fun HomeViewModel.hotTheme() {
        getHotTheme()
        hotHome.layoutManager =
            GridLayoutManager(requireContext(), if (requireContext().isTablet()) 4 else 2)
        newsList.observe(viewLifecycleOwner) {
            hotHome.adapter =
                RxAdapter(it.rows, requireContext(), R.layout.item_hot) { root, position ->
                    val data = it.rows[position]
                    val coverHot = root.findViewById<ImageView>(R.id.cover_hot)
                    val titleHot = root.findViewById<TextView>(R.id.title_hot)

                    coverHot.glide(data.cover)
                    titleHot.text = data.title

                    root.setOnClickListener {
                        Intent(requireActivity(), NewsInfoActivity::class.java).apply {
                            putExtra("newsId", data.id)
                            startActivity(this)
                        }
                    }
                }
        }
    }

    private fun HomeViewModel.allService() {
        getAllService()
        serviceHome.layoutManager =
            GridLayoutManager(requireContext(), if (requireContext().isTablet()) 6 else 5)
        allService.observe(viewLifecycleOwner) {
            val list: MutableList<AllService.RowsDTO> = ArrayList()
            list.addAll(it.rows.subList(0, 10))
            serviceHome.adapter =
                RxAdapter(list, requireContext(), R.layout.item_service) { root, position ->
                    val data = it.rows[position]
                    val image = root.findViewById<ImageView>(R.id.imageAs)
                    val text = root.findViewById<TextView>(R.id.textAs)
                    image.glide(data.imgUrl)
                    text.text = data.serviceName
                    if (position == 9) {
                        image.glide(R.drawable.ic_baseline_more_horiz_24, true)
                        text.text = "更多服务"
                    }
                    root.setOnClickListener {
                        when (text.text.toString()) {
                            "更多服务" -> (requireActivity() as MainActivity).switch()
                            "城市地铁" -> {
                                Intent(requireContext(), CityMetroActivity::class.java).apply {
                                    startActivity(this)
                                }
                            }
                            else -> "敬请期待".toast(requireContext())
                        }
                    }

                }
        }
    }

    private fun imageLoader() = object : ImageLoader() {
        override fun displayImage(p0: Context, p1: Any, p2: ImageView) {
            val data = p1 as BannerHome.RowsDTO
            p2.glide(data.advImg)
            p2.setOnClickListener {
                Intent(requireActivity(), NewsInfoActivity::class.java).apply {
                    putExtra("newsId", data.id)
                    startActivity(this)
                }
            }
        }

    }

    private fun EditText.search() {
        setOnEditorActionListener { _, actionId, _ ->
            val str = searchHome.text.toString()
            if (str.trim().isEmpty()) {
                "搜索内容不能为空".toast(requireContext())
                return@setOnEditorActionListener false
            }
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                search(str)
                requireActivity().off()
            }
            true
        }
    }

    private fun search(str: String) {
        // 搜索
        Intent(requireContext(), SearchListActivity::class.java).apply {
            putExtra("search", str)
            putExtra("type", "news")
            startActivity(this)
        }
    }
}