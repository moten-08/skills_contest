package com.example.smartcity1213.result;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DictData {

    public String msg;
    public int code;
    public List<DataDTO> data;

    public static class DataDTO {
        public Object searchValue;
        public String createBy;
        public String createTime;
        public Object updateBy;
        public Object updateTime;
        public Object remark;
        public ParamsDTO params;
        public int dictCode;
        public int dictSort;
        public String dictLabel;
        public String dictValue;
        public String dictType;
        public Object cssClass;
        public Object listClass;
        public String isDefault;
        public String status;
        @SerializedName("default")
        public boolean defaultX;

        public static class ParamsDTO {
        }
    }
}
