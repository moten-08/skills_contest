package com.example.smartcity1213.ui.all

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.smartcity1213.R
import com.example.smartcity1213.result.AllService
import com.example.smartcity1213.tools.RxAdapter
import com.example.smartcity1213.tools.glide
import com.example.smartcity1213.tools.off
import com.example.smartcity1213.tools.toast
import com.example.smartcity1213.ui.home.HomeViewModel
import com.example.smartcity1213.ui.home.SearchListActivity
import kotlinx.android.synthetic.main.fragment_all_servie.*

class DashboardFragment : Fragment() {

    private lateinit var viewModel: HomeViewModel
    private val set = mutableSetOf<String>()
    private val indexType = MutableLiveData<Int>().apply {
        value = 0
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel =
            ViewModelProviders.of(this).get(HomeViewModel::class.java)
        return inflater.inflate(R.layout.fragment_all_servie, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        typeList.layoutManager = LinearLayoutManager(requireContext())
        serviceList.layoutManager = GridLayoutManager(requireContext(), 3)

        searchAll.search()

        viewModel.getAllService()
        viewModel.allService.observe(viewLifecycleOwner) {
            it.rows.forEach { item -> set.add(item.serviceType) }
            val list = set.toList()
            typeList.adapter = RxAdapter(
                list,
                requireContext(),
                android.R.layout.simple_selectable_list_item
            ) { root, position ->
                val textView = root.findViewById<TextView>(android.R.id.text1)
                textView.text = list[position]
                root.setOnClickListener { indexType.value = position }
                val drawable = ContextCompat.getDrawable(requireContext(), R.drawable.bg)
                drawable?.setBounds(0, 0, 400, 5)
                textView.setCompoundDrawables(null, null, null, drawable)
            }
            indexType.observe(viewLifecycleOwner) { index ->
                val list2: MutableList<AllService.RowsDTO> = ArrayList()
                it.rows.forEach { item ->
                    if (item.serviceType == list[index]) {
                        list2.add(item)
                    }
                }
                serviceList.adapter =
                    rxAdapter(list2)
            }

        }
    }

    private fun rxAdapter(list2: MutableList<AllService.RowsDTO>) =
        RxAdapter(list2, requireContext(), R.layout.item_service) { root, position ->
            val data = list2[position]
            val image = root.findViewById<ImageView>(R.id.imageAs)
            val text = root.findViewById<TextView>(R.id.textAs)
            image.glide(data.imgUrl)
            text.text = data.serviceName
            root.setOnClickListener {
                when (text.text.toString()) {
                    "城市地铁" -> {
                        Intent(requireContext(), CityMetroActivity::class.java).apply {
                            startActivity(this)
                        }
                    }
                    else -> "敬请期待".toast(requireContext())
                }
            }
        }

    private fun EditText.search() {
        setOnEditorActionListener { _, actionId, _ ->
            val str = this.text.toString()
            if (str.trim().isEmpty()) {
                "搜索内容不能为空".toast(requireContext())
                return@setOnEditorActionListener false
            }
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                search(str)
                requireActivity().off()
            }
            true
        }
    }

    private fun search(str: String) {
        // 搜索
        Intent(requireContext(), SearchListActivity::class.java).apply {
            putExtra("search", str)
            putExtra("type", "service")
            startActivity(this)
        }
    }
}
