package com.example.smartcity1213.ui.home

import android.content.Intent
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.smartcity1213.R
import com.example.smartcity1213.result.AllService
import com.example.smartcity1213.result.NewsList
import com.example.smartcity1213.tools.*
import com.example.smartcity1213.ui.all.CityMetroActivity
import com.example.smartcity1213.ui.news.NewsInfoActivity
import kotlinx.android.synthetic.main.activity_search_list.*

class SearchListActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_list)

        val data = intent.getStringExtra("search") ?: ""
        title = "\"$data\"的搜索结果"
        when (intent.getStringExtra("type")) {
            "news" -> {
                rv_search.layoutManager = LinearLayoutManager(this)
                getNewsList(data, rv_search)
            }
            "service" -> {
                rv_search.layoutManager = GridLayoutManager(this,5)
                getService(data, rv_search)
            }
        }
    }

    private fun getService(data: String, rvSearch: RecyclerView) {
        HttpUtil.get(
            "api/service/list",
            map = mapOf("serviceName" to data),
            callback = MyCallBack {
                val data = jsonToModel(it, AllService::class.java)
                runOnUiThread {
                    "共${data.total}条".toast(this)
                    rvSearch.adapter = rxAdapter(data.rows)
                }
            })
    }

    private fun rxAdapter(list2: List<AllService.RowsDTO>) =
        RxAdapter(list2, this, R.layout.item_service) { root, position ->
            val data = list2[position]
            val image = root.findViewById<ImageView>(R.id.imageAs)
            val text = root.findViewById<TextView>(R.id.textAs)
            image.glide(data.imgUrl)
            text.text = data.serviceName
            root.setOnClickListener {
                when (text.text.toString()) {
                    "城市地铁" -> {
                        Intent(this, CityMetroActivity::class.java).apply {
                            startActivity(this)
                        }
                    }
                    else -> "敬请期待".toast(this)
                }
            }
        }


    private fun getNewsList(type: String, view: RecyclerView) {
        HttpUtil.get(
            "press/press/list",
            map = mapOf("title" to type),
            callback = MyCallBack {
                val data = jsonToModel(it, NewsList::class.java)
                runOnUiThread {
                    "共${data.total}条".toast(this)
                    view.adapter = rxAdapterNews(data)
                }
            })
    }

    private fun rxAdapterNews(data: NewsList) = RxAdapter(
        data.rows,
        this,
        R.layout.item_news
    ) { root, position ->
        val data2 = data.rows[position]
        val title = root.findViewById<TextView>(R.id.titleNews)
        val image = root.findViewById<ImageView>(R.id.coverNews)
        val content = root.findViewById<TextView>(R.id.contentNews)
        val commentNum = root.findViewById<TextView>(R.id.commentNumNews)
        val date = root.findViewById<TextView>(R.id.publishDateNews)

        title.text = data2.title
        image.glide(data2.cover)
        content.text = data2.content.removeHTML()
        commentNum.format(data2.commentNum)
        date.format(data2.publishDate)

        root.setOnClickListener {
            Intent(this, NewsInfoActivity::class.java).apply {
                putExtra("newsId", data2.id)
                startActivity(this)
            }
        }
    }
}