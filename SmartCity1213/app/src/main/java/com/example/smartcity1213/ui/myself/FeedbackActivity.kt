package com.example.smartcity1213.ui.myself

import android.content.SharedPreferences
import android.graphics.Color
import android.os.Bundle
import androidx.core.widget.addTextChangedListener
import com.example.smartcity1213.R
import com.example.smartcity1213.request.Feedback
import com.example.smartcity1213.result.BaseResult
import com.example.smartcity1213.tools.*
import kotlinx.android.synthetic.main.activity_feedback.*

class FeedbackActivity : BaseActivity() {

    private lateinit var sp: SharedPreferences
    private lateinit var token: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_feedback)
        title = "意见反馈"

        sp = getSharedPreferences("data", MODE_PRIVATE)
        token = sp.getString("token", "") ?: ""

        addFeedback.addTextChangedListener {
            number.text =it.toString().length.toString()
            if (it.toString().length > 150) {
                number.setTextColor(Color.RED)
                numberHint.setTextColor(Color.RED)
            }
        }

        submitFeed.setOnClickListener {
            val content = addFeedback.text.toString()
            val title = titleEd.text.toString()
            if (content.length > 150) {
                "字数超出限制".toast(this)
                return@setOnClickListener
            }
            HttpUtil.post(
                "api/common/feedback",
                token = token,
                requestBody = Feedback(content, title),
                callback = MyCallBack {
                    val data = jsonToModel(it,BaseResult::class.java)
                    runOnUiThread{
                        data.msg.toast(applicationContext)
                        if (data.code == 200){
                            addFeedback.setText("")
                            titleEd.setText("")
                            finish()
                        }
                    }
                })
        }

    }
}