package com.example.smartcity1213.tools

import android.util.Log
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MyCallBack(val block: (Any) -> Unit) : Callback<Any> {
    override fun onResponse(p0: Call<Any>, p1: Response<Any>) {
        val body = p1.body()
        Log.d("MyCallBack", "$body")
        if (body == null) Log.e("MyCallBack", "body is null")
        else block(body)
    }

    override fun onFailure(p0: Call<Any>, p1: Throwable) = p1.printStackTrace()
}