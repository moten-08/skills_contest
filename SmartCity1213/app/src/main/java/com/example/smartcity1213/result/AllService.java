package com.example.smartcity1213.result;

import java.io.Serializable;
import java.util.List;

/**
 * 全部服务
 */
public class AllService implements Serializable {

    public int total;
    public List<RowsDTO> rows;
    public int code;
    public String msg;

    public static class RowsDTO implements Serializable {
        public Object searchValue;
        public Object createBy;
        public String createTime;
        public Object updateBy;
        public String updateTime;
        public Object remark;
        public ParamsDTO params;
        public int id;
        public String serviceName;
        public String serviceDesc;
        public String serviceType;
        public Object imgUrl;
        public int pid;
        public String link;
        public int sort;
        public String isRecommend;

        public static class ParamsDTO implements Serializable {
        }
    }
}
