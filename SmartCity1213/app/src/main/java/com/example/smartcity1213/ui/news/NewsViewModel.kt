package com.example.smartcity1213.ui.news

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.smartcity1213.request.NewsRequest
import com.example.smartcity1213.result.BaseResult
import com.example.smartcity1213.result.NewsCategoryList
import com.example.smartcity1213.result.NewsComment
import com.example.smartcity1213.result.NewsInfo
import com.example.smartcity1213.tools.HttpUtil
import com.example.smartcity1213.tools.MyCallBack
import com.example.smartcity1213.tools.jsonToModel

class NewsViewModel : ViewModel() {

    val newsCategory = MutableLiveData<NewsCategoryList>()
    fun getNewsTitle() {
        HttpUtil.get("press/category/list", callback = MyCallBack {
            newsCategory.value = jsonToModel(it, NewsCategoryList::class.java)
        })
    }

    val result = MutableLiveData<NewsInfo>()
    fun getNewInfo(id: Int) {
        HttpUtil.get(
            "press/press/$id",
            callback = MyCallBack {
                result.value = jsonToModel(it, NewsInfo::class.java)
                Log.d("TAG", "getNewInfo: ${result.value}")
            })
    }

    var baseResult = MutableLiveData<BaseResult>()
    fun putComment(id: Int, string: String, token: String) {
        HttpUtil.post(
            "press/pressComment",
            requestBody = NewsRequest(string, id),
            token = token,
            callback = MyCallBack {
                baseResult.value = jsonToModel(it, BaseResult::class.java)
            })
    }

    var commentResult = MutableLiveData<NewsComment>()
    fun getCommentList(id: Int) {
        HttpUtil.get(
            "press/comments/list",
            map = mapOf("newsId" to id.toString()),
            callback = MyCallBack {
                commentResult.value = jsonToModel(it, NewsComment::class.java)
            })
    }

    fun likeThisNews(id: Int, token: String) {
        HttpUtil.putTo(
            "press/press/like/$id",
            token = token,
            callback = MyCallBack {
                baseResult.value = jsonToModel(it, BaseResult::class.java)
            })
    }

    fun putLikeComment(id: Int, token: String) {
        HttpUtil.putTo(
            "press/pressComment/like/$id",
            token = token,
            callback = MyCallBack {
                baseResult.value = jsonToModel(it, BaseResult::class.java)
            }
        )
    }


}