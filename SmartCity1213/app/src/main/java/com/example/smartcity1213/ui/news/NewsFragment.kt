package com.example.smartcity1213.ui.news

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.PagerAdapter
import com.example.smartcity1213.R
import com.example.smartcity1213.result.NewsList
import com.example.smartcity1213.tools.*
import kotlinx.android.synthetic.main.fragment_news.*

class NewsFragment : Fragment() {

    private lateinit var newsViewModel: NewsViewModel
    private val viewList: MutableList<View> = ArrayList()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        newsViewModel =
            ViewModelProviders.of(this).get(NewsViewModel::class.java)
        return inflater.inflate(R.layout.fragment_news, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        newsViewModel.apply {
            getNewsTitle()
            newsCategory.observe(viewLifecycleOwner) {
                val data = it.data
                data.forEach { item ->
                    val view2 = View.inflate(requireContext(), R.layout.item_news_f, null)
                    view2.findViewById<RecyclerView>(R.id.itemNF).apply {
                        layoutManager = LinearLayoutManager(requireContext())
                        getNewsList(item.id, this)
                    }
                    viewList.add(view2)
                }
                vpNews.adapter = pagerAdapter()
                repeat(viewList.size) { index ->
                    tabNews.getTabAt(index)?.text = newsCategory.value?.data?.get(index)?.name
                }
            }

        }
        tabNews.setupWithViewPager(vpNews)
    }

    private fun pagerAdapter() = object : PagerAdapter() {
        override fun getCount(): Int = viewList.size

        override fun isViewFromObject(view: View, `object`: Any): Boolean =
            `object` == view

        override fun instantiateItem(container: ViewGroup, position: Int): Any {
            val view = viewList[position]
            container.addView(view)
            return view
        }

        override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
            container.removeView(viewList[position])
        }
    }

    private fun getNewsList(type: Int, view: RecyclerView) {
        HttpUtil.get(
            "press/press/list",
            map = mapOf("type" to type.toString()),
            callback = MyCallBack {
                val data = jsonToModel(it, NewsList::class.java)
                requireActivity().runOnUiThread {
                    view.adapter = RxAdapter(
                        data.rows,
                        requireContext(),
                        R.layout.item_news
                    ) { root, position ->
                        val data2 = data.rows[position]
                        val title = root.findViewById<TextView>(R.id.titleNews)
                        val image = root.findViewById<ImageView>(R.id.coverNews)
                        val content = root.findViewById<TextView>(R.id.contentNews)
                        val commentNum = root.findViewById<TextView>(R.id.commentNumNews)
                        val date = root.findViewById<TextView>(R.id.publishDateNews)

                        title.text = data2.title
                        image.glide(data2.cover)
                        content.text = data2.content.removeHTML()
                        commentNum.text = "评论数:${data2.commentNum}\n" +
                                "观看人数:${data2.readNum}\n" +
                                "点赞数:${data2.likeNum}"
                        date.format(data2.publishDate)

                        root.setOnClickListener {
                            Intent(requireActivity(), NewsInfoActivity::class.java).apply {
                                putExtra("newsId", data2.id)
                                startActivity(this)
                            }
                        }
                    }
                }
            })
    }

}