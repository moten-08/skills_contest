package com.example.smartcity1213.ui.myself

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.example.smartcity1213.MainActivity
import com.example.smartcity1213.R
import com.example.smartcity1213.tools.format
import com.example.smartcity1213.tools.glide
import com.example.smartcity1213.tools.snack

class MyselfFragment : Fragment() {


    private lateinit var viewModel: MyselfViewModel
    private lateinit var sp: SharedPreferences
    private lateinit var token: String

    private lateinit var username: TextView
    private lateinit var avatar: ImageView
    private lateinit var logout: Button
    private lateinit var ms1: TextView
    private lateinit var ms2: TextView
    private lateinit var ms3: TextView
    private lateinit var ms4: TextView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProviders.of(this).get(MyselfViewModel::class.java)
        sp = requireActivity().getSharedPreferences("data", Context.MODE_PRIVATE)
        return inflater.inflate(R.layout.fragment_myself, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initView(view)
        token = sp.getString("token", "") ?: ""
        if (token.trim().isEmpty()) {
            "暂未登录或登录状态失效".snack(logout, "登录") {
                startActivity(Intent(requireContext(), LoginActivity::class.java))
            }
        }
        viewModel.apply {
            getUserInfo(token)
            userInfo.observe(viewLifecycleOwner) {
                if (it.code == 200) {
                    username.format(it.user.userName)
                    if ("/prod-api" in it.user.avatar) {
                        avatar.glide(it.user.avatar, circle = true)
                    } else {
                        avatar.glide("/prod-api" + it.user.avatar, circle = true)
                    }
                }
                ms1.setOnClickListener { _ ->
                    Intent(requireContext(), UserInfoActivity::class.java).apply {
                        putExtra("userInfo", it.user)
                        startActivity(this)
                    }
                }
                ms2.setOnClickListener {
                    startActivity(Intent(requireContext(), OrderListActivity::class.java))
                }
                ms3.setOnClickListener {
                    startActivity(Intent(requireContext(), UpPasswordActivity::class.java))
                }
                ms4.setOnClickListener {
                    startActivity(Intent(requireContext(), FeedbackActivity::class.java))
                }
            }
        }

        logout.setOnClickListener {
            val edit = sp.edit()
            edit.remove("token")
            edit.apply()
            onResume()
            (requireContext() as MainActivity).switch(R.id.navigation_myself)
        }
    }

    private fun initView(view: View) {
        username = view.findViewById(R.id.username)
        avatar = view.findViewById(R.id.avatar)
        logout = view.findViewById(R.id.logout)

        ms1 = view.findViewById(R.id.ms1)
        ms2 = view.findViewById(R.id.ms2)
        ms3 = view.findViewById(R.id.ms3)
        ms4 = view.findViewById(R.id.ms4)

    }

    override fun onResume() {
        super.onResume()
        token = sp.getString("token", "") ?: ""
        viewModel.getUserInfo(token)
    }
}