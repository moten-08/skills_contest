package com.moten.smartCity.models.result.main

/**
 * 全部服务
 */
data class Service(
    val code: Int,
    val msg: String,
    val rows: List<Row>,
    val total: Int
) {
    class Row(
        val createBy: Any?,
        val createTime: String?,
        val id: Int?,
        var imgUrl: Any?,
        val isRecommend: String?,
        var link: String?,
        val params: Params?,
        val pid: Int?,
        val remark: Any?,
        val searchValue: Any?,
        val serviceDesc: String?,
        var serviceName: String?,
        val serviceType: String?,
        val sort: Int?,
        val updateBy: Any?,
        val updateTime: String?
    ) {
        class Params
    }
}
