package com.moten.smartCity.models.request

/**
 * 新闻评论请求
 */
data class NewsRequest(
    var content: String,
    var newsId: Int
)