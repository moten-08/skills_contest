package com.moten.smartCity.models.request

data class PasswordUp(
    var newPassword: String,
    var oldPassword: String
)
