package com.moten.smartCity.ui.module.movie

import android.os.Bundle
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.navigation.NavigationBarView
import com.moten.smartCity.R
import com.moten.smartCity.databinding.ActivityMovieBinding
import com.moten.smartCity.tools.BaseActivity

class MovieActivity : BaseActivity() {

    private lateinit var binding: ActivityMovieBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMovieBinding.inflate(layoutInflater)
        setContentView(binding.root)

        title = "看电影"

        val navView: BottomNavigationView = binding.navView

        val fragment: NavHostFragment =
            supportFragmentManager.findFragmentById(R.id.nav_host_fragment_activity_movie) as NavHostFragment
        val navController = fragment.navController
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        val appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.navigation_home_movie,
                R.id.navigation_cinema_movie,
                R.id.navigation_movie_movie
            )
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)
        navView.labelVisibilityMode = NavigationBarView.LABEL_VISIBILITY_LABELED

        // supportActionBar修改过,需要重新设置右上返回键
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
    }
}
