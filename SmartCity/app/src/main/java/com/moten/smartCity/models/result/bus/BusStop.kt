package com.moten.smartCity.models.result.bus

/**
 * 巴士站点
 */
data class BusStop(
    var code: Int,
    var msg: String,
    var rows: List<Row>,
    var total: Int
) {
    data class Row(
        var createBy: Any?,
        var createTime: Any?,
        var linesId: Int,
        var name: String,
        var params: Params,
        var remark: Any?,
        var searchValue: Any?,
        var sequence: Int,
        var stepsId: Int,
        var updateBy: Any?,
        var updateTime: Any?
    ) {
        class Params
    }
}