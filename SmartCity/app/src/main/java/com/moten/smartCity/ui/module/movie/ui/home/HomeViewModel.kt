package com.moten.smartCity.ui.module.movie.ui.home

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.moten.smartCity.models.result.movie.*
import com.moten.smartCity.tools.HttpUtil
import com.moten.smartCity.tools.HttpUtil.toModel
import com.moten.smartCity.tools.MyCallback

class HomeViewModel : ViewModel() {

    // 广告轮播
    val bannerData = MutableLiveData<List<BannerHome.Row>>()
    fun getBanner() {
        HttpUtil.service.get(
            "/api/movie/rotation/list",
            map = mapOf("type" to "2"),
        ).enqueue(MyCallback {
            bannerData.value = toModel(BannerHome::class.java).rows
            Log.d("TAG", "getBanner: ${bannerData.value}")
        })
    }

    // 地理位置
    val local = MutableLiveData<Location>()
    fun getLocal() {
        HttpUtil.service.get(
            "/api/common/gps/location",
        ).enqueue(MyCallback {
            local.value = toModel(Location::class.java)
        })
    }

    // 今日天气
    val weather = MutableLiveData<TodayWeather>()
    fun getWeather() {
        HttpUtil.service.get(
            "/api/common/weather/today",
        ).enqueue(MyCallback {
            weather.value = toModel(TodayWeather::class.java)
        })
    }

    // 热映电影
    val movies = MutableLiveData<MovieList>()
    fun getHotMovie() {
        HttpUtil.service.get(
            "/api/movie/film/list",
        ).enqueue(MyCallback {
            movies.value = toModel(MovieList::class.java)
        })
    }

    // 即将上映
    val wantMovies = MutableLiveData<MovieList>()
    fun getWantMovie() {
        HttpUtil.service.get(
            "/api/movie/film/preview/list",
        ).enqueue(MyCallback {
            wantMovies.value = toModel(MovieList::class.java)
        })
    }

    // 附近影院
    val cinemas = MutableLiveData<Cinema>()
    fun getNearCinema() {
        HttpUtil.service.get(
            "/api/movie/theatre/list",
        ).enqueue(MyCallback {
            cinemas.value = toModel(Cinema::class.java)
        })
    }
}
