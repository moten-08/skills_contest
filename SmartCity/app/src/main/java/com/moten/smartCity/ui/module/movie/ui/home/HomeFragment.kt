package com.moten.smartCity.ui.module.movie.ui.home

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.moten.smartCity.R
import com.moten.smartCity.databinding.FragmentHomeMovieBinding
import com.moten.smartCity.databinding.ItemCinameBinding
import com.moten.smartCity.models.result.movie.BannerHome
import com.moten.smartCity.models.result.movie.MovieList
import com.moten.smartCity.tools.RxAdapter
import com.moten.smartCity.tools.glide
import com.youth.banner.adapter.BannerImageAdapter
import com.youth.banner.holder.BannerImageHolder
import com.youth.banner.indicator.RoundLinesIndicator

class HomeFragment : Fragment() {

    private lateinit var homeViewModel: HomeViewModel
    private lateinit var binding: FragmentHomeMovieBinding

    // This property is only valid between onCreateView and
    // onDestroyView.

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        homeViewModel =
            ViewModelProvider(this)[HomeViewModel::class.java]

        binding = FragmentHomeMovieBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initView()
        initData()
        dataBindingView(homeViewModel)
    }

    private fun dataBindingView(it: HomeViewModel) {
        // 电影广告轮播
        it.bannerData.observe(viewLifecycleOwner) {
            binding.banner.apply {
                // banner2.1.0
                adapter = object : BannerImageAdapter<BannerHome.Row>(it) {
                    override fun onBindView(
                        p0: BannerImageHolder,
                        p1: BannerHome.Row,
                        p2: Int,
                        p3: Int
                    ) {
                        p0.imageView.glide(p1.advImg).into(p0.imageView)
                    }
                }
                setLoopTime(2500)
                scrollTime = 500
                indicator = RoundLinesIndicator(requireContext())
                setOnBannerListener { _, position ->
                    Intent(requireContext(), MovieInfoActivity::class.java).apply {
                        putExtra("movieId", it[position].targetId)
                        startActivity(this)
                    }
                }
                // banner1.4.10
//                setImages(it)
//                setImageLoader(object : ImageLoader() {
//                    override fun displayImage(p0: Context, p1: Any, p2: ImageView) {
//                        val dto = p1 as BannerHome.RowsDTO
//                        p2.glide(dto.advImg).into(p2)
//                    }
//                })
//                setOnBannerListener { position ->
//                    Intent(requireActivity(), MovieInfoActivity::class.java).apply {
//                        putExtra("movieId", it[position].targetId)
//                        startActivity(this)
//                    }
//                }
//                setIndicatorGravity(BannerConfig.CENTER)
//                start()
            }
        }
        // 地理位置
        it.local.observe(viewLifecycleOwner) {
            binding.location.text =
                with(it.data) { String.format("%s-%s-%s-%s", province, city, area, location) }
        }
        // 天气
        it.weather.observe(viewLifecycleOwner) {
            binding.weather.apply {
                text = with(it.data) {
                    String.format(
                        "天气：%s\t温度：%s℃\t湿度：%s\t空气质量:%s",
                        weather,
                        temperature,
                        humidity,
                        air
                    )
                }
                val drawable = ResourcesCompat.getDrawable(
                    resources,
                    when (it.data.weather) {
                        "晴" -> R.drawable.ic_baseline_wb_sunny_24
                        "多云" -> R.drawable.ic_baseline_cloud_24
                        else -> R.drawable.ic_baseline_wb_sunny_24
                    },
                    requireActivity().theme
                )?.apply {
                    setBounds(0, 0, this.minimumWidth, this.minimumHeight)
                }
                setCompoundDrawables(drawable, null, null, null)
            }
        }
        // 热映电影
        it.movies.observe(viewLifecycleOwner) {
            binding.hotPlay.adapter =
                RxAdapter(it.rows, requireContext(), R.layout.item_movie) { root, position ->
                    val (buy: Button, data) = pair(root, it, position)
                    buy.setOnClickListener {
                        Intent(requireContext(),MovieInfoActivity::class.java).apply {
                            putExtra("movieId",data.id)
                            startActivity(this)
                        }
                    }
                }
        }
        // 即将上映
        it.wantMovies.observe(viewLifecycleOwner) {
            binding.wantPlay.adapter =
                RxAdapter(it.rows, requireContext(), R.layout.item_movie) { root, position ->
                    val (buy: Button, data) = pair(root, it, position)
                    buy.apply {
                        text = "想看"
                        setOnClickListener {
                            Toast.makeText(requireContext(), "收藏${data.id}", Toast.LENGTH_SHORT)
                                .show()
                        }
                    }
                }
        }
        // 周边影院
        it.cinemas.observe(viewLifecycleOwner) {
            binding.nearCinema.adapter =
                RxAdapter(it.rows, requireContext(), R.layout.item_ciname) { root, position ->
                    val rBinding: ItemCinameBinding = ItemCinameBinding.bind(root)
                    it.rows[position].let { data ->
                        rBinding.apply {
                            val addressStr = data.city + data.area + data.address
                            address.text = addressStr
                            cover.glide(data.cover).into(cover)
                            name.text = data.name
                            scoreU.rating = (data.score / 20).toFloat()
                        }
                    }

                }
        }


    }

    private fun pair(
        root: View,
        it: MovieList,
        position: Int
    ): Pair<Button, MovieList.Row> {
        val cover: ImageView = root.findViewById(R.id.cover)
        val name: TextView = root.findViewById(R.id.name)
        val buy: Button = root.findViewById(R.id.buy_ticket)
        val data = it.rows[position]
        cover.glide(data.cover).into(cover)
        name.text = data.name
        return Pair(buy, data)
    }

    private fun initData() {
        // 数据初始化
        homeViewModel.apply {
            getBanner()
            getLocal()
            getWeather()
            getHotMovie()
            getWantMovie()
            getNearCinema()
        }
    }

    private fun initView() {
        // 搜索框
        binding.searchMovie.apply {
            setOnEditorActionListener { _, actionId, _ ->
                val searchString = this.text.toString()
                if (searchString.trim().isEmpty()) Toast.makeText(
                    requireContext(),
                    "搜索内容不能为空",
                    Toast.LENGTH_SHORT
                ).show()
                else if (actionId == EditorInfo.IME_ACTION_SEARCH) search(searchString)
                true
            }
        }
        // 快捷功能入口
        binding.apply {
            service1.setOnClickListener { }
            service2.setOnClickListener { }
            service3.setOnClickListener { }
            service4.setOnClickListener { }
            hotPlay.layoutManager =
                LinearLayoutManager(requireContext(), RecyclerView.HORIZONTAL, false)
            wantPlay.layoutManager =
                LinearLayoutManager(requireContext(), RecyclerView.HORIZONTAL, false)
            nearCinema.layoutManager = LinearLayoutManager(requireContext())
        }
    }

    private fun search(searchString: String) {
        Toast.makeText(
            requireContext(),
            "搜索$searchString",
            Toast.LENGTH_SHORT
        ).show()
    }

}
