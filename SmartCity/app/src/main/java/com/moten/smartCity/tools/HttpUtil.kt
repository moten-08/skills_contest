package com.moten.smartCity.tools

import android.util.Log
import com.google.gson.Gson
import okhttp3.HttpUrl.Companion.toHttpUrl
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.OkHttpClient
import okhttp3.RequestBody.Companion.toRequestBody
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object HttpUtil {

    // 本地服务器
    const val baseUrl1 = "http://10.10.230.112:10001"

    // 公网服务器--大连
    const val baseUrl2 = "http://124.93.196.45:10001"

    // 公网服务器--大连，已关闭
//  const val baseUrl3 = "http://222.207.204.15:10001"
    /**
     * 普通JSON参数
     */
    private val MEDIA_TYPE = "application/json".toMediaTypeOrNull()

    /**
     * 文件参数
     */
    private val MEDIA_FILE = "application/octet-stream".toMediaTypeOrNull()
    private val GSON = Gson()
    var BASE_URL: String = baseUrl2
        set(value) {
            // 修改baseUrl时打印日志
            Log.i("Update BaseUrl:", value)
            field = value
        }

    /**
     * OkHttp客户端,用于实现一些高端操作
     */
    private val okHttpClient = OkHttpClient.Builder()
        // 添加拦截器实现真正的baseUrl的修改
        .addInterceptor {
            // 获取request实例
            val request = it.request()
            // 获取request的创建者
            val builder = request.newBuilder()
            // 获取旧url示例
            val ordUrl = request.url
            // url.toHttpUrl(): HttpUrl.parse(url)的替换,该方法已弃用
            val newUrl = BASE_URL.toHttpUrl().run {
                // 重构url,协议 地址 端口
                ordUrl.newBuilder()
                    .scheme(scheme)
                    .host(host)
                    .port(port)
                    .build()
            }
            it.proceed(builder.url(newUrl).build())
        }
        .build()

    /**
     * Retrofit请求实例
     *
     * 其中client若为空(不写client(OkHttpClient))
     * 则会默认构建一个OkHttpClient
     *
     * 看了源码,确实如此
     */
    val service: FitService = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .client(okHttpClient)
        .build()
        .create(FitService::class.java)
        get() {
            Log.d("Now baseUrl", BASE_URL)
            return field
        }

    fun <T> Any.toModel(clazz: Class<T>): T = GSON.fromJson(GSON.toJson(this), clazz)

    /**
     * 封装一下put请求
     */
    fun putTo(
        url: String,
        token: String = "",
        request: Any = ""
    ) = service.put(url, token, body = GSON.toJson(request).toRequestBody(MEDIA_TYPE))

    /**
     * 封装一下post请求
     */
    fun postTo(
        url: String,
        token: String = "",
        request: Any = ""
    ) = service.post(url, GSON.toJson(request).toRequestBody(MEDIA_TYPE), token)

    /**
     * 封装一下文件上传
     */
    fun uploadTo(
        token: String,
        bytes: ByteArray,
    ) = service.upload(
        token,
        MultipartBody.Builder()
            .setType(MultipartBody.FORM)
            .addFormDataPart(
                "file", "file.jpeg",
                bytes.toRequestBody(MEDIA_FILE)
            ).build().parts
    )
}

