package com.moten.smartCity.ui.module.smartBus

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.LinearLayoutManager
import com.moten.smartCity.R
import com.moten.smartCity.databinding.ActivitySmartBusBinding
import com.moten.smartCity.databinding.ItemBusBinding
import com.moten.smartCity.models.result.bus.BusLine
import com.moten.smartCity.models.result.bus.BusStop
import com.moten.smartCity.tools.*
import com.moten.smartCity.tools.HttpUtil.toModel
import com.moten.smartCity.ui.module.OrderListActivity

class SmartBusActivity : BaseActivity() {
    private val binding by lazy { ActivitySmartBusBinding.inflate(layoutInflater) }
    private val busListLivedata = MutableLiveData<BusLine>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        title = "智慧巴士"

        busListLivedata.observe(this) {
            binding.root.apply {
                layoutManager = LinearLayoutManager(this@SmartBusActivity)
                adapter = rxAdapter(it)
            }
        }

        getBusLine()

    }

    private fun rxAdapter(it: BusLine) =
        RxAdapter(it.rows, this@SmartBusActivity, R.layout.item_bus) { root, position ->
            val data = it.rows[position]
            val rBinding = ItemBusBinding.bind(root)
            rBinding.apply {
                name.text = data.name
                firstToEnd.format(data.first, data.end)
                timeStartToEnd.format(data.startTime, data.endTime)
                price.format(data.price)
                mileage.format(data.mileage)
                rv.layoutManager = LinearLayoutManager(this@SmartBusActivity)
                getBusStop(data.id, this)
                showMore.setOnClickListener {
                    rv.visibility = View.VISIBLE
                    showMoreOff.visibility = View.VISIBLE
                    it.visibility = View.INVISIBLE
                }
                showMoreOff.setOnClickListener {
                    rv.visibility = View.GONE
                    showMore.visibility = View.VISIBLE
                    it.visibility = View.INVISIBLE
                }
                root.setOnClickListener {
                    Intent(this@SmartBusActivity, Custom1Activity::class.java).apply {
                        putExtra("busData", data)
                        startActivity(this)
                    }
                }
            }
        }

    private fun getBusStop(id: Int, rBinding: ItemBusBinding) {
        HttpUtil.service.get(
            "/api/bus/stop/list",
            map = mapOf("linesId" to id.toString())
        ).enqueue(MyCallback {
            val busStop = toModel(BusStop::class.java)
            rBinding.rv.adapter = RxAdapter(
                busStop.rows,
                this,
                android.R.layout.simple_list_item_1
            ) { root, position ->
                val text = root.findViewById<TextView>(android.R.id.text1)
                val data = busStop.rows[position]
                val str = data.name +
                        (when (position) {
                            0 -> "\t\t\t\t起点"
                            (busStop.rows.size - 1) -> "\t\t\t\t终点"
                            else -> ""
                        })
                text.text = str
            }
        })
    }

    private fun getBusLine() {
        HttpUtil.service.get(
            "/api/bus/line/list"
        ).enqueue(MyCallback {
            busListLivedata.value = toModel(BusLine::class.java)
        })
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.my_order) startActivity(Intent(this, OrderListActivity::class.java))
        return super.onOptionsItemSelected(item)
    }
}
