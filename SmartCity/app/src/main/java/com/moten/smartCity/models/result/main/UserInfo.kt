package com.moten.smartCity.models.result.main

import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * 用户信息
 */
data class UserInfo(
    @SerializedName("msg") val msg: String,
    @SerializedName("code") val code: Int,
    @SerializedName("user") val user: User
) : Serializable {
    data class User(
        @SerializedName("userId") val userId: Int,
        @SerializedName("userName") val userName: String,
        @SerializedName("nickName") val nickName: String,
        @SerializedName("email") val email: String,
        @SerializedName("phonenumber") val phonenumber: String,
        @SerializedName("sex") val sex: String,
        @SerializedName("avatar") val avatar: String,
        @SerializedName("idCard") val idCard: Any?,
        @SerializedName("balance") val balance: Double,
        @SerializedName("score") val score: Int
    ) : Serializable
}
