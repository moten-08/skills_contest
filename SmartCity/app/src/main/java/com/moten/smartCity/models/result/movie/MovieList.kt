package com.moten.smartCity.models.result.movie

/**
 * 电影列表（热映和即将上映）
 * @video 预告特有
 */
data class MovieList(
    var code: Int,
    var msg: String,
    var rows: List<Row>,
    var total: Int
) {
    data class Row(
        var category: String,
        var cover: String,
        var createBy: Any?,
        var createTime: Any?,
        var duration: Int,
        var favoriteNum: Int,
        var id: Int,
        var introduction: String,
        var language: String,
        var likeNum: Int,
        var name: String,
        var other: Any?,
        var params: Params,
        var playDate: String,
        var playType: String,
        var recommend: String,
        var remark: Any?,
        var score: Int,
        var searchValue: Any?,
        var updateBy: Any?,
        var updateTime: Any?,

        val video: String?
    ) {
        class Params
    }
}