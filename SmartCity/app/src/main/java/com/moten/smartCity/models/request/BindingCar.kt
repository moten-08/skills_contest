package com.moten.smartCity.models.request

/**
 * 车辆绑定
 */
data class BindingCar(
    var engineNo: String,
    var plateNo: String,
    var type: String
)