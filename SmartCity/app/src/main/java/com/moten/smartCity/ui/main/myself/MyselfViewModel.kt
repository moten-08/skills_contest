package com.moten.smartCity.ui.main.myself

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.moten.smartCity.models.request.LoginRequest
import com.moten.smartCity.models.result.main.Login
import com.moten.smartCity.models.result.main.UserInfo
import com.moten.smartCity.tools.HttpUtil
import com.moten.smartCity.tools.HttpUtil.toModel
import com.moten.smartCity.tools.MyCallback

class MyselfViewModel : ViewModel() {

    val text = MutableLiveData<String>()

    val loginResult = MutableLiveData<Login>()
    // 登录
    fun login(loginRequest: LoginRequest) {
        HttpUtil.postTo(
            "/api/login",
            request = loginRequest
        ).enqueue(MyCallback {
            loginResult.value =
                it.toModel(Login::class.java)
        })
    }

    // 获取用户信息
    val userInfoLive = MutableLiveData<UserInfo>()
    fun getUserInfo(token: String) {
        HttpUtil.service.get(
            "api/common/user/getInfo",
            token
        ).enqueue(MyCallback {
            userInfoLive.value = it.toModel(UserInfo::class.java)
        })
    }
}
