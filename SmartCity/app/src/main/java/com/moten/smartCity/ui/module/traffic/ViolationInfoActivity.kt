package com.moten.smartCity.ui.module.traffic

import android.os.Bundle
import android.view.View
import androidx.lifecycle.MutableLiveData
import com.moten.smartCity.databinding.ActivityViolationInfoBinding
import com.moten.smartCity.models.result.BaseResult
import com.moten.smartCity.models.result.traffic.ViolationInfo
import com.moten.smartCity.tools.*
import com.moten.smartCity.tools.HttpUtil.toModel

class ViolationInfoActivity : BaseActivity() {
    private val binding by lazy { ActivityViolationInfoBinding.inflate(layoutInflater) }
    private val liveData = MutableLiveData<ViolationInfo>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        title = "违章详情"

        liveData.observe(this) {
            binding.apply {
                badTime.format(it.data.badTime)
                illegalSites.format(it.data.illegalSites)
                trafficOffence.format(it.data.trafficOffence)
                noticeNo.format(it.data.noticeNo)
                deductMarks.format(it.data.deductMarks)
                money.format(it.data.money)
                if (it.data.disposeState == "已处理未缴款") {
                    pay.text = "缴款"
                    pay.visibility = View.VISIBLE
                    pay.setOnClickListener { _ ->
                        HttpUtil.postTo(
                            "/api/traffic/illegal/pay/${it.data.id}",
                            token = token
                        ).enqueue(MyCallback {
                            toModel(BaseResult::class.java).msg.toast(this@ViolationInfoActivity)
                        })
                    }

                }
            }

        }
        getData(intent.getIntExtra("id", 0))

    }

    private fun getData(id: Int) {
        HttpUtil.service.get(
            "/api/traffic/illegal/${id}",
            token = token
        ).enqueue(MyCallback {
            liveData.value = toModel(ViolationInfo::class.java)
        })
    }
}
