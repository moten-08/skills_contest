package com.moten.smartCity.ui.main.news

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.moten.smartCity.R
import com.moten.smartCity.databinding.ItemNewsBinding
import com.moten.smartCity.databinding.ItemNewsListBinding
import com.moten.smartCity.models.result.news.NewsList
import com.moten.smartCity.tools.*
import com.moten.smartCity.ui.main.home.HomeViewModel

// 搜索结果列表
class NListActivity : BaseActivity() {
    private val binding: ItemNewsListBinding by lazy {
        ItemNewsListBinding.inflate(
            layoutInflater
        )
    }
    private lateinit var viewModel: HomeViewModel
    private val newsList: MutableLiveData<List<NewsList.Row>> = MutableLiveData()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        val searchText = intent.getStringExtra("search") ?: ""
        title = "\"$searchText\"的搜索结果"

        initView()
        viewModel = ViewModelProvider(this).get(HomeViewModel::class.java)

        viewModel.getNews()
        viewModel.newsInfoList.observe(this) {
            val list: MutableList<NewsList.Row> = ArrayList()
            it.forEach { item ->
                val boolean: Boolean = with(item) {
                    title.contains(searchText) or content.substring(0, 40).contains(searchText)
                }
                if (boolean) {
                    list.add(item)
                }
            }
            newsList.value = list
            if (list.size < 1) binding.hint.apply {
                text = "暂无数据"
                visibility = View.VISIBLE
            }
        }


    }

    private fun initView() {
        newsList.observe(this) {
            binding.rvNewsInfo.apply {
                layoutManager = LinearLayoutManager(this@NListActivity)
                adapter = rxAdapter(it)
            }
        }

    }

    private fun RecyclerView.rxAdapter(it: List<NewsList.Row>) =
        RxAdapter(it, context, R.layout.item_news) { root, position ->
            root.setOnClickListener { _ ->
                Intent(context, ToolActivity::class.java).apply {
                    putExtra("newId", it[position].id)
                    context.startActivity(this)
                }
            }
            val binding: ItemNewsBinding = ItemNewsBinding.bind(root)
            binding.apply {
                newsCover.glide(it[position].cover).into(newsCover)
                newsTitle.text = it[position].title
                newsLikeNumber.text = it[position].likeNum.toString()
                newsDate.text = it[position].publishDate
                newsReadNumber.text = it[position].readNum.toString()
                newsCommentNum.format(it[position].commentNum)
                newsContent.text = it[position].content.removeHTML()
            }
        }
}
