package com.moten.smartCity.models.result.news

data class NewsInfo(
    val code: Int,
    val `data`: Data,
    val msg: String
) {
    data class Data(
        val appType: String,
        val commentNum: Int,
        val content: String,
        val cover: String,
        val createBy: String,
        val createTime: String,
        val hot: String,
        val id: Int,
        val likeNum: Int,
        val params: Params,
        val publishDate: String,
        val readNum: Int,
        val remark: Any,
        val searchValue: Any,
        val status: String,
        val subTitle: Any,
        val tags: Any,
        val title: String,
        val top: String,
        val type: String,
        val updateBy: String,
        val updateTime: String
    ) {
        class Params
    }
}