package com.moten.smartCity.ui.main.news

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.recyclerview.widget.LinearLayoutManager
import com.moten.smartCity.databinding.ActivityNewInfoBinding
import com.moten.smartCity.models.request.NewsRequest
import com.moten.smartCity.models.result.BaseResult
import com.moten.smartCity.models.result.news.NewsInfo
import com.moten.smartCity.models.result.news.NewsList
import com.moten.smartCity.tools.*
import com.moten.smartCity.tools.HttpUtil.toModel
import com.moten.smartCity.ui.main.myself.LoginActivity

class NewInfoActivity : BaseActivity() {
    private val binding by lazy { ActivityNewInfoBinding.inflate(layoutInflater) }
    private val newsId by lazy { intent.getIntExtra("newId", 0) }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)


        getNews(newsId)
        binding.apply {
            topNews.apply {
                layoutManager = LinearLayoutManager(this@NewInfoActivity)
                rxAdapter()
            }
            addCommentBtn.setOnClickListener {
                val comment = addCommentEd.toText()
                if (comment.trim().isEmpty()) {
                    "内容不能为空".toast(this@NewInfoActivity)
                    return@setOnClickListener
                }
                addComment(comment, token)
            }
            seeComment.setOnClickListener {
                Intent(this@NewInfoActivity, CommentActivity::class.java).apply {
                    putExtra("newsId", newsId)
                    startActivity(this)
                }
            }
        }
    }

    private fun addComment(comment: String, token: String) {
        HttpUtil.postTo(
            "press/pressComment", token, NewsRequest(comment, newsId)
        ).enqueue(MyCallback {
            toModel(BaseResult::class.java).apply {
                if (code == 200) {
                    msg.snack(binding.addCommentBtn, "查看评论列表") {
                        Intent(this@NewInfoActivity, CommentActivity::class.java).apply {
                            putExtra("newsId", newsId)
                            startActivity(this)
                        }
                    }
                    binding.addCommentEd.text.clear()

                } else {
                    "未登录或登录失效".snack(binding.seeComment, "去登录") {
                        Intent(this@NewInfoActivity, LoginActivity::class.java).apply {
                            startActivity(this)
                        }
                    }
                    msg.toast(this@NewInfoActivity)
                }
            }
        })
    }

    private fun rxAdapter() {
        HttpUtil.service.get(
            "press/press/list"
        ).enqueue(MyCallback {
            val list = it.toModel(NewsList::class.java).rows
            val listR = ArrayList<NewsList.Row>()
            repeat(3) {
                listR.add(list.random())
            }
//            adapter = this@NewInfoActivity.newsRxAdapter(listR)
        })
    }

    private fun getNews(newsId: Int) {
        HttpUtil.service.get(
            "press/press/$newsId"
        ).enqueue(MyCallback {
            val data = it.toModel(NewsInfo::class.java)
            if (data.code != 200) {
                "出错啦".toast(this@NewInfoActivity)
                return@MyCallback
            }
//            Log.d("TAG", "getNews: $data")
//            return@MyCallback
            val html = data.data.content
                .replace("/prod-api", "${HttpUtil.BASE_URL}/prod-api")
                .replace("<img", "<img style='max-width:100%;height:auto;'")
            binding.apply {
                this@NewInfoActivity.title = data.data.title
                newsCover.glide(data.data.cover).into(newsCover)
//                content.loadData(html, "text/html", "utf-8")
                // 乱码就用下面这个
                newsContent.loadDataWithBaseURL(HttpUtil.BASE_URL, html, "text/html", "utf-8", null)
            }

        })
    }
}
