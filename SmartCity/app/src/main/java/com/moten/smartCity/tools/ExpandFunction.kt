package com.moten.smartCity.tools

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.res.Configuration
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import androidx.viewpager.widget.PagerAdapter
import com.bumptech.glide.Glide
import com.google.android.material.snackbar.Snackbar
import com.moten.smartCity.R
import com.moten.smartCity.databinding.ItemServiceBinding
import com.moten.smartCity.models.result.main.Service
import com.moten.smartCity.models.result.movie.MovieTypeMap
import com.moten.smartCity.tools.HttpUtil.toModel
import com.moten.smartCity.ui.main.MainActivity
import com.moten.smartCity.ui.main.myself.LoginActivity
import com.moten.smartCity.ui.module.*
import com.moten.smartCity.ui.module.movie.MovieActivity
import com.moten.smartCity.ui.module.smartBus.SmartBusActivity
import com.moten.smartCity.ui.module.subway.CitySubwayActivity
import com.moten.smartCity.ui.module.traffic.SmartTrafficActivity
import java.util.regex.Pattern

fun String.toast(context: Context) {
    Toast.makeText(context, this, Toast.LENGTH_SHORT).show()
}

fun String.snack(view: View, action: String, block: (view: View) -> Unit) {
    Snackbar.make(view, this, Snackbar.LENGTH_SHORT)
        .setAction(action) { block(it) }
        .show()
}

fun String.removeHTML(): String = Pattern.compile("<[^>]+>")
    .matcher(this)
    .replaceAll("")
    .replace("&nbsp;", "\n")
    .replace("&ensp;", "")
    .replace("&emsp;", "")

fun Context.reLogin(view: View) {
    "登录状态失效或未登录".snack(view, "重新登录") {
        startActivity(Intent(this, LoginActivity::class.java))
    }
}


fun SharedPreferences.open(block: SharedPreferences.Editor.() -> Unit) {
    val editor = this.edit()
    editor.block()
    editor.apply()
}

fun Activity.removeAll() {
    val intent = Intent(this, MainActivity::class.java)
    "返回主页".toast(this)
    // 清空当前堆栈Activity
//    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
    startActivity(intent)
}

fun Activity.off() {
    // 关闭输入法服务
    val imm = this.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    if (imm.isActive) imm.hideSoftInputFromWindow(
        this.currentFocus?.windowToken,
        InputMethodManager.HIDE_NOT_ALWAYS
    )
}

fun ImageView.glide(url: Any? = null, local: Boolean = false) =
    Glide.with(this.context)
        .load(if (local) url else HttpUtil.BASE_URL + url)
        .disallowHardwareConfig()

fun TextView.toText(): String = this.text.toString()

fun TextView.format(vararg string: Any?) {
    this.text = String.format(this.toText(), *string)
}

fun pagerAdapter(viewList: ArrayList<View>) =
    object : PagerAdapter() {
        override fun getCount(): Int = viewList.size

        override fun isViewFromObject(view: View, `object`: Any): Boolean =
            `object` == view

        override fun instantiateItem(container: ViewGroup, position: Int): Any {
            val view = viewList[position]
            container.addView(view)
            return view
        }

        override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
            container.removeView(viewList[position])
        }
    }

fun serviceRxAdapter(context: Context, list: List<Service.Row>, other: () -> Unit = {}) =
    RxAdapter(list, context, R.layout.item_service) { r, p ->
        val data = list[p]
        val rb = ItemServiceBinding.bind(r)

        rb.serviceImage.glide(data.imgUrl, local = (data.link == "more")).into(rb.serviceImage)
        rb.serviceName.text = data.serviceName

        r.setOnClickListener {
            other()
            if (data.serviceName == "更多服务") {
                (context as MainActivity).switch()
                return@setOnClickListener
            }
            val clazz = when (data.serviceName) {
                "停哪儿" -> StopWhereActivity::class.java
                "智慧交管" -> SmartTrafficActivity::class.java
                "城市地铁" -> CitySubwayActivity::class.java
                "生活缴费" -> LiveActivity::class.java
                "看电影" -> MovieActivity::class.java
                "活动管理" -> ManageActivity::class.java
                "智慧巴士" -> SmartBusActivity::class.java
                "门诊预约" -> OutpatientActivity::class.java
                "外卖订餐" -> TakeoutActivity::class.java
                "找房子" -> FindHouseActivity::class.java
                "找工作" -> FindJobActivity::class.java
                else -> null
            }
            if (clazz != null) context.startActivity(Intent(context, clazz))
            else "敬请期待".toast(context)
        }
    }


fun isTablet(context: Context): Boolean {
    val screenLayout = context.resources.configuration.screenLayout
    val size = Configuration.SCREENLAYOUT_SIZE_LARGE
    return (screenLayout and Configuration.SCREENLAYOUT_SIZE_MASK) >= size
}

fun EditText.search(context: Context, block: (str: String) -> Unit) {
    setOnEditorActionListener { _, actionId, _ ->
        val searchText = this.text.toString()
        if (searchText.trim() == "") "搜索内容不能为空".toast(context)
        else if (actionId == EditorInfo.IME_ACTION_SEARCH) block(searchText)
        true
    }
}

fun String.isPhone(): Boolean = Pattern
    .compile("^((13[0-9])|(14[5|7])|(15[0-3|5-9])|(17[03678])|(18[0,5-9]))\\d{8}$")
    .matcher(this)
    .matches()

fun FitService.getType(typeMap: MutableLiveData<HashMap<String, String>>) {
    get(
        "/system/dict/data/type/film_category"
    ).enqueue(MyCallback {
        val mapData = it.toModel(MovieTypeMap::class.java).data
        Log.e("TAG", "getType: $mapData")
        val map = HashMap<String, String>()
        mapData.forEach { data ->
            val key = data.dictValue
            val value = data.dictLabel
            map[key] = value
        }
        typeMap.value = map
    })
}
