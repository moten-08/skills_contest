package com.moten.smartCity.ui.main

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import java.util.*

class GuideActivity : AppCompatActivity() {
    override fun onStart() {
        super.onStart()
        Thread.sleep(1000)
        startActivity(Intent(this@GuideActivity, MainActivity::class.java))
        finish()
    }
}