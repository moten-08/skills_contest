package com.moten.smartCity.models.request

/**
 * 意见反馈
 */
data class Feedback(val titleStr: String, val feedback: String)
