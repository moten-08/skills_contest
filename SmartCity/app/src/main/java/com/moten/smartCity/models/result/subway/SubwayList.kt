package com.moten.smartCity.models.result.subway

/**
 * 地铁站点查询
 */
data class SubwayList(
    var code: Int,
    var `data`: List<Data>,
    var msg: String
) {
    data class Data(
        var currentName: String,
        var lineId: Int,
        var lineName: String,
        var nextStep: NextStep,
        var preStep: PreStep,
        var reachTime: Int
    ) {
        data class NextStep(
            var lines: List<Line>,
            var name: String
        ) {
            data class Line(
                var lineId: Int,
                var lineName: String
            )
        }

        data class PreStep(
            var lines: List<Line>,
            var name: String
        ) {
            data class Line(
                var lineId: Int,
                var lineName: String
            )
        }
    }
}
