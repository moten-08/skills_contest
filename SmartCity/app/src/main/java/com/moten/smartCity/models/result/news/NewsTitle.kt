package com.moten.smartCity.models.result.news

import com.google.gson.annotations.SerializedName

/**
 * 新闻标题
 */
data class NewsTitle(
    var code: Int,
    // 注解演示用，不做任何功能
    @SerializedName("data")
    var rows: List<Rows>,
    var msg: String
) {
    class Rows(
        var appType: String,
        var createBy: Any,
        var createTime: Any,
        var id: Int,
        var name: String,
        var params: Params,
        var parentId: Any,
        var remark: Any,
        var searchValue: Any,
        var sort: Int,
        var status: String,
        var updateBy: Any,
        var updateTime: Any
    ) {
        class Params
    }
}