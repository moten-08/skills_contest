package com.moten.smartCity.models.result.movie

/**
 * 电影页轮播图
 */
data class BannerHome(
    var code: Int,
    var msg: String,
    var rows: List<Row>,
    var total: Int
) {
    data class Row(
        var advImg: String,
        var advTitle: String,
        var appType: String,
        var createBy: String,
        var createTime: String,
        var id: Int,
        var params: Params,
        var remark: Any?,
        var searchValue: Any?,
        var servModule: String,
        var sort: Int,
        var status: String,
        var targetId: Int?,
        var type: String,
        var updateBy: String?,
        var updateTime: String?
    ) {
        class Params
    }
}