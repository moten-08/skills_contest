package com.moten.smartCity.models.result.movie

/**
 * 周边影院
 */
data class Cinema(
    var code: Int,
    var msg: String,
    var rows: List<Row>,
    var total: Int
) {
    data class Row(
        var address: String,
        var area: String,
        var brand: Any?,
        var city: String,
        var cover: String,
        var createBy: String,
        var createTime: String,
        var distance: String,
        var id: Int,
        var movieId: Any?,
        var name: String,
        var params: Params,
        var province: String,
        var remark: Any?,
        var score: Int,
        var searchValue: Any?,
        var status: String,
        var tags: Any?,
        var timesId: Any?,
        var updateBy: String,
        var updateTime: String
    ) {
        class Params
    }
}