package com.moten.smartCity.ui.main.news

import android.app.AlertDialog
import android.content.ClipData
import android.content.ClipboardManager
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.moten.smartCity.R
import com.moten.smartCity.databinding.ActivityCommentBinding
import com.moten.smartCity.models.result.news.NewsComment
import com.moten.smartCity.tools.*
import com.moten.smartCity.tools.HttpUtil.toModel

class CommentActivity : BaseActivity() {
    private val binding by lazy { ActivityCommentBinding.inflate(layoutInflater) }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        val id = intent.getIntExtra("newsId", 0)
        title = "评论列表"

        binding.rvComment.apply {
            layoutManager = LinearLayoutManager(this@CommentActivity)
            getComments(id)
        }
    }

    private fun RecyclerView.getComments(id: Int) {
        HttpUtil.service.get(
            "press/comments/list", map = mapOf("newsId" to id.toString())
        ).enqueue(MyCallback {
            val data = toModel(NewsComment::class.java)
            if (data.code != 200) {
                "出错了".snack(binding.rvComment, "查看详情") { dialog(data) }
                return@MyCallback
            }
            if (data.total > 0) {
                val list = data.rows
                this.adapter = RxAdapter(
                    list, this@CommentActivity, R.layout.item_comment,
                ) { r, p ->
                    val commentCover: ImageView = r.findViewById(R.id.card)
                    val userName: TextView = r.findViewById(R.id.comment_username)
                    val commentContent: TextView = r.findViewById(R.id.comment_content)
                    val commentDate: TextView = r.findViewById(R.id.comment_date)

                    commentCover.glide(R.mipmap.person, local = true)
                    userName.text = list[p].userName
                    commentContent.text = list[p].content.removeHTML()
                    commentDate.text = list[p].commentDate
                }
            } else {
                "暂无评论".toast(this@CommentActivity)
            }

        })
    }

    private fun dialog(data: NewsComment) =
        AlertDialog.Builder(this@CommentActivity)
            .setTitle("报错信息")
            .setView(TextView(this@CommentActivity).apply {
                text = data.msg
                setTextIsSelectable(true)
            })
            .setNegativeButton("取消") { v, _ -> v.dismiss() }
            .setNeutralButton("copy") { _, _ ->
                (getSystemService(CLIPBOARD_SERVICE) as ClipboardManager).setPrimaryClip(
                    ClipData.newPlainText("label", data.msg)
                )
            }
            .setPositiveButton("确认") { v, _ -> v.dismiss() }
            .create()
            .show()

}