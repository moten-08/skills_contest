package com.moten.smartCity.models.result.movie

/**
 * 电影详情
 * @video 预告详情特有
 */
data class MovieInfo(
    var code: Int,
    var `data`: Data,
    var msg: String
) {
    data class Data(
        var category: String,
        var cover: String,
        var createBy: Any?,
        var createTime: Any?,
        var duration: Int,
        var favoriteNum: Int,
        var id: Int,
        var introduction: String,
        var language: String,
        var likeNum: Int,
        var name: String,
        var other: Any?,
        var params: Params,
        var playDate: String,
        var playType: String,
        var recommend: String,
        var remark: Any?,
        var score: Int,
        var searchValue: Any?,
        var updateBy: Any?,
        var updateTime: Any?,

        var video: String
    ) {
        class Params
    }
}