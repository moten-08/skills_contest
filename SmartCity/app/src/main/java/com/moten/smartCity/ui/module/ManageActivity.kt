package com.moten.smartCity.ui.module

import android.os.Bundle
import com.moten.smartCity.R
import com.moten.smartCity.tools.BaseActivity

class ManageActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_manage)
        title = "活动管理"
    }
}