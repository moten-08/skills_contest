package com.moten.smartCity.tools

import android.util.Log
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MyCallback(val block: (body: Any) -> Unit) : Callback<Any> {
    override fun onResponse(p0: Call<Any>, p1: Response<Any>) {
        val body = p1.body()
        if (body == null) Log.e("TAG", "onResponse: body is null")
        else block(body)
    }

    override fun onFailure(p0: Call<Any>, p1: Throwable) = p1.printStackTrace()
}
