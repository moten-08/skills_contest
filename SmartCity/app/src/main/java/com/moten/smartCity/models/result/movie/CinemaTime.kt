package com.moten.smartCity.models.result.movie

/**
 * 场次信息
 */
data class CinemaTime(
    var code: Int,
    var msg: String,
    var rows: List<Row>,
    var total: Int
) {
    data class Row(
        var createBy: Any?,
        var createTime: Any?,
        var endTime: String,
        var id: Int,
        var movieId: Int,
        var movieName: String,
        var params: Params,
        var playDate: String,
        var playType: String,
        var price: Double,
        var remark: Any?,
        var roomId: Int,
        var roomName: String,
        var saleNum: Int,
        var searchValue: Any?,
        var startTime: String,
        var status: String,
        var theaterId: Int,
        var theatreName: String,
        var totalNum: Int,
        var updateBy: Any?,
        var updateTime: Any?
    ) {
        class Params
    }
}