package com.moten.smartCity.ui.module.traffic

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.lifecycle.MutableLiveData
import com.moten.smartCity.R
import com.moten.smartCity.databinding.ActivitySmartTrafficBinding
import com.moten.smartCity.models.request.BindingCar
import com.moten.smartCity.models.result.BaseResult
import com.moten.smartCity.models.result.traffic.PlateType
import com.moten.smartCity.tools.*
import com.moten.smartCity.tools.HttpUtil.toModel

class SmartTrafficActivity : BaseActivity() {

    private val binding by lazy {
        ActivitySmartTrafficBinding.inflate(layoutInflater)
    }
    private val carType = MutableLiveData<PlateType>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        title = "智慧交管"

        initView()
    }

    private fun initView() {
        getCarType()
        carType.observe(this) {
            val typeList: MutableList<String> = ArrayList()
            it.data.forEach { item ->
                typeList.add(item.dictValue)
            }
            val list: MutableList<String> = ArrayList()
            resources.getStringArray(R.array.province).forEach { item ->
                list.add(item)
            }
            binding.apply {
                plateType.apply {
                    adapter = ArrayAdapter(
                        this@SmartTrafficActivity,
                        android.R.layout.simple_spinner_item,
                        typeList
                    )
                    onItemSelectedListener = getItemSelectedListener(typeList)


                }
                province.onItemSelectedListener = getItemSelectedListener(list)
                selectTraffic.setOnClickListener {
                    val type = plateType.selectedItem.toString()
                    val plate = province.selectedItem.toString() + plateEd.text
                    val engineNo = engineEd.text.toString()

                    if (type.trim().isEmpty()) {
                        "汽车类型不能为空".toast(this@SmartTrafficActivity)
                        return@setOnClickListener
                    }
                    if (plateEd.text.toString().trim().isEmpty()) {
                        "车牌号码不能为空".toast(this@SmartTrafficActivity)
                        return@setOnClickListener
                    }
                    bindingCar(type, plate, engineNo)
                }
            }
        }
    }

    private fun bindingCar(type: String, plate: String, engineNo: String) {
        Log.d("plate", type)
        Log.d("province", plate)
        // 当前用户绑定车辆
        if (token == "") this.reLogin(binding.root)
        else {
            HttpUtil.postTo(
                "/api/traffic/car",
                token = token,
                request = BindingCar(engineNo, plate, type)
            ).enqueue(MyCallback {
                toModel(BaseResult::class.java).apply {
                    if (code != 200) {
                        this@SmartTrafficActivity.reLogin(binding.root)
                    } else {
                        "绑定成功".toast(this@SmartTrafficActivity)
                        Intent(
                            this@SmartTrafficActivity,
                            ViolationListActivity::class.java
                        ).apply {
                            putExtra("token", token)
                            startActivity(this)
                        }

                    }
                }
            })

        }
    }

    private fun getCarType() {
        HttpUtil.service.get(
            "/system/dict/data/type/plate_type"
        ).enqueue(MyCallback {
            carType.value = toModel(PlateType::class.java)
        })
    }

    private fun getItemSelectedListener(list: List<String>): AdapterView.OnItemSelectedListener {
        return object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long
            ) {
                Log.d("list", list[position])
            }

            override fun onNothingSelected(parent: AdapterView<*>) = Unit
        }
    }
}
