package com.moten.smartCity.models.result.news

/**
 * 新闻评论
 */
data class NewsComment(
    var code: Int, var msg: String, var rows: List<Row>, var total: Int
) {
    class Row(
        var appType: String,
        var commentDate: String,
        var content: String,
        var createBy: Any?,
        var createTime: Any?,
        var id: Int,
        var likeNum: Int,
        var newsId: Int,
        var newsTitle: String,
        var params: Params,
        var remark: Any?,
        var searchValue: Any?,
        var updateBy: Any?,
        var updateTime: Any?,
        var userId: Int,
        var userName: String
    ) {

        class Params
    }
}
