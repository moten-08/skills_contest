package com.moten.smartCity.models.result.main

/**
 * 登录返回
 */
data class Login(
    val code: Int,
    val msg: String,
    val token: String
)
