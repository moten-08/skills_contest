package com.moten.smartCity.tools

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import com.moten.smartCity.ui.module.*
import com.moten.smartCity.ui.module.movie.MovieActivity
import com.moten.smartCity.ui.main.news.NewInfoActivity
import com.moten.smartCity.ui.module.smartBus.SmartBusActivity
import com.moten.smartCity.ui.module.subway.CitySubwayActivity
import com.moten.smartCity.ui.module.traffic.SmartTrafficActivity

class ToolActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        val id = intent.getIntExtra("newId", 0)
        if (id != 0) {
            val intent = Intent(this, NewInfoActivity::class.java)
            intent.putExtra("newId", id)
            startActivity(intent)
        }
        finish()
    }
}
