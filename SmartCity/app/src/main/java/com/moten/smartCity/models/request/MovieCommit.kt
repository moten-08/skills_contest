package com.moten.smartCity.models.request

/**
 * 电影评论请求
 */
data class MovieCommit(
    var content: String,
    var movieId: Int,
    var score: Float
)