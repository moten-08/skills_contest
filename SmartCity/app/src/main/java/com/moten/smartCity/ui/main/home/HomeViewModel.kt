package com.moten.smartCity.ui.main.home

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.moten.smartCity.models.result.main.HomeBanner
import com.moten.smartCity.models.result.main.Service
import com.moten.smartCity.models.result.news.NewsList
import com.moten.smartCity.models.result.news.NewsTitle
import com.moten.smartCity.tools.HttpUtil
import com.moten.smartCity.tools.HttpUtil.toModel
import com.moten.smartCity.tools.MyCallback

class HomeViewModel : ViewModel() {

    val text = MutableLiveData<Map<String, String>>().apply {
        value = mapOf(
            "services" to "推荐服务",
            "news" to "新闻专栏",
            "hotTheme" to "热门主题"
        )
    }

    val bannerList = MutableLiveData<List<HomeBanner.Row>>()
    fun getBanner() {
        HttpUtil.service.get(
            "/api/rotation/list",
            map = mapOf("type" to "2")
        ).enqueue(MyCallback {
            Log.d("TAG", "getBanner: $it")
            bannerList.value = it.toModel(HomeBanner::class.java).rows
        })
    }

    // 获取全部服务
    val serviceList = MutableLiveData<Service>()
    fun getService(searchStr:String = "",type:String="") {
        HttpUtil.service.get(
            "api/service/list",map = mapOf("serviceName" to searchStr,"serviceType" to type)
        ).enqueue(MyCallback {
            serviceList.value = it.toModel(Service::class.java)
        })
    }

    // 新闻标题
    val newsTitleList = MutableLiveData<List<NewsTitle.Rows>>()
    fun getNewsTitle() {
        HttpUtil.service.get(
            "/press/category/list"
        ).enqueue(MyCallback {
            newsTitleList.value = it.toModel(NewsTitle::class.java).rows
        })
    }

    // 新闻内容
    val newsInfoList = MutableLiveData<List<NewsList.Row>>()
    fun getNews() {
        HttpUtil.service.get(
            "/press/press/list"
        ).enqueue(MyCallback {
            newsInfoList.value = it.toModel(NewsList::class.java).rows
        })
    }

    // 热门主题
    val newsHotList = MutableLiveData<List<NewsList.Row>>()
    fun getNewsHot() {
        HttpUtil.service.get(
            "/press/press/list",
            map = mapOf("hot" to "Y")
        ).enqueue(MyCallback {
            newsHotList.value = it.toModel(NewsList::class.java).rows
        })
    }

}
