package com.moten.smartCity.ui.main.myself

import android.os.Bundle
import com.moten.smartCity.databinding.ActivityUpPassBinding
import com.moten.smartCity.models.request.PasswordUp
import com.moten.smartCity.models.result.BaseResult
import com.moten.smartCity.tools.*
import com.moten.smartCity.tools.HttpUtil.toModel

class UpPassActivity : BaseActivity() {
    private val binding by lazy { ActivityUpPassBinding.inflate(layoutInflater) }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        title = "修改密码"

        binding.apply {
            submitUP.setOnClickListener {
                if (newPass.toText() != newPass2.toText()) {
                    "两次输入的密码不一致".toast(this@UpPassActivity)
                    return@setOnClickListener
                }
                upPassword(orderPass.toText(), newPass.toText())
            }
        }

    }

    private fun upPassword(ordPassword: String, newPassword: String) {
        HttpUtil.putTo(
            "api/common/user/resetPwd", token, PasswordUp(newPassword, ordPassword)
        ).enqueue(MyCallback {
            it.toModel(BaseResult::class.java).apply {
                msg.toast(this@UpPassActivity)
                if (code == 200) finish()
            }
        })
    }
}
