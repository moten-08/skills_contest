package com.moten.smartCity.ui.module

import android.os.Bundle
import com.moten.smartCity.R
import com.moten.smartCity.tools.BaseActivity

class FindHouseActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_find_house)
        title = "找房子"
    }
}