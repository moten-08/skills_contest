package com.moten.smartCity.ui.module

import android.os.Bundle
import com.moten.smartCity.R
import com.moten.smartCity.tools.BaseActivity

class OutpatientActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_outpatient)
        title = "门诊预约"
    }
}