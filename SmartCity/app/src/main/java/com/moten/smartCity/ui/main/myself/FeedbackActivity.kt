package com.moten.smartCity.ui.main.myself

import android.graphics.Color
import android.os.Bundle
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.MutableLiveData
import com.moten.smartCity.databinding.ActivityFeedbackBinding
import com.moten.smartCity.models.request.Feedback
import com.moten.smartCity.models.result.BaseResult
import com.moten.smartCity.tools.*
import com.moten.smartCity.tools.HttpUtil.toModel

class FeedbackActivity : BaseActivity() {

    private val binding by lazy { ActivityFeedbackBinding.inflate(layoutInflater) }
    private val edIndex = MutableLiveData<Int>().apply {
        value = 0
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        title = "意见反馈"

        edIndex.observe(this) {
            binding.apply {
                edNow.text = it.toString()
                edNow.setTextColor(if (it > 150) Color.RED else Color.BLACK)
                submit.setOnClickListener { _ ->
                    val titleStr = edTitle.toText()
                    val feedback = feedbackEd.toText()
                    if (titleStr.trim().isEmpty()) {
                        "标题不能为空".toast(this@FeedbackActivity)
                        return@setOnClickListener
                    }
                    if (feedback.trim().isEmpty()) {
                        "内容不能为空".toast(this@FeedbackActivity)
                        return@setOnClickListener
                    }
                    if (it > 150) {
                        "字数超出限制".toast(this@FeedbackActivity)
                        return@setOnClickListener
                    }
                    common(Feedback(titleStr, feedback))
                }
            }
            binding.feedbackEd.addTextChangedListener {
                edIndex.value = it?.length
            }
        }
    }

    private fun common(feedback: Feedback) {
        HttpUtil.postTo(
            "api/common/feedback", token, feedback
        ).enqueue(MyCallback {
            val data = it.toModel( BaseResult::class.java)
            runOnUiThread {
                data.msg.toast(this)
                if (data.code == 200) finish()
            }
        })
    }
}
