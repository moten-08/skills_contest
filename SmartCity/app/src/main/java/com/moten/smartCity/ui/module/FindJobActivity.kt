package com.moten.smartCity.ui.module

import android.os.Bundle
import com.moten.smartCity.databinding.ActivityFindJobBinding
import com.moten.smartCity.tools.BaseActivity

class FindJobActivity : BaseActivity() {
    private val binding: ActivityFindJobBinding by lazy {
        ActivityFindJobBinding.inflate(
            layoutInflater
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        title = "找工作"
    }
}