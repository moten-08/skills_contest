package com.moten.smartCity.ui.main.news

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.moten.smartCity.models.request.NewsRequest
import com.moten.smartCity.models.result.BaseResult
import com.moten.smartCity.models.result.news.NewsComment
import com.moten.smartCity.models.result.news.NewsInfo
import com.moten.smartCity.tools.HttpUtil
import com.moten.smartCity.tools.HttpUtil.toModel
import com.moten.smartCity.tools.MyCallback

class NewInfoViewModel : ViewModel() {
    val result = MutableLiveData<NewsInfo>()
    fun getNewInfo(id: Int) {
        HttpUtil.service.get(
            "/press/press/$id"
        ).enqueue(MyCallback {
            result.value = toModel(NewsInfo::class.java)
        })
    }

    var baseResult = MutableLiveData<BaseResult>()
    fun putComment(id: Int, string: String, token: String) {
        HttpUtil.postTo(
            "/press/pressComment",
            request = NewsRequest(string, id),
            token = token
        ).enqueue(MyCallback {
            baseResult.value = toModel(BaseResult::class.java)
        })
    }

    var commentResult = MutableLiveData<NewsComment>()
    fun getCommentList(id: Int) {
        HttpUtil.service.get(
            "/press/comments/list",
            map = mapOf("newsId" to id.toString())
        ).enqueue(MyCallback {
            commentResult.value = toModel(NewsComment::class.java)
        })
    }

    fun likeThisNews(id: Int, token: String) {
        HttpUtil.putTo(
            "/press/press/like/$id",
            token = token
        ).enqueue(MyCallback {
            baseResult.value = toModel(BaseResult::class.java)
        })
    }

    fun putLikeComment(id: Int, token: String) {
        HttpUtil.putTo(
            "/press/pressComment/like/$id",
            token = token
        ).enqueue(MyCallback {
            baseResult.value = toModel(BaseResult::class.java)
        })
    }

}
