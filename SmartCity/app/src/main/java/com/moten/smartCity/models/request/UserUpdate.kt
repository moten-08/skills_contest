package com.moten.smartCity.models.request

/**
 * 个人信息更新
 */
data class UserUpdate (
    var nickName: String,
    var phonenumber: String,
    var sex: String,
    var avatar: String
)
