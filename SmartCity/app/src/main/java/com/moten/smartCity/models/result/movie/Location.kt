package com.moten.smartCity.models.result.movie

/**
 * 当前地理位置
 */
data class Location(
    var code: Int,
    var `data`: Data,
    var msg: String
) {
    data class Data(
        var area: String,
        var city: String,
        var location: String,
        var province: String
    )
}