package com.moten.smartCity.models.request

/**
 * 登录请求
 */
data class LoginRequest(
    val username: String,
    val password: String,
)