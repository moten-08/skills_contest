package com.moten.smartCity.models.result.main

/**
 * 文件上传
 */
data class FileLoad(
    val msg: String,
    val fileName: String,
    val code: Int,
    val url: String,
)
