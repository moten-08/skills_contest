package com.moten.smartCity.ui.module.smartBus

import android.os.Bundle
import com.moten.smartCity.databinding.ActivityCustom2Binding
import com.moten.smartCity.tools.BaseActivity

class Custom2Activity : BaseActivity() {
    private val binding by lazy {
        ActivityCustom2Binding.inflate(layoutInflater)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        title = "定制班车--第二步"
    }
}