package com.moten.smartCity.models.result.traffic

/**
 * 违章详情
 */
data class ViolationInfo(
    var code: Int,
    var `data`: Data,
    var msg: String
) {
    data class Data(
        var badTime: String,
        var catType: String,
        var createBy: Any?,
        var createTime: Any?,
        var deductMarks: String,
        var disposeState: String,
        var engineNumber: String,
        var id: Int,
        var illegalSites: String,
        var imgUrl: String,
        var licencePlate: String,
        var money: String,
        var noticeNo: String,
        var params: Params,
        var performDate: String,
        var performOffice: String,
        var plateNoList: List<Any>,
        var remark: Any?,
        var searchValue: Any?,
        var trafficOffence: String,
        var updateBy: Any?,
        var updateTime: Any?
    ) {
        class Params
    }
}