package com.moten.smartCity.models.result.news

/**
 * 新闻列表
 */
data class NewsList(
    var code: Int,
    var msg: String,
    var rows: List<Row>,
    var total: Int
) {
    data class Row(
        var appType: String,
        var commentNum: Int,
        var content: String,
        var cover: String,
        var createBy: String,
        var createTime: String,
        var hot: String,
        var id: Int,
        var likeNum: Int,
        var params: Params,
        var publishDate: String,
        var readNum: Int,
        var remark: Any,
        var searchValue: Any,
        var status: String,
        var subTitle: Any,
        var tags: Any,
        var title: String,
        var top: String,
        var type: String,
        var updateBy: String,
        var updateTime: String
    ) {
        class Params
    }
}