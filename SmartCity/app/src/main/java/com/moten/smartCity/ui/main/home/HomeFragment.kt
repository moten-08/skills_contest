package com.moten.smartCity.ui.main.home

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import com.moten.smartCity.R
import com.moten.smartCity.databinding.FragmentHomeBinding
import com.moten.smartCity.models.result.main.HomeBanner
import com.moten.smartCity.models.result.main.Service
import com.moten.smartCity.models.result.news.NewsList
import com.moten.smartCity.tools.*
import com.moten.smartCity.ui.main.news.NListActivity
import com.moten.smartCity.ui.main.news.NewInfoActivity
import com.youth.banner.adapter.BannerImageAdapter
import com.youth.banner.holder.BannerImageHolder
import com.youth.banner.indicator.RoundLinesIndicator

class HomeFragment : Fragment() {

    private lateinit var homeViewModel: HomeViewModel
    private lateinit var binding: FragmentHomeBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        homeViewModel =
            ViewModelProvider(this)[HomeViewModel::class.java]
        binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val hotNumber = if (isTablet(requireContext())) 4 else 2
        val serviceNumber = if (isTablet(requireContext())) 6 else 5

        // 标题加载
        homeViewModel.text.observe(viewLifecycleOwner) {
            binding.apply {
                titleService.text = it["services"]
                titleNews.text = it["news"]
                titleHotTheme.text = it["hotTheme"]

                rvService.layoutManager = GridLayoutManager(requireContext(), serviceNumber)
                rvHot.layoutManager = GridLayoutManager(requireContext(), hotNumber)
            }
        }

        // 获取轮播图
        homeViewModel.getBanner()
        homeViewModel.bannerList.observe(viewLifecycleOwner) {
            binding.banner.apply {
                adapter = bannerAdapter(it)
                indicator = RoundLinesIndicator(requireContext())
                setLoopTime(2500)
                scrollTime = 500
            }
        }

        binding.search.apply {
            search(requireContext()) {
                Log.d("TAG", "search: $it")
                val intent = Intent(requireContext(), NListActivity::class.java)
                intent.putExtra("search", it)
                startActivity(intent)
            }
        }

        // 全部服务
        homeViewModel.getService()
        homeViewModel.serviceList.observe(viewLifecycleOwner) {
            val list: MutableList<Service.Row> = ArrayList()
            list.addAll(it.rows.subList(0, 10))
            list[9].serviceName = "更多服务"
            list[9].imgUrl = R.mipmap.all
            list[9].link = "more"
            binding.rvService.adapter = serviceRxAdapter(requireContext(), list)
        }

        // 热门主题
        homeViewModel.getNewsHot()
        homeViewModel.newsHotList.observe(viewLifecycleOwner) {
            binding.rvHot.adapter = hotRvAdapter(it)
        }

    }

    private fun hotRvAdapter(it: List<NewsList.Row>): RxAdapter {
        return RxAdapter(it, requireContext(), R.layout.item_hot) { root, position ->
            val image: ImageView = root.findViewById(R.id.news_hot_image)
            val title: TextView = root.findViewById(R.id.news_hot_title)

            root.setOnClickListener { _ ->
                startActivity(
                    Intent(requireContext(), ToolActivity::class.java).apply {
                        putExtra("newId", it[position].id)
                    })
            }

            image.glide(it[position].cover).into(image)
            title.text = it[position].title
        }
    }

    private fun bannerAdapter(it: List<HomeBanner.Row>): BannerImageAdapter<HomeBanner.Row> {
        return object : BannerImageAdapter<HomeBanner.Row>(it) {
            override fun onBindView(
                holder: BannerImageHolder,
                row: HomeBanner.Row,
                i1: Int,
                i2: Int
            ) {
                holder.imageView.run { glide(row.advImg).into(this) }
                holder.imageView.setOnClickListener {
                    Intent(requireContext(), NewInfoActivity::class.java).apply {
                        putExtra("newId", row.id)
                        startActivity(this)
                    }

                }

            }
        }
    }
}
