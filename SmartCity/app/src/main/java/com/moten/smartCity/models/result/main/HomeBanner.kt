package com.moten.smartCity.models.result.main

/**
 * 主页轮播图
 */
data class HomeBanner(
    val code: Int,
    val msg: String,
    val rows: List<Row>,
    val total: Int
) {
    data class Row(
        val advImg: String,
        val advTitle: String,
        val appType: String,
        val createBy: String,
        val createTime: String,
        val id: Int,
        val params: Params,
        val remark: Any,
        val searchValue: Any,
        val servModule: String,
        val sort: Int,
        val status: String,
        val targetId: Int,
        val type: String,
        val updateBy: String,
        val updateTime: String
    ) {
        class Params
    }
}
