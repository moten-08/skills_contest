package com.moten.smartCity.ui.module.subway

import android.os.Bundle
import com.moten.smartCity.databinding.ActivityCitySubwayBinding
import com.moten.smartCity.tools.BaseActivity

class CitySubwayActivity : BaseActivity() {
    private val binding by lazy { ActivityCitySubwayBinding.inflate(layoutInflater) }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        title = "城市地铁"
    }
}