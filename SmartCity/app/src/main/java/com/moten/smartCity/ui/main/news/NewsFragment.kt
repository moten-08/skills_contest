package com.moten.smartCity.ui.main.news

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.moten.smartCity.R
import com.moten.smartCity.databinding.FragmentNewsBinding
import com.moten.smartCity.databinding.ItemNewsBinding
import com.moten.smartCity.models.result.news.NewsList
import com.moten.smartCity.models.result.news.NewsTitle
import com.moten.smartCity.tools.*
import com.moten.smartCity.tools.HttpUtil.toModel
import com.moten.smartCity.ui.main.home.HomeViewModel

class NewsFragment : Fragment() {

    private lateinit var newsViewModel: HomeViewModel
    private lateinit var binding: FragmentNewsBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        newsViewModel =
            ViewModelProvider(this)[HomeViewModel::class.java]

        binding = FragmentNewsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // 获取标题
        newsViewModel.getNewsTitle()
        // 默认加载所有新闻
        newsViewModel.getNews()
        val titleList: MutableList<NewsTitle.Rows> = ArrayList()
        val viewList = ArrayList<View>()
        // 标题绑定
        newsViewModel.newsTitleList.observe(viewLifecycleOwner) {
            titleList.addAll(it)
            // 标题绑定、视图加载
            it.forEach { rows ->
                val itemView = View.inflate(requireContext(), R.layout.item_news_list, null)
                itemView.findViewById<RecyclerView>(R.id.rv_news_info).getNews(rows.id)
                viewList.add(itemView)
            }
            binding.apply {
                // viewPager的适配器
                vp.adapter = pagerAdapter(viewList)
                // viewPager与tabLayout的互相绑定
                tabLayoutTitle.setupWithViewPager(binding.vp)
                it.forEachIndexed { index, rows ->
                    tabLayoutTitle.getTabAt(index)?.text = rows.name
                }
            }
        }
    }

    private fun RecyclerView.getNews(id: Int) {
        HttpUtil.service.get(
            "press/press/list", map = mapOf("type" to id.toString())
        ).enqueue(MyCallback {
            if (isAdded) aboutRecyclerView(it)
        })
    }

    private fun RecyclerView.aboutRecyclerView(it: Any) {
        val data = it.toModel(NewsList::class.java)
        this.layoutManager = LinearLayoutManager(requireContext())
        adapter = RxAdapter(data.rows, requireContext(), R.layout.item_news) { r, p ->
            val rb = ItemNewsBinding.bind(r)
            val d = data.rows[p]
            rb.apply {
                newsTitle.text = d.title
                newsCover.glide(d.cover).into(newsCover)
                newsContent.text = d.content.removeHTML()
                newsDate.text = d.publishDate
                newsLikeNumber.text = d.likeNum.toString()
                newsReadNumber.text = d.readNum.toString()
                newsCommentNum.format(d.commentNum)
            }
            r.setOnClickListener {
                Intent(requireContext(), ToolActivity::class.java)
                    .apply {
                        putExtra("newId", d.id)
                        startActivity(this)
                    }
            }
        }
    }

}
