package com.moten.smartCity.ui.main.allService

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.moten.smartCity.R
import com.moten.smartCity.databinding.FragmentAllBinding
import com.moten.smartCity.models.result.main.Service
import com.moten.smartCity.tools.*
import com.moten.smartCity.tools.HttpUtil.toModel
import com.moten.smartCity.ui.main.home.HomeViewModel


class AllServiceFragment : Fragment() {

    private lateinit var viewModel: HomeViewModel
    private lateinit var binding: FragmentAllBinding

    private val index = MutableLiveData<Int>().apply { value = 0 }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        binding = FragmentAllBinding.inflate(inflater, container, false)
        viewModel =
            ViewModelProvider(this)[HomeViewModel::class.java]
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val set = mutableSetOf<String>()
        val list = ArrayList<String>()
        binding.types.layoutManager = LinearLayoutManager(requireContext())
        binding.services.layoutManager = GridLayoutManager(requireContext(), 3)
        viewModel.apply {
            getService()
            serviceList.observe(viewLifecycleOwner) {
                if (it.code != 200) {
                    "出错啦".toast(requireContext())
                    return@observe
                }
                it.rows.forEach { item ->
                    set.add(item.serviceType.toString())
                }
                list.addAll(set.toList())
                index.observe(viewLifecycleOwner) { i ->
                    binding.types.adapter = rxAdapter(set, i)
                    val list2 = ArrayList<Service.Row>()
                    it.rows.forEach { item ->
                        if (item.serviceType == list[i]) list2.add(item)
                    }
                    binding.services.adapter = serviceRxAdapter(requireContext(), list2)
                }
            }
            binding.searchAll.search(requireContext()) {
                it.toast(requireContext())
                getSearchService(it)
                requireActivity().off()
            }
        }
    }

    private fun getSearchService(it: String) {
        HttpUtil.service.get(
            "api/service/list", map = mapOf("serviceName" to it)
        ).enqueue(MyCallback {
            val data = it.toModel( Service::class.java)
            val dialog = BottomSheetDialog(requireContext())
            val dialogView = RecyclerView(requireContext()).apply {
                layoutManager = GridLayoutManager(requireContext(), 4)
                adapter = serviceRxAdapter(requireContext(), data.rows) {
                    dialog.dismiss()
                }
            }
            dialog.apply {
                setCancelable(true)
                setContentView(dialogView)
                show()
            }
        })
    }

    private fun rxAdapter(
        set: MutableSet<String>,
        i: Int?
    ) = RxAdapter(
        set.toList(),
        requireContext(),
        R.layout.item_type,
    ) { r, p ->
        r.findViewById<TextView>(R.id.textIT).apply {
            text = set.toList()[p]
        }
        r.setOnClickListener { index.value = p }
        if (p == i) r.setBackgroundColor(Color.WHITE)
    }

}
