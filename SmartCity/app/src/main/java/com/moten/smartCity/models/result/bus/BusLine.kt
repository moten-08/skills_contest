package com.moten.smartCity.models.result.bus

import java.io.Serializable

/**
 * 巴士列表
 */
data class BusLine(
    var code: Int,
    var msg: String,
    var rows: List<Row>,
    var total: Int
) : Serializable {
    data class Row(
        var createBy: Any?,
        var createTime: String,
        var end: String,
        var endTime: String,
        var first: String,
        var id: Int,
        var mileage: String,
        var name: String,
        var params: Params,
        var price: Double,
        var remark: Any?,
        var searchValue: Any?,
        var startTime: String,
        var updateBy: Any?,
        var updateTime: String
    ) : Serializable {
        class Params : Serializable
    }
}
