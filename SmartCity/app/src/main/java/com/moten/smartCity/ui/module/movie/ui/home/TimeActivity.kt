package com.moten.smartCity.ui.module.movie.ui.home

import android.os.Bundle
import com.moten.smartCity.databinding.ActivityTimeBinding
import com.moten.smartCity.tools.BaseActivity
import com.moten.smartCity.tools.HttpUtil
import com.moten.smartCity.tools.MyCallback

class TimeActivity : BaseActivity() {
    private val binding by lazy { ActivityTimeBinding.inflate(layoutInflater) }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        getData()
    }

    private fun getData() {
        val intentData = intent
        HttpUtil.service.get(
            "/api/movie/theatre/list4times",
            map = mapOf(
                "roomId" to intentData.getIntExtra("roomId", 0).toString(),
                "id" to intentData.getIntExtra("id", 0).toString()
            ),
        ).enqueue(MyCallback {

        })
    }

}
