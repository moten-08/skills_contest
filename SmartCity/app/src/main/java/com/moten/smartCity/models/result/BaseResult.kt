package com.moten.smartCity.models.result

/**
 * 基础回调类
 */
data class BaseResult(
    var code: Int,
    var msg: String
)