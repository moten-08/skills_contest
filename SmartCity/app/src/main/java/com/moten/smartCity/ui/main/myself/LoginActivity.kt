package com.moten.smartCity.ui.main.myself

import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.lifecycle.ViewModelProvider
import com.moten.smartCity.databinding.ActivityLoginBinding
import com.moten.smartCity.models.request.LoginRequest
import com.moten.smartCity.tools.BaseActivity
import com.moten.smartCity.tools.open
import com.moten.smartCity.tools.toText
import com.moten.smartCity.tools.toast

class LoginActivity : BaseActivity() {
    private lateinit var binding: ActivityLoginBinding
    private lateinit var viewModel: MyselfViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)
        title = "登录"

        viewModel = ViewModelProvider(this)[MyselfViewModel::class.java]

        viewModel.loginResult.observe(this@LoginActivity) {
            it.msg.toast(this@LoginActivity)
            if (it.code == 200) {
                Log.d("token", it.token)
                getSharedPreferences("data", Context.MODE_PRIVATE).open {
                    putString("token", "Bearer " + it.token)
                }
                finish()
            }
        }
        binding.apply {
            submit.setOnClickListener {
                viewModel.login(
                    LoginRequest(
                        edUsername.toText(),
                        edPassword.toText()
                    )
                )
            }
        }

    }
}
