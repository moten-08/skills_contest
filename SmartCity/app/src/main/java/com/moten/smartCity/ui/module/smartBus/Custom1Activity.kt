package com.moten.smartCity.ui.module.smartBus

import android.os.Bundle
import android.widget.TextView
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.moten.smartCity.R
import com.moten.smartCity.databinding.ActivityCustom1Binding
import com.moten.smartCity.models.result.bus.BusLine
import com.moten.smartCity.models.result.bus.BusStop
import com.moten.smartCity.tools.*
import com.moten.smartCity.tools.HttpUtil.toModel

class Custom1Activity : BaseActivity() {
    private val binding by lazy {
        ActivityCustom1Binding.inflate(layoutInflater)
    }

    private val busStop = MutableLiveData<BusStop>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        title = "定制班车--第一步"

        val data = intent.getSerializableExtra("busData") as BusLine.Row

        binding.apply {
            rv.layoutManager = LinearLayoutManager(
                this@Custom1Activity,
                RecyclerView.HORIZONTAL,
                false
            )
            busStop.observe(this@Custom1Activity) {
                rv.adapter = RxAdapter(
                    it.rows,
                    this@Custom1Activity,
                    R.layout.item_bus_stop
                ) { root, position ->
                    val text: TextView = root.findViewById(R.id.bus_text)
                    text.text = it.rows[position].name
                    startToEnd.format(it.rows[0].name, it.rows[it.rows.size - 1].name)
                    price.format(data.price)
                    mileage.format(data.mileage)
                    customName.text = data.name
                }
            }
        }
        getBusStop(data.id)
    }

    private fun getBusStop(id: Int) {
        HttpUtil.service.get(
            "/api/bus/stop/list",
            map = mapOf("linesId" to id.toString())
        ).enqueue(MyCallback {
            busStop.value = toModel(BusStop::class.java)
        })

    }
}
