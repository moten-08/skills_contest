package com.moten.smartCity.tools

import android.content.SharedPreferences
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.moten.smartCity.R

open class BaseActivity : AppCompatActivity() {

    private val sp: SharedPreferences by lazy { getSharedPreferences("data", MODE_PRIVATE) }
    protected val token by lazy { sp.getString("token", "") ?: "" }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.let {
            it.setDisplayHomeAsUpEnabled(true)
            it.setHomeAsUpIndicator(R.drawable.ic_baseline_navigate_before_24)
        }

        // 获取本地类名并拆分
        val nameList: List<String> = this.localClassName.split(".")
        // 得到当前类名，去掉Activity后缀
        var activityName: String
        nameList.also { activityName = it[it.size - 1].replace("Activity", "") }

        title = activityName
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) finish()
        return super.onOptionsItemSelected(item)
    }

}
