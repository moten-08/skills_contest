package com.moten.smartCity.models.result.movie

/**
 * 场次座位信息
 */
data class List4times(
    var code: Int,
    var `data`: List<Data>,
    var msg: String
) {
    data class Data(
        var col: String,
        var createBy: Any?,
        var createTime: Any?,
        var id: Int,
        var params: Params,
        var remark: Any?,
        var roomId: Int,
        var row: String,
        var searchValue: Any?,
        var status: Any?,
        var type: String,
        var updateBy: Any?,
        var updateTime: Any?
    ) {
        class Params
    }
}