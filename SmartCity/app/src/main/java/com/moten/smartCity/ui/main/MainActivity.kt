package com.moten.smartCity.ui.main

import android.Manifest
import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.navigation.NavigationBarView
import com.moten.smartCity.R
import com.moten.smartCity.databinding.ActivityMainBinding
import com.moten.smartCity.tools.HttpUtil
import com.moten.smartCity.tools.open

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        accessRequest()

        // 使用FragmentContainerView替换Fragment需改成这样
        // findFragmentById/findFragmentByTag都行
        val fragment = supportFragmentManager.findFragmentByTag("my_tag")
        navController = (fragment as NavHostFragment).navController

        val navView: BottomNavigationView = binding.navView
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.

        // 控制标题栏
        val appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.navigation_home,
                R.id.navigation_all_service,
                R.id.navigation_theme,
                R.id.navigation_news,
                R.id.navigation_myself
            )
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)

        // 显示全部tab
        navView.labelVisibilityMode = NavigationBarView.LABEL_VISIBILITY_LABELED

        tips()
    }

    private fun tips() {
        val sp = getSharedPreferences("data", MODE_PRIVATE)
        val boolean = sp.getBoolean("first", true)
        if (HttpUtil.BASE_URL == HttpUtil.baseUrl2 && boolean) {
            AlertDialog.Builder(this)
                .setTitle("温馨提示")
                .setView(TextView(this).apply {
                    text = "默认使用内网服务器,请点击右上角切换"
                })
                .create()
                .show()
            sp.open { putBoolean("first", false) }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.setting -> startActivity(Intent(this, SettingActivity::class.java))
        }
        return super.onOptionsItemSelected(item)
    }

    // 切换fragment
    fun switch(index: Int = R.id.navigation_all_service) = navController.navigate(index)

    private fun accessRequest() {
        requestPermissions(
            arrayOf(
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.CAMERA,
                Manifest.permission.CALL_PHONE,
            ), 100
        )
    }

}
