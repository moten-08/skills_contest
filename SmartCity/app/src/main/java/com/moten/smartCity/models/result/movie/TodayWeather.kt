package com.moten.smartCity.models.result.movie

/**
 * 今日天气
 */
data class TodayWeather(
    var code: Int,
    var `data`: Data,
    var msg: String
) {
    data class Data(
        var air: String,
        var apparentTemperature: String,
        var day: Int,
        var humidity: String,
        var label: String,
        var maxTemperature: String,
        var minTemperature: String,
        var temperature: String,
        var uv: String,
        var weather: String
    )
}