package com.moten.smartCity.ui.module.movie.ui.home

import android.content.Intent
import android.os.Bundle
import android.widget.TextView
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.LinearLayoutManager
import com.moten.smartCity.R
import com.moten.smartCity.databinding.ActivityCinemaBinding
import com.moten.smartCity.databinding.ItemCinema2Binding
import com.moten.smartCity.models.result.movie.*
import com.moten.smartCity.tools.*
import com.moten.smartCity.tools.HttpUtil.toModel

class CinemaActivity : BaseActivity() {

    private val binding: ActivityCinemaBinding by lazy {
        ActivityCinemaBinding.inflate(layoutInflater)
    }
    private val movieInfo = MutableLiveData<MovieInfo.Data>()
    private val typeMap = MutableLiveData<HashMap<String, String>>()
    private val cinemaTime = MutableLiveData<CinemaTime>()

    private var movieId: Int = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        title = "选择影院"
        movieId = intent.getIntExtra("movieId", 0)

        getMovieInfo(movieId)
        setData()
    }

    private fun setData() {
        movieInfo.observe(this) { data ->
            binding.apply {
                advImg.glide(data.cover).into(advImg)
                movieName.text = data.name
                scoreU.format(data.score)
                typeMap.observe(this@CinemaActivity) {
                    binding.category.format(it[data.category] ?: "其他")
                }
                playDate.format(data.playDate)
                duration.format(data.duration)
            }
        }
        cinemaTime.observe(this) {
            binding.cinemas.adapter =
                RxAdapter(it.rows, this, R.layout.item_cinema2) { root, position ->
                    val rBinding = ItemCinema2Binding.bind(root)
                    root.setOnClickListener {
                        val data = cinemaTime.value?.rows?.get(position)
                        Intent(this, TimeActivity::class.java).apply {
                            putExtra("movieId", data?.movieId)
                            putExtra("theaterId", data?.theaterId)
                            putExtra("roomId", data?.roomId)
                            putExtra("playDate", data?.playDate)
                            putExtra("id", data?.id)
                            startActivity(this)
                        }

                    }
                    val data = it.rows[position]
                    rBinding.apply {
                        name.format(data.theatreName)
                        room.format(data.roomName)
                        money.format(data.price)
                        getAddress(data.theaterId, address)
                        date.format(data.playDate, data.startTime, data.endTime)
                        ticket.format(data.saleNum, data.totalNum)
                    }
                }
        }

    }

    private fun getAddress(id: Int, str: TextView) {
        HttpUtil.service.get(
            "/api/movie/theatre/$id"
        ).enqueue(MyCallback {
            with(
                toModel(CinemaInfo::class.java).data
            ) { str.format(area + address) }
        })
    }

    private fun getMovieInfo(movieId: Int) {
        binding.cinemas.layoutManager = LinearLayoutManager(this)
        // 获取电影详情
        with(HttpUtil.service) {
            get("/api/movie/film/detail/$movieId")
                .enqueue(MyCallback {
                    movieInfo.value = toModel(MovieInfo::class.java).data
                })
            // 查询系统字典获取当前影片类型
            this.getType(typeMap)
            // 查询影片场次列表
            get(
                "/api/movie/theatre/times/list",
                map = mapOf("movieId" to movieId.toString())
            ).enqueue(MyCallback {
                cinemaTime.value = toModel(CinemaTime::class.java)
            })
        }
    }


}
