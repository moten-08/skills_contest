package com.moten.smartCity.models.result.movie

/**
 * 影院详情
 */
data class CinemaInfo(
    var code: Int,
    var `data`: Data,
    var msg: String
) {
    data class Data(
        var address: String,
        var area: String,
        var brand: Any?,
        var city: String,
        var cover: String,
        var createBy: String,
        var createTime: String,
        var distance: String,
        var id: Int,
        var movieId: Any?,
        var name: String,
        var params: Params,
        var province: String,
        var remark: Any?,
        var score: Int,
        var searchValue: Any?,
        var status: String,
        var tags: Any?,
        var timesId: Any?,
        var updateBy: String,
        var updateTime: String
    ) {
        class Params
    }
}