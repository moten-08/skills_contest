package com.moten.smartCity.ui.module.movie.ui.home

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.LinearLayoutManager
import com.moten.smartCity.R
import com.moten.smartCity.databinding.ActivityMovieInfoBinding
import com.moten.smartCity.databinding.DialogWriteCommentBinding
import com.moten.smartCity.databinding.ItemCommentBinding
import com.moten.smartCity.models.request.MovieCommit
import com.moten.smartCity.models.result.BaseResult
import com.moten.smartCity.models.result.movie.MovieComment
import com.moten.smartCity.models.result.movie.MovieInfo
import com.moten.smartCity.tools.*
import com.moten.smartCity.tools.HttpUtil.toModel
import com.moten.smartCity.ui.main.myself.LoginActivity

class MovieInfoActivity : BaseActivity() {
    private val binding: ActivityMovieInfoBinding by lazy {
        ActivityMovieInfoBinding.inflate(
            layoutInflater
        )
    }

    private val movieInfo = MutableLiveData<MovieInfo.Data>()
    private val comment = MutableLiveData<MovieComment>()
    private val typeMap = MutableLiveData<HashMap<String, String>>()

    private var id: Int = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        title = "买票选座"

        id = intent.getIntExtra("movieId", 0)
        getMovieData()

        bindingTopData()
        bindingCommentData()

    }

    private fun bindingCommentData() {
        binding.apply {
            commentRv.layoutManager = LinearLayoutManager(this@MovieInfoActivity)
            comment.observe(this@MovieInfoActivity) {
                if (it.code != 200) {
                    "出错啦".snack(binding.root, "查看详情") { _ ->
                        AlertDialog.Builder(this@MovieInfoActivity)
                            .setView(TextView(this@MovieInfoActivity).apply {
                                text = it.msg
                            })
                            .create()
                            .show()
                    }
                    it.rows = listOf()
                }
                // 这里不能用自定义的拓展函数，不知道为什么，用了就更新不了数据
                commentNumber.text = String.format("评论人数：%s", it.rows.size)
                commentRv.adapter =
                    RxAdapter(
                        it.rows,
                        this@MovieInfoActivity,
                        R.layout.item_comment
                    ) { root, position ->
                        val rBinding = ItemCommentBinding.bind(root)
                        val data = it.rows[position]
                        rBinding.apply {
                            commentUsername.text = data.nickName
                            commentContent.text = data.content.removeHTML()
                            commentDate.text = data.commentDate
                            commentLikeNum.text = data.likeNum.toString()
                        }
                    }
            }
        }

    }

    private fun dialog(token: String) {
        // 弹出写评论窗口
        val view = View.inflate(this, R.layout.dialog_write_comment, null)
        val vBinding = DialogWriteCommentBinding.bind(view)
        val dialog: AlertDialog = AlertDialog.Builder(this@MovieInfoActivity)
            .setView(vBinding.root)
            .setTitle("写评论")
            .create()
        vBinding.submit.setOnClickListener {
            val content: String = vBinding.commentText.text.toString().trim()
            val score = vBinding.scoreU.rating
            if (content.isEmpty()) {
                "内容不能为空".toast(this)
                return@setOnClickListener
            }
            HttpUtil.postTo(
                "/api/movie/film/comment", token, MovieCommit(content, id, score)
            ).enqueue(MyCallback { request ->
                val data = request.toModel(BaseResult::class.java)
                if (data.code == 401) {
                    this@MovieInfoActivity.reLogin(binding.root)
                    return@MyCallback
                }
                data.msg.toast(applicationContext)
                // 重新加载下网络数据
                getMovieData()
            })
            dialog.dismiss()
        }
        dialog.show()
    }

    private fun bindingTopData() {
        movieInfo.observe(this) { data ->
            binding.apply {
                movieName.text = data.name
                advImg.glide(data.cover).into(advImg)
                playDate.format(data.playDate)

                typeMap.observe(this@MovieInfoActivity) {
                    category.format(it[data.category] ?: "其他")
                }

                val str = data.introduction.removeHTML()
                onclick(str)
                setIntroduction(str)

                scoreU.format(data.score)

                btnSeen.text = String.format("看过\n%s", data.favoriteNum)
                btnWant.text = String.format("想看\n%s", data.likeNum)

            }
        }
    }

    private fun setIntroduction(str: String) {
        binding.apply {
            introduction.text =
                if (str.length > 78) {
                    open.visibility = View.VISIBLE
                    putDown.visibility = View.INVISIBLE
                    str.subSequence(0, 78).toString() + "……"
                } else {
                    open.visibility = View.GONE
                    putDown.visibility = View.GONE
                    str
                }
        }

    }

    private fun getMovieData() {
        if (id == 0) {
            finish()
            "暂无信息".toast(applicationContext)
            return
        }
        // 获取电影详情
        with(HttpUtil.service) {
            get(
                "/api/movie/film/detail/$id"
            ).enqueue(MyCallback {
                movieInfo.value = toModel(MovieInfo::class.java).data
            })
            // 获取评论列表
            get(
                "/api/movie/film/comment/list",
                map = mapOf("movieId" to "$id")
            ).enqueue(MyCallback {
                comment.value = toModel(MovieComment::class.java)
            })
            // 查询系统字典获取当前影片类型
            getType(typeMap)
        }
    }

    private fun onclick(str: String) {
        binding.apply {
            // 购票
            buy.setOnClickListener {
                Intent(this@MovieInfoActivity, CinemaActivity::class.java).apply {
                    putExtra("movieId", id)
                    startActivity(this)
                }
            }
            // 展开
            open.setOnClickListener {
                introduction.text = str
                it.visibility = View.INVISIBLE
                putDown.visibility = View.VISIBLE
            }
            // 收起
            putDown.setOnClickListener {
                introduction.text = String.format("%s……", str.subSequence(0, 78).toString())
                it.visibility = View.INVISIBLE
                open.visibility = View.VISIBLE
            }
            // 登录验证
            writeComment.setOnClickListener {
                if (token == "") {
                    "尚未登录".snack(it, "去登录") {
                        startActivity(Intent(this@MovieInfoActivity, LoginActivity::class.java))
                    }
                    return@setOnClickListener
                }
                dialog(token)
            }
            // 看过
            btnSeen.setOnClickListener {
                HttpUtil.postTo(
                    "/api/movie/film/favorite/$id",
                    token
                ).enqueue(myCallback())
            }
            // 想看
            btnWant.setOnClickListener {
                HttpUtil.postTo(
                    "/api/movie/film/like/$id",
                    token
                ).enqueue(myCallback())
            }
        }
    }

    private fun myCallback() = MyCallback {
        val data = toModel(BaseResult::class.java)
        if (data.code == 401) {
            this@MovieInfoActivity.reLogin(binding.root)
            return@MyCallback
        }
        data.msg.toast(this@MovieInfoActivity)
        getMovieData()
    }
}
