package com.moten.smartCity.ui.main.myself

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.moten.smartCity.R
import com.moten.smartCity.databinding.FragmentMyselfBinding
import com.moten.smartCity.tools.format
import com.moten.smartCity.tools.glide
import com.moten.smartCity.tools.open
import com.moten.smartCity.tools.snack
import com.moten.smartCity.ui.main.MainActivity


class MyselfFragment : Fragment() {

    private lateinit var binding: FragmentMyselfBinding
    private lateinit var viewModel: MyselfViewModel

    private lateinit var sp: SharedPreferences
    private lateinit var token: String

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        viewModel = ViewModelProvider(this)[MyselfViewModel::class.java]
        sp = requireContext().getSharedPreferences("data", Context.MODE_PRIVATE)
        token = sp.getString("token", "") ?: ""
        binding = FragmentMyselfBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.userInfoLive.observe(viewLifecycleOwner) {
            binding.apply {
                if (it.code == 200) {
                    val avatar =
                        if ("/prod-api" in it.user.avatar) it.user.avatar else "/prod-api${it.user.avatar}"
                    avatarM.glide(avatar).circleCrop().into(avatarM)
                    nickName.format(it.user.nickName)
                } else if (token.trim().isEmpty()) {
                    "登录失效".snack(logout, "去登录") {
                        startActivity(Intent(requireContext(), LoginActivity::class.java))
                    }
                    nickName.text = "用户昵称"
                }
                logout.setOnClickListener { onClick() }
                people.setOnClickListener { _ ->
                    if (verifyToken()) return@setOnClickListener
                    Intent(requireContext(), UserInfoActivity::class.java).apply {
                        putExtra("userInfo", it.user)
                        startActivity(this)
                    }
                }
                myOrders.setOnClickListener { onClick(MyOrdersActivity::class.java) }
                upPassword.setOnClickListener { onClick(UpPassActivity::class.java) }
                feedback.setOnClickListener { onClick(FeedbackActivity::class.java) }
            }
        }
    }


    override fun onResume() {
        super.onResume()
        token = sp.getString("token", "") ?: ""
        verifyToken()
        viewModel.getUserInfo(token)

    }


    private fun onClick(clazz: Class<*>? = null) {
        if (verifyToken()) return
        if (clazz == null) {
            sp.open {
                remove("token")
                (requireActivity() as MainActivity).switch(R.id.navigation_myself)
            }
            return
        }
        startActivity(Intent(requireContext(), clazz))

    }

    private fun verifyToken(): Boolean {
        if (token.trim().isEmpty()) {
            "未登录".snack(binding.logout, "去登录") {
                startActivity(Intent(requireContext(), LoginActivity::class.java))
            }
            return true
        }
        return false
    }
}
