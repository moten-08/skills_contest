package com.moten.smartCity.ui.main.myself

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContract
import com.moten.smartCity.databinding.ActivityUserInfoBinding
import com.moten.smartCity.models.request.UserUpdate
import com.moten.smartCity.models.result.BaseResult
import com.moten.smartCity.models.result.main.FileLoad
import com.moten.smartCity.models.result.main.UserInfo
import com.moten.smartCity.tools.*
import com.moten.smartCity.tools.HttpUtil.toModel

class UserInfoActivity : BaseActivity() {

    private val binding by lazy { ActivityUserInfoBinding.inflate(layoutInflater) }
    private lateinit var idCardStr: String
    private lateinit var filePath: String

    private val aRC = object : ActivityResultContract<Intent, ActivityResult>() {
        override fun createIntent(context: Context, input: Intent): Intent = input
        override fun parseResult(resultCode: Int, intent: Intent?): ActivityResult = ActivityResult(resultCode, intent)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        title = "个人信息"

        val registerForActivityResult = registerForActivityResult(aRC) {
            if (it.resultCode == RESULT_OK) {
                val uri = it.data?.data
                binding.avatarU.run { this.glide(uri, local = true).into(this) }
                if (uri != null) {
                    val inputStream = contentResolver.openInputStream(uri)
                    val bytes = ByteArray(inputStream!!.available())
                    inputStream.read(bytes)
                    binding.tip.visibility = View.VISIBLE
                    binding.upload.setOnClickListener { upFile(bytes) }
                }
            }
        }

        val userInfo = intent.getSerializableExtra("userInfo") as UserInfo.User
        userInfo.apply {
            binding.apply {
                avatarU.glide(if ("/prod-api" in avatar) avatar else "/prod-api$avatar").into(avatarU)
                filePath = avatar
                usernameU.format(userName)
                nickEd.setText(nickName)
                sex0.isChecked = (sex == "0")
                sex1.isChecked = (sex == "1")
                phoneEd.setText(phonenumber)
                idCardStr = if ((idCard ?: "").toString().trim().isEmpty()) {
                    "暂未上传证件"
                } else {
                    val idc = StringBuffer(idCard.toString())
                    idc.replace(2, idc.length - 4, "*".repeat(idc.length - 6)).toString()
                }
                idCardU.format(idCardStr)
                emailU.format(email)
                balanceU.format(balance)
                scoreU.format(score)
                save.setOnClickListener {
                    val nicknameR = nickEd.toText()
                    val phoneR = phoneEd.toText()
                    val sexR = if (sex0.isChecked) "0" else "1"
                    val avatarR = filePath
                    if(!phoneR.isPhone()){
                        "手机号码不合法".toast(this@UserInfoActivity)
                        return@setOnClickListener
                    }
                    common(UserUpdate(nicknameR, phoneR, sexR, avatarR))
                }
                avatarU.setOnClickListener {
                    Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI).apply {
                        registerForActivityResult.launch(this)
                    }
                }
            }

        }
    }

    private fun upFile(bytes: ByteArray) {
        HttpUtil.uploadTo(token, bytes)
            .enqueue(MyCallback {
                val data = toModel(FileLoad::class.java)
                data.msg.toast(this)
                if (data.code == 200) {
                    binding.tip.visibility = View.GONE
                    filePath = data.fileName
                }
            })

    }

    private fun common(userUpdate: UserUpdate) {
        HttpUtil.putTo(
            "api/common/user", token, userUpdate
        ).enqueue(MyCallback {
            val data = toModel(BaseResult::class.java)
            data.msg.toast(this)
            if (data.code == 200) finish()
        })
    }
}
