package com.moten.smartCity.ui.main

import android.os.Bundle
import android.widget.RadioButton
import com.moten.smartCity.databinding.ActivitySettingBinding
import com.moten.smartCity.tools.BaseActivity
import com.moten.smartCity.tools.HttpUtil

class SettingActivity : BaseActivity() {
    private val binding by lazy { ActivitySettingBinding.inflate(layoutInflater) }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        binding.apply {
            ip1.initView(HttpUtil.baseUrl1)
            ip2.initView(HttpUtil.baseUrl2)
            ip3.initView("http://127.0.0.1")
        }

    }

    private fun RadioButton.initView(url: String) {
        text = url
        isChecked = HttpUtil.BASE_URL == url
        setOnClickListener { HttpUtil.BASE_URL = url }
    }
}
