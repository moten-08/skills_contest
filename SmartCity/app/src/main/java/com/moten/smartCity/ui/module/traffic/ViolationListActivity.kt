package com.moten.smartCity.ui.module.traffic

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.LinearLayoutManager
import com.moten.smartCity.R
import com.moten.smartCity.databinding.ActivityViolationListBinding
import com.moten.smartCity.databinding.ItemViolationBinding
import com.moten.smartCity.models.result.traffic.ViolationList
import com.moten.smartCity.tools.*
import com.moten.smartCity.tools.HttpUtil.toModel

// 违章列表
class ViolationListActivity : BaseActivity() {
    private val binding by lazy { ActivityViolationListBinding.inflate(layoutInflater) }
    private val violationList = MutableLiveData<ViolationList>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        title = "违章查询"

        getViolation()
        binding.rvViolation.apply {
            layoutManager = LinearLayoutManager(this@ViolationListActivity)
            violationList.observe(this@ViolationListActivity) {
                adapter = vioAdapter(with(it.rows) {
                    if (this.size > 6) return@with this.subList(0, 6)
                    else return@with this
                })
                binding.showMore.setOnClickListener { view ->
                    this.adapter = vioAdapter(it.rows)
                    view.visibility = View.GONE
                }
            }

        }
    }

    private fun getViolation() {
        // 获取当前违章信息列表
        HttpUtil.service.get(
            "/api/traffic/illegal/list",
            token = token
        ).enqueue(MyCallback {
            violationList.value = toModel(ViolationList::class.java)
        })
    }

    private fun vioAdapter(list: List<ViolationList.Row>): RxAdapter {
        return RxAdapter(
            list,
            this@ViolationListActivity,
            R.layout.item_violation
        ) { root, position ->
            val rBinding = ItemViolationBinding.bind(root)
            val data = list[position]
            rBinding.apply {
                licencePlate.format(data.licencePlate)
                disposeState.format(data.disposeState)
                violationTime.format(data.badTime)
                punishMoney.format(data.money)
                deductMarks.format(data.deductMarks)
                illegalSites.format(data.illegalSites)
                root.setOnClickListener {
                    Intent(this@ViolationListActivity, ViolationInfoActivity::class.java).apply {
                        putExtra("id", data.id)
                        startActivity(this)
                    }
                }
            }
        }
    }
}
