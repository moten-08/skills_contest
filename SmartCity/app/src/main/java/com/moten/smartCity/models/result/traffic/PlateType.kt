package com.moten.smartCity.models.result.traffic

/**
 * 车牌类型
 */
data class PlateType(
    var code: Int,
    var `data`: List<Data>,
    var msg: String
) {
    data class Data(
        var createBy: String,
        var createTime: String,
        var cssClass: Any?,
        var default: Boolean,
        var dictCode: Int,
        var dictLabel: String,
        var dictSort: Int,
        var dictType: String,
        var dictValue: String,
        var isDefault: String,
        var listClass: Any?,
        var params: Params,
        var remark: Any?,
        var searchValue: Any?,
        var status: String,
        var updateBy: Any?,
        var updateTime: Any?
    ) {
        class Params
    }
}