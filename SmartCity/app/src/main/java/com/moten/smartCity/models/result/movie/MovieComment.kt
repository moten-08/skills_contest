package com.moten.smartCity.models.result.movie

/**
 * 影片评论
 */
data class MovieComment(
    var code: Int,
    var msg: String,
    var rows: List<Row>,
    var total: Int
) {
    data class Row(
        var commentDate: String,
        var content: String,
        var createBy: Any?,
        var createTime: Any?,
        var id: Int,
        var likeNum: Int,
        var movieId: Int,
        var movieName: String,
        var nickName: String,
        var params: Params,
        var remark: Any?,
        var score: Int,
        var searchValue: Any?,
        var updateBy: Any?,
        var updateTime: Any?,
        var userId: Int,
        var userName: String
    ) {
        class Params
    }
}