package com.moten.smartCity.ui.main.myself

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import com.moten.smartCity.databinding.ActivityMyOrdersBinding
import com.moten.smartCity.tools.BaseActivity
import com.moten.smartCity.ui.main.news.NewsFragment

class MyOrdersActivity : BaseActivity() {
    private val binding by lazy { ActivityMyOrdersBinding.inflate(layoutInflater) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        title = "我的订单"

        val fragments = listOf(NewsFragment(), NewsFragment())
        val vp = ViewPager2(this)
        val tabLayout = TabLayout(this)
        vp.adapter = object : FragmentStateAdapter(this) {
            override fun getItemCount(): Int = fragments.size

            override fun createFragment(position: Int): Fragment = fragments[position]
        }
        TabLayoutMediator(tabLayout, vp) { tab, position ->
            tab.text = "$position"
        }.attach()

    }
}
