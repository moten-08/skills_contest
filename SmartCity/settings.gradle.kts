dependencyResolutionManagement {
    repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)
    repositories {
        google()
        mavenCentral()
    }
}
rootProject.apply {
    name = "SmartCity"
    buildFileName = "build.gradle.kts"
}
include(":app")
