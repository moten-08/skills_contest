package com.example.smartcity1215.ui.news

import android.os.Bundle
import com.example.smartcity1215.R
import com.example.smartcity1215.result.NewsInfo
import com.example.smartcity1215.tool.*
import kotlinx.android.synthetic.main.activity_news_info.*

class NewsInfoActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_news_info)

        val newsId = intent.getIntExtra("newsId", 0)
        title = "新闻$newsId"

        getNews(newsId)
    }

    private fun getNews(newsId: Int) {
        HttpUtil.getTo(
            "/press/press/$newsId",
            callback = MyCallback {
                val data = jsonToModel(it, NewsInfo::class.java)
                runOnUiThread {
                    title = data.data.title

                    cover.glide(data.data.cover)
                    val html = data.data.content
                        .replace("/prod-api", "${HttpUtil.baseUrl}/prod-api")
                        .replace("<img ", "<img style = \"max-width:100%;height:auto;\"")
                    web.loadDataWithBaseURL(HttpUtil.baseUrl, html, "text/html", "utf-8", null)
                }
            })
    }
}