package com.example.smartcity1215.ui.xx;

import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.smartcity1215.R;
import com.example.smartcity1215.tool.BaseActivity;

import java.util.List;

public class T3_1Activity extends BaseActivity {
    private EditText T3Text;
    private ImageButton T3Button;
    private LinearLayout T1G2Lin;
    private TextView T3Commit;
    List<XCard> cardList = T3Activity.cardList;
    int tof = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_t3_1);


        EditText T3Text = (EditText) findViewById(R.id.T3_Text);
        ImageButton T3Button = (ImageButton) findViewById(R.id.T3_Button);
        LinearLayout T1G2Lin = (LinearLayout) findViewById(R.id.T1G2_Lin);
        TextView T3Commit = (TextView) findViewById(R.id.T3_commit);

        T3Button.setOnClickListener(v -> {
            tof = 1;
            T3Button.setImageDrawable(getDrawable(R.mipmap.t3_2));
        });

        T3Commit.setOnClickListener(v -> {
            String text = T3Text.getText().toString();
            if (tof == 1){
                cardList.add(new XCard(R.mipmap.ic_tou_xiang,"张三",text,R.mipmap.t3_2));
            }else {
                cardList.add(new XCard(R.mipmap.ic_tou_xiang,"张三",text,0));
            }
            finish();
        });
    }
}