package com.example.smartcity1215.ui.home

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import com.example.smartcity1215.MainActivity
import com.example.smartcity1215.R
import com.example.smartcity1215.tool.RxAdapter
import com.example.smartcity1215.tool.open
import com.example.smartcity1215.tool.toast
import kotlinx.android.synthetic.main.activity_guide.*
import kotlinx.android.synthetic.main.dialog_g.view.*

class GuideActivity : AppCompatActivity() {

    private val index = MutableLiveData<Int>().apply {
        value = 0
    }
    private val sp: SharedPreferences by lazy { getSharedPreferences("data", MODE_PRIVATE) }
    private lateinit var token: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_guide)

        token = sp.getString("token", "") ?: ""
        val guideList = listOf(
            R.mipmap.guide1,
            R.mipmap.guide2,
            R.mipmap.guide3,
            R.mipmap.guide4,
            R.mipmap.guide5,
        )

        val views = ArrayList<View>()

        guideList.forEach { item ->
            val view = View.inflate(this, R.layout.item_g, null)
            view.findViewById<ImageView>(R.id.itemGR).setImageResource(item)
            views.add(view)
        }

        index.observe(this) {
            vpG.currentItem = it
            btnG.visibility = if (it == guideList.size - 1) View.VISIBLE else View.GONE
            settingG.visibility = if (it == guideList.size - 1) View.VISIBLE else View.GONE
        }
        vpG.apply {
            adapter = pagerAdapter(views)
            pagerListener()
        }
        rvG.apply {
            layoutManager = LinearLayoutManager(this@GuideActivity, RecyclerView.HORIZONTAL, false)
            adapter = rxAdapter(guideList)
        }

        goHome()
        dialog()
    }

    private fun dialog() {
        val view = View.inflate(this, R.layout.dialog_g, null)
        val dialog = AlertDialog.Builder(this)
            .setTitle("网络设置")
            .setView(view)
            .create()
        view.apply {
            ipSetting.setText(sp.getString("ip", ""))
            portSetting.setText(sp.getString("port", ""))
            save(dialog)
        }
        settingG.setOnClickListener {
            dialog.show()
        }
    }

    private fun goHome() {
        btnG.setOnClickListener {
            val ip = sp.getString("ip", "") ?: ""
            val port = sp.getString("port", "") ?: ""
            if (ip.trim().isEmpty()) {
                "IP未设置".toast(this)
                return@setOnClickListener
            }
            if (port.trim().isEmpty()) {
                "端口未设置".toast(this)
                return@setOnClickListener
            }
            sp.open { putBoolean("isFirst", false) }
            finish()
            startActivity(Intent(this, MainActivity::class.java))
        }
    }

    override fun onStart() {
        super.onStart()
        if (!sp.getBoolean("isFirst", true)) {
            finish()
            startActivity(Intent(this, MainActivity::class.java))
        }
    }

    private fun View.save(dialog: AlertDialog) {
        saveSetting.setOnClickListener {
            val ip = ipSetting.text.toString()
            val port = portSetting.text.toString()
            sp.open {
                putString("ip", ip)
                putString("port", port)
            }
            dialog.dismiss()
        }
    }

    private fun rxAdapter(guideList: List<Int>) =
        RxAdapter(this@GuideActivity, R.layout.item_gp, guideList) { root, position ->
            index.observe(this@GuideActivity) {
                root.findViewById<ImageView>(R.id.itemGPR).setImageResource(
                    if (position == it) R.drawable.ic_baseline_fiber_manual_record_24
                    else R.drawable.ic_baseline_fiber_manual_record_25
                )
            }
            root.setOnClickListener {
                index.value = position
            }
        }

    private fun ViewPager.pagerListener() {
        addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) = Unit

            override fun onPageSelected(position: Int) {
                index.value = position
            }

            override fun onPageScrollStateChanged(state: Int) = Unit

        })
    }

    private fun pagerAdapter(views: ArrayList<View>) =
        object : PagerAdapter() {
            override fun getCount(): Int = views.size

            override fun isViewFromObject(view: View, `object`: Any): Boolean =
                `object` == view

            override fun instantiateItem(container: ViewGroup, position: Int): Any {
                val view = views[position]
                container.addView(view)
                return view
            }

            override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
                container.removeView(views[position])
            }
        }
}