package com.example.smartcity1215.ui.xx;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.smartcity1215.R;
import com.example.smartcity1215.tool.BaseActivity2;
import com.google.android.material.tabs.TabLayout;

public class T1Activity extends BaseActivity2 {

    private LinearLayout T1G1Lin;
    private LinearLayout T1G2Lin;
    private TextView T1Commit;
    private TabLayout T1G;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTitle("物业服务");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_t1);


        T1G = (TabLayout) findViewById(R.id.T1_G);
        T1G1Lin = (LinearLayout) findViewById(R.id.T1G1_Lin);
        T1G2Lin = (LinearLayout) findViewById(R.id.T1G2_Lin);
        T1Commit = (TextView) findViewById(R.id.T1_commit);

        T1Commit.setOnClickListener(v -> {
            Toast.makeText(this, "提交成功", Toast.LENGTH_LONG).show();
        });

        T1G.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getPosition() == 0) {
                    T1G1Lin.setVisibility(View.VISIBLE);
                    T1G2Lin.setVisibility(View.GONE);
                } else {
                    T1G2Lin.setVisibility(View.VISIBLE);
                    T1G1Lin.setVisibility(View.GONE);
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    public void a(View view) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:110"));
        startActivity(intent);
    }
}