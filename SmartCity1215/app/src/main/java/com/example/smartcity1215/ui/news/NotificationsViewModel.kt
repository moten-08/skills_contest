package com.example.smartcity1215.ui.news

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.smartcity1215.result.NewsTitle
import com.example.smartcity1215.tool.HttpUtil
import com.example.smartcity1215.tool.MyCallback
import com.example.smartcity1215.tool.jsonToModel

class NotificationsViewModel : ViewModel() {

    val newsTitles = MutableLiveData<NewsTitle>()
    fun getTitles() {
        HttpUtil.getTo(
            "press/category/list", callback = MyCallback {
                newsTitles.value = jsonToModel(it, NewsTitle::class.java)
            })
    }
}