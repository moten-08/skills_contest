package com.example.smartcity1215.ui.news

import android.media.Image
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.smartcity1215.R
import com.example.smartcity1215.result.NewsList
import com.example.smartcity1215.result.ServiceList
import com.example.smartcity1215.tool.*
import kotlinx.android.synthetic.main.activity_guide.*
import kotlinx.android.synthetic.main.item_news.view.*

class NewsListActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_news_list)

        val search = intent.getStringExtra("search") ?: ""
        title = "\"$search\"的搜索结果"

        when (intent.getStringExtra("type")) {
            "newsTitle" -> {
                getNews(search)
            }
            "service" -> {
                getService(search)
            }
        }

    }

    private fun getService(search: String){
        HttpUtil.getTo(
            "api/service/list",
            map = mapOf("serviceName" to search),
            callback = MyCallback {
                val data = jsonToModel(it, ServiceList::class.java)
                runOnUiThread {
                    "共${data.total}条".toast(this)
                    findViewById<RecyclerView>(R.id.itemRv).apply {
                        layoutManager = GridLayoutManager(this@NewsListActivity,5)
                        adapter = RxAdapter(this@NewsListActivity,R.layout.item_service,data.rows){root, position ->
                            data.rows[position].let {item->
                                root.findViewById<ImageView>(R.id.serviceImg).glide(item.imgUrl,circle = true)
                                root.findViewById<TextView>(R.id.serviceName).text = item.serviceName
                            }


                        }
                    }
                }
            })
    }

    private fun getNews(search: String) {
        HttpUtil.getTo(
            "press/press/list",
            map = mapOf("title" to search),
            callback = MyCallback {
                val data = jsonToModel(it, NewsList::class.java)
                runOnUiThread {
                    "共${data.total}条".toast(this)
                    findViewById<RecyclerView>(R.id.itemRv).apply {
                        layoutManager = LinearLayoutManager(this@NewsListActivity)
                        adapter = rxAdapter1(data)
                    }
                }
            })
    }

    private fun rxAdapter1(data: NewsList) = RxAdapter(
        this@NewsListActivity,
        R.layout.item_news,
        data.rows
    ) { root, position ->
        data.rows[position].let { item ->
            root.findViewById<TextView>(R.id.titleNews).text = item.title
            root.findViewById<ImageView>(R.id.coverNews).glide(item.cover?:"")
            root.findViewById<TextView>(R.id.contentNews).text =
                item.content.removeHTML()
            root.findViewById<TextView>(R.id.dateNews).text =
                "发布时间:${item.publishDate}"
            root.findViewById<TextView>(R.id.textNews).text =
                "评论数:${item.commentNum}    阅读数:${item.readNum}   点赞数:${item.likeNum}"
        }
    }
}