package com.example.smartcity1215.result;

public class UserInfo {

    public String msg;
    public int code;
    public UserDTO user;

    public static class UserDTO {
        public int userId;
        public String userName;
        public String nickName;
        public String email;
        public String phonenumber;
        public String sex;
        public String avatar;
        public String idCard;
        public double balance;
        public int score;
    }
}
