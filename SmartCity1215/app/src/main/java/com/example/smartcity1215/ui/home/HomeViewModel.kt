package com.example.smartcity1215.ui.home

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.smartcity1215.result.BannerHome
import com.example.smartcity1215.result.NewsList
import com.example.smartcity1215.result.NewsTitle
import com.example.smartcity1215.result.ServiceList
import com.example.smartcity1215.tool.HttpUtil
import com.example.smartcity1215.tool.MyCallback
import com.example.smartcity1215.tool.jsonToModel

class HomeViewModel : ViewModel() {

    val bannerHome = MutableLiveData<BannerHome>()
    fun getBannerHome() {
        HttpUtil.getTo(
            "api/rotation/list",
            map = mapOf("type" to "2"),
            callback = MyCallback {
                bannerHome.value = jsonToModel(it, BannerHome::class.java)
            })
    }

    val allService = MutableLiveData<ServiceList>()
    fun getService() {
        HttpUtil.getTo(
            "api/service/list",
            callback = MyCallback {
                allService.value = jsonToModel(it, ServiceList::class.java)
            })
    }

    val newsList = MutableLiveData<NewsList>()
    fun getNews() {
        HttpUtil.getTo(
            "press/press/list",
            map = mapOf("hot" to "Y"),
            callback = MyCallback {
                newsList.value = jsonToModel(it, NewsList::class.java)
            })
    }

}