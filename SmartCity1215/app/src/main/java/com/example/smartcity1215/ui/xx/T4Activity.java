package com.example.smartcity1215.ui.xx;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.smartcity1215.R;
import com.example.smartcity1215.tool.BaseActivity;

import java.util.ArrayList;
import java.util.List;

public class T4Activity extends BaseActivity {

    public static List<XCard> cardList = new ArrayList<>();
    private RecyclerView T4_Rec;
    private ImageButton imageButton2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        cardList.add(new XCard(R.mipmap.tool4, "京B23456","114215","4512", "铁拐李","11223344556","",""));
        cardList.add(new XCard(R.mipmap.tool4, "京B28446","114514","3215","刘勇", "11223343356","",""));
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_t4);
        initView();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        T4_Rec.getAdapter().notifyDataSetChanged();
    }

    private void initView() {
        T4_Rec = (RecyclerView) findViewById(R.id.T4_Rec);
        imageButton2 = (ImageButton) findViewById(R.id.imageButton2);

        imageButton2.setOnClickListener(v -> startActivity(new Intent(T4Activity.this, T4_1Activity.class)));

        T4_Rec.setLayoutManager(new LinearLayoutManager(this));
        T4_Rec.setAdapter(new MyAdapter(this, R.layout.t4_layout, cardList) {
            @Override
            public void init(View view, int position) {
                XCard c = cardList.get(position);
                ImageView cardImage = (ImageView) view.findViewById(R.id.card_image);
                TextView cardFtext = (TextView) view.findViewById(R.id.card_ftext);
                TextView cardStext = (TextView) view.findViewById(R.id.card_stext);
                TextView cardTtext = (TextView) view.findViewById(R.id.card_ttext);
                TextView T4Button = (TextView) view.findViewById(R.id.T4_button);

                Glide.with(view).load(c.getfImage()).centerCrop().into(cardImage);

                cardFtext.setText(c.getFoT());
                cardStext.setText(c.getFT());
                cardTtext.setText(c.getFiT());
                Intent intent = new Intent(T4Activity.this, T4_1Activity.class);
                intent.putExtra("index",position);
                T4Button.setOnClickListener(v -> startActivity(intent));
            }
        });
    }

}