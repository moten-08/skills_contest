package com.example.smartcity1215.ui.xx;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.smartcity1215.R;
import com.example.smartcity1215.tool.BaseActivity;

import java.util.ArrayList;
import java.util.List;

public class T4_1Activity extends BaseActivity {

    private TextView textView6;
    private EditText carNum;
    private EditText parking;
    private EditText parkingNum;
    private EditText name;
    private EditText phone;
    private EditText Xname;
    private EditText Xlocation;
    private TextView button;


    List<XCard> cardList = T4Activity.cardList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_t4_1);

        textView6 = (TextView) findViewById(R.id.textView6);
        carNum = (EditText) findViewById(R.id.carNum);
        parking = (EditText) findViewById(R.id.parking);
        parkingNum = (EditText) findViewById(R.id.parkingNum);
        name = (EditText) findViewById(R.id.name);
        phone = (EditText) findViewById(R.id.phone);
        Xname = (EditText) findViewById(R.id.Xname);
        Xlocation = (EditText) findViewById(R.id.Xlocation);
        button = (TextView) findViewById(R.id.button);

        Intent intent = getIntent();
        int index = intent.getIntExtra("index", -1);
        if (index != -1){
            XCard c = cardList.get(index);
            carNum.setText(c.getFT());
            parking.setText(c.getSt());
            parkingNum.setText(c.getTT());
            name.setText(c.getFoT());
            phone.setText(c.getFiT());
            Xname.setText(c.getSiT());
            Xlocation.setText(c.getSeT());
        }

        button.setOnClickListener(v -> {
            if (carNum.getText().toString().trim().isEmpty()){
                Toast.makeText(this,"请填写车牌号码",Toast.LENGTH_LONG).show();
            }else if (name.getText().toString().trim().isEmpty()){
                Toast.makeText(this,"请填写车主姓名",Toast.LENGTH_LONG).show();
            }else if (phone.getText().toString().trim().isEmpty()){
                Toast.makeText(this,"请填写车主手机号",Toast.LENGTH_LONG).show();
            }else {
                if (index != -1){
                    cardList.remove(index);
                }
                cardList.add(new XCard(R.mipmap.tool4,carNum.getText().toString(),parking.getText().toString(),parkingNum.getText().toString(),name.getText().toString(),phone.getText().toString(),Xname.getText().toString(),Xlocation.getText().toString()));
                finish();
            }
        });
    }
}