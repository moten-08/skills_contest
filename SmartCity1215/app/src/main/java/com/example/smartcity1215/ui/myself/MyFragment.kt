package com.example.smartcity1215.ui.myself

import android.content.Context.MODE_PRIVATE
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.example.smartcity1215.MainActivity
import com.example.smartcity1215.R
import com.example.smartcity1215.tool.glide
import com.example.smartcity1215.tool.open
import com.example.smartcity1215.tool.snack

class MyFragment : Fragment() {

    private val viewModel: MyViewModel by lazy {
        ViewModelProviders.of(this).get(MyViewModel::class.java)
    }

    private lateinit var sp: SharedPreferences
    private lateinit var token: String

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_my, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        initView(view)

        view.findViewById<Button>(R.id.logout).setOnClickListener {
            sp.open {
                remove("token")
            }
            (requireActivity() as MainActivity).swith(R.id.navigation_myself)
        }
        view.findViewById<TextView>(R.id.upUserInfo).setOnClickListener {
            startActivity(Intent(requireContext(),UUActivity::class.java))
        }
        view.findViewById<TextView>(R.id.orderList).setOnClickListener {
            startActivity(Intent(requireContext(),OLActivity::class.java))
        }
        view.findViewById<TextView>(R.id.upPassword).setOnClickListener {
            startActivity(Intent(requireContext(),UPActivity::class.java))
        }
        view.findViewById<TextView>(R.id.feedback).setOnClickListener {
            startActivity(Intent(requireContext(),FBActivity::class.java))
        }
    }

    override fun onResume() {
        super.onResume()
        sp = requireActivity().getSharedPreferences("data", MODE_PRIVATE)
        token = sp.getString("token", "") ?: ""
        viewModel.getUserInfo(token)

    }

    private fun initView(view: View) {
        viewModel.apply {
            userLive.observe(viewLifecycleOwner) {
                if (it.code != 200) {
                    "暂未登录或登录态失效".snack(view, "去登录") {
                        startActivity(Intent(requireContext(), LoginActivity::class.java))
                    }
                } else {
                    it.user.let { i ->
                        view.findViewById<ImageView>(R.id.myAvatar)
                            .glide(
                                if ("/prod-api" in i.avatar) i.avatar else "/prod-api${i.avatar}",
                                circle = true
                            )
                        view.findViewById<TextView>(R.id.myUsername).text = i.nickName
                    }
                }
            }
        }
    }

}