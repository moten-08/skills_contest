package com.example.smartcity1215.ui.xx;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.smartcity1215.R;
import com.example.smartcity1215.tool.BaseActivity;
import com.google.android.material.tabs.TabLayout;

import java.util.Arrays;
import java.util.List;

public class T2Activity extends BaseActivity {

    List<XCard> stations = Arrays.asList(new XCard(R.mipmap.post, "A1快递站", "珠海市金湾区红旗镇"),
            new XCard(R.mipmap.post, "A2快递站", "珠海市金湾区飞机镇"));
    List<XCard> posts = Arrays.asList(new XCard(R.mipmap.post, "【A同学】的快递", "A1快递站", "未领取"),
            new XCard(R.mipmap.post, "【A同学】的快递", "A1快递站", "已领取"),
            new XCard(R.mipmap.post, "【A同学】的快递", "A2快递站", "已领取"));
    private TabLayout T2G;
    private RecyclerView T2G1Rec;
    private RecyclerView T2G2Rec;
    private LinearLayout T2G3Lin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTitle("快件管理");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_t2);

        T2G = (TabLayout) findViewById(R.id.T2_G);
        T2G1Rec = (RecyclerView) findViewById(R.id.T2G1_Rec);
        T2G2Rec = (RecyclerView) findViewById(R.id.T2G2_Rec);
        T2G3Lin = (LinearLayout) findViewById(R.id.T2G3_Lin);

        T2G.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                switch (tab.getPosition()) {
                    case 0:
                        T2G1Rec.setVisibility(View.VISIBLE);
                        T2G2Rec.setVisibility(View.GONE);
                        T2G3Lin.setVisibility(View.GONE);
                        break;
                    case 1:
                        T2G2Rec.setVisibility(View.VISIBLE);
                        T2G1Rec.setVisibility(View.GONE);
                        T2G3Lin.setVisibility(View.GONE);
                        break;
                    case 2:
                        T2G3Lin.setVisibility(View.VISIBLE);
                        T2G2Rec.setVisibility(View.GONE);
                        T2G1Rec.setVisibility(View.GONE);
                        break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


        T2G1Rec.setLayoutManager(new LinearLayoutManager(this));
        T2G1Rec.setAdapter(new MyAdapter(this, R.layout.t2_layout, stations) {
            @Override
            public void init(View view, int position) {
                XCard c = stations.get(position);
                ImageView cardImage = (ImageView) view.findViewById(R.id.card_image);
                TextView cardFtext = (TextView) view.findViewById(R.id.card_ftext);
                TextView cardStext = (TextView) view.findViewById(R.id.card_stext);
                TextView cardTtext = (TextView) view.findViewById(R.id.card_ttext);
                cardTtext.setVisibility(View.GONE);
                Glide.with(view).load(c.getfImage()).centerCrop().into(cardImage);
                cardFtext.setText(c.getFT());
                cardStext.setText(c.getSt());
            }
        });


        T2G2Rec.setLayoutManager(new LinearLayoutManager(this));
        T2G2Rec.setAdapter(new MyAdapter(this, R.layout.t2_layout, posts) {
            @Override
            public void init(View view, int position) {
                XCard c = posts.get(position);
                ImageView cardImage = (ImageView) view.findViewById(R.id.card_image);
                TextView cardFtext = (TextView) view.findViewById(R.id.card_ftext);
                TextView cardStext = (TextView) view.findViewById(R.id.card_stext);
                TextView cardTtext = (TextView) view.findViewById(R.id.card_ttext);
                Glide.with(view).load(c.getfImage()).centerCrop().into(cardImage);
                cardFtext.setText(c.getFT());
                cardStext.setText(c.getSt());
                cardTtext.setText(c.getTT());
            }
        });
    }
}