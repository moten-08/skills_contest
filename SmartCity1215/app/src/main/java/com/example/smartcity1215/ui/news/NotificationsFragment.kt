package com.example.smartcity1215.ui.news

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import com.example.smartcity1215.R
import com.example.smartcity1215.result.NewsList
import com.example.smartcity1215.tool.*
import com.google.android.material.tabs.TabLayout

class NotificationsFragment : Fragment() {

    private lateinit var notificationsViewModel: NotificationsViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        notificationsViewModel =
            ViewModelProviders.of(this).get(NotificationsViewModel::class.java)
        return inflater.inflate(R.layout.fragment_notifications, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val viewList = ArrayList<View>()

        notificationsViewModel.apply {
            getTitles()
            newsTitles.observe(viewLifecycleOwner) {
                val viewPager = view.findViewById<ViewPager>(R.id.vpNews)
                val tabLayout = view.findViewById<TabLayout>(R.id.tabNews)
                it.data.forEach {
                    val itemView = View.inflate(requireContext(), R.layout.activity_news_list, null)
                    itemView.findViewById<RecyclerView>(R.id.itemRv).apply {
                        layoutManager = LinearLayoutManager(requireContext())
                        getNews(it.id)
                    }
                    viewList.add(itemView)
                }
                viewPager.adapter = pagerAdapter(viewList)
                tabLayout.setupWithViewPager(viewPager)
                it.data.forEachIndexed { index, item ->
                    Log.d("TAG", "onViewCreated:${item.name} ")
                    tabLayout.getTabAt(index)?.text = item.name
                }
            }
        }
    }

    private fun pagerAdapter(viewList: ArrayList<View>) =
        object : PagerAdapter() {
            override fun getCount(): Int = viewList.size

            override fun isViewFromObject(view: View, `object`: Any): Boolean =
                `object` == view

            override fun instantiateItem(container: ViewGroup, position: Int): Any {
                val view = viewList[position]
                container.addView(view)
                return view
            }

            override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
                container.removeView(viewList[position])
            }
        }

    private fun RecyclerView.getNews(type: Int) {
        HttpUtil.getTo(
            "press/press/list",
            map = mapOf("type" to type.toString()),
            callback = MyCallback {
                val list = jsonToModel(it, NewsList::class.java)
                this.adapter =
                    RxAdapter(requireContext(), R.layout.item_news, list.rows) { root, position ->
                        list.rows[position].let { item ->
                            root.findViewById<TextView>(R.id.titleNews).text = item.title
                            root.findViewById<ImageView>(R.id.coverNews).glide(item.cover ?: "")
                            root.findViewById<TextView>(R.id.contentNews).text =
                                item.content.removeHTML()
                            root.findViewById<TextView>(R.id.dateNews).text =
                                "发布时间:${item.publishDate}"
                            root.findViewById<TextView>(R.id.textNews).text =
                                "评论数:${item.commentNum}    阅读数:${item.readNum}   点赞数:${item.likeNum}"
                            root.setOnClickListener {
                                Intent(requireContext(), NewsInfoActivity::class.java).apply {
                                    putExtra("newsId", item.id)
                                    startActivity(this)
                                }
                            }
                        }
                    }
            })
    }

}