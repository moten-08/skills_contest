package com.example.smartcity1215.tool

import android.util.Log
import com.google.gson.Gson
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.toRequestBody
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object HttpUtil {
    private const val baseUrl1 = "http://10.10.230.112:10001"
    private const val baseUrl2 = "http://124.93.196.45:10001"
    const val baseUrl = baseUrl2
    private val retrofit: Retrofit = Retrofit.Builder()
        .baseUrl(baseUrl)
        .addConverterFactory(GsonConverterFactory.create())
        .build()
    val GSON = Gson()
    private val MEDIA_TYPE = "application/json".toMediaTypeOrNull()
    private val MEDIA_FILE = "multipart/form-data".toMediaTypeOrNull()

    private val service: FitService = retrofit.create(FitService::class.java)

    fun getTo(
        url: String,
        token: String = "",
        map: Map<String, String> = mapOf("" to ""),
        callback: MyCallback
    ) {
        val result = service.get(url, token, map).enqueue(callback)
        Log.d(url + "$map", "getTo: $result")
    }

    fun postTo(
        url: String,
        token: String = "",
        any: Any,
        callback: MyCallback
    ) {
        val body = GSON.toJson(any).toRequestBody(MEDIA_TYPE)
        val result = service.post(url, token, body).enqueue(callback)
        Log.d(url, "postTo: $result")
    }

    fun putTo(
        url: String,
        token: String = "",
        any: Any,
        callback: MyCallback
    ) {
        val body = GSON.toJson(any).toRequestBody(MEDIA_TYPE)
        val result = service.put(url, token, body).enqueue(callback)
        Log.d(url, "putTo: $result")
    }

    fun deleteTo(
        url: String,
        token: String = "",
        map: Map<String, String> = mapOf("" to ""),
        callback: MyCallback
    ) {
        val result = service.delete(url, token, map).enqueue(callback)
        Log.d(url, "deleteTo: $result")
    }

    fun upLoadTo(
        token: String = "",
        byteArray: ByteArray,
        callback: MyCallback
    ) {
        val body = MultipartBody.Builder()
            .setType(MultipartBody.FORM)
            .addFormDataPart("file", "file1215.jpeg", byteArray.toRequestBody(MEDIA_FILE))
            .build()
        val result = service.upLoad(token, body.parts).enqueue(callback)
        Log.d("upLoad", "$result")
    }

}