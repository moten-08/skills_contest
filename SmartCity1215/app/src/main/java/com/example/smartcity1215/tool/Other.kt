package com.example.smartcity1215.tool

import android.content.Context
import android.content.SharedPreferences
import android.graphics.Color
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.bumptech.glide.Glide
import com.google.android.material.snackbar.Snackbar
import java.util.regex.Pattern

fun SharedPreferences.open(block: SharedPreferences.Editor.() -> Unit) {
    val edit = this.edit()
    edit.block()
    edit.apply()
}

fun String.toast(context: Context) {
    Toast.makeText(context, this, Toast.LENGTH_SHORT).show()
}

fun String.snack(view: View, action: String = "", block: () -> Unit = {}) {

    val snackbar = Snackbar.make(view, this, Snackbar.LENGTH_SHORT)
        .setAction(action) { block() }
    snackbar.view.findViewById<TextView>(com.google.android.material.R.id.snackbar_text)
        .setTextColor(
            Color.WHITE
        )
    snackbar.show()
}

fun <T> jsonToModel(data: Any, clazz: Class<T>): T =
    HttpUtil.GSON.fromJson(HttpUtil.GSON.toJson(data), clazz)

fun ImageView.glide(url: Any, local: Boolean = false, circle: Boolean = false) {
    if (circle) {
        Glide.with(this.context)
            .load(if (local) url else HttpUtil.baseUrl + url)
            .circleCrop()
            .into(this)
    } else {
        Glide.with(this.context)
            .load(if (local) url else HttpUtil.baseUrl + url)
            .into(this)
    }

}

fun String.removeHTML() = Pattern.compile("<[^>]+>")
    .matcher(this)
    .replaceAll("")
    .replace("&nbsp;", "\n")
    .replace("&ensp;", "")
    .replace("&emsp;", "")