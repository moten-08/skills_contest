package com.example.smartcity1215.ui.myself

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.smartcity1215.R
import com.example.smartcity1215.tool.BaseActivity

class UPActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_up)

        title = "修改密码"
    }
}