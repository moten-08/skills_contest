package com.example.smartcity1215.ui.myself

import android.os.Bundle
import com.example.smartcity1215.R
import com.example.smartcity1215.tool.BaseActivity

class UUActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_uu)

        title = "个人信息"
    }
}