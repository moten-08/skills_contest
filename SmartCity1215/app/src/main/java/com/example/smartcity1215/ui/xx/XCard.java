package com.example.smartcity1215.ui.xx;

public class XCard {
    private int fImage;
    private String FT;
    private String St;
    private String TT;
    private String FoT;
    private String FiT;
    private String SiT;
    private String SeT;
    private int sImage;

    public XCard(int fImage, String FT, String st, String TT, String foT, String fiT, String siT, String seT) {
        this.fImage = fImage;
        this.FT = FT;
        St = st;
        this.TT = TT;
        FoT = foT;
        FiT = fiT;
        SiT = siT;
        SeT = seT;
    }

    public XCard(int fImage, String FT, String st) {
        this.fImage = fImage;
        this.FT = FT;
        St = st;
    }

    public XCard(int fImage, String FT, String st, String TT) {
        this.fImage = fImage;
        this.FT = FT;
        St = st;
        this.TT = TT;
    }

    public XCard(int fImage, String FT, String st, int sImage) {
        this.fImage = fImage;
        this.FT = FT;
        St = st;
        this.sImage = sImage;
    }

    public int getfImage() {
        return fImage;
    }

    public String getFT() {
        return FT;
    }

    public String getSt() {
        return St;
    }

    public String getTT() {
        return TT;
    }

    public int getsImage() {
        return sImage;
    }

    public String getFoT() {
        return FoT;
    }

    public String getFiT() {
        return FiT;
    }

    public String getSiT() {
        return SiT;
    }

    public String getSeT() {
        return SeT;
    }
}
