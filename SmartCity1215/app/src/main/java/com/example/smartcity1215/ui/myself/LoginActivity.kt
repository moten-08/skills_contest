package com.example.smartcity1215.ui.myself

import android.content.SharedPreferences
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import com.example.smartcity1215.R
import com.example.smartcity1215.request.LoginRequest
import com.example.smartcity1215.result.Login
import com.example.smartcity1215.tool.*
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : BaseActivity() {

    private lateinit var sp: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        sp = getSharedPreferences("data", MODE_PRIVATE)

        setContentView(R.layout.activity_login)

        val username = findViewById<EditText>(R.id.username)
        val password = findViewById<EditText>(R.id.password)
        val login = findViewById<Button>(R.id.login)
        val loading = findViewById<ProgressBar>(R.id.loading)

        login.setOnClickListener {
            val unstr = username.text.toString()
            val psstr = password.text.toString()
            login(LoginRequest(unstr, psstr))
            loading.visibility = View.VISIBLE
        }
    }

    private fun login(loginRequest: LoginRequest) {
        HttpUtil.postTo(
            "api/login",
            any = loginRequest,
            callback = MyCallback {
                val data = jsonToModel(it, Login::class.java)
                sp.open {
                    putString("token", data.token)
                }
                runOnUiThread {
                    data.msg.toast(applicationContext)
                    loading.visibility = View.GONE
                    if (data.code == 200) finish()
                }
            }
        )
    }
}
