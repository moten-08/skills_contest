package com.example.smartcity1215.ui.myself

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.smartcity1215.result.UserInfo
import com.example.smartcity1215.tool.HttpUtil
import com.example.smartcity1215.tool.MyCallback
import com.example.smartcity1215.tool.jsonToModel

class MyViewModel : ViewModel() {
    val userLive = MutableLiveData<UserInfo>()
    fun getUserInfo(token: String) {
        HttpUtil.getTo(
            "api/common/user/getInfo", token, callback = MyCallback {
                userLive.value = jsonToModel(it, UserInfo::class.java)
            })
    }
}