package com.example.smartcity1215.request

data class LoginRequest (val username:String, val password:String)