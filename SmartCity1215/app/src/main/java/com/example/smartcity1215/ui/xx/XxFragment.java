package com.example.smartcity1215.ui.xx;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.smartcity1215.R;
import com.youth.banner.Banner;
import com.youth.banner.BannerConfig;
import com.youth.banner.listener.OnBannerListener;
import com.youth.banner.loader.ImageLoader;

import java.util.Arrays;
import java.util.List;

public class XxFragment extends Fragment {
    List<Integer> images = Arrays.asList(R.mipmap.xx_main_image, R.mipmap.xx_main_image, R.mipmap.xx_main_image);
    List<String> messages = Arrays.asList("深圳市龙岗区南湾街道办事处通知\n2021.12.15 11:00", "深圳市龙岗区南湾街道办事处通知\n2021.12.15 13:00", "深圳市龙岗区南湾街道办事处通知\n2021.12.15 14:00");
    List<XCard> cards = Arrays.asList(new XCard(R.mipmap.xx_main_image, "标题标题标题标题标题标题1", "内容内容内容内容内容内容内容内容内容内容1"),
            new XCard(R.mipmap.xx_main_image, "标题标题标题标题标题标题2", "内容内容内容内容内容内容内容内容内容内容2"),
            new XCard(R.mipmap.xx_main_image, "标题标题标题标题标题标题3", "内容内容内容内容内容内容内容内容内容内容3"));

    private Banner banner;
    private LinearLayout tool1;
    private LinearLayout tool2;
    private LinearLayout tool3;
    private LinearLayout tool4;
    private RecyclerView MRec;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_xx, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        banner = (Banner) view.findViewById(R.id.banner);
        tool1 = (LinearLayout) view.findViewById(R.id.tool1);
        tool2 = (LinearLayout) view.findViewById(R.id.tool2);
        tool3 = (LinearLayout) view.findViewById(R.id.tool3);
        tool4 = (LinearLayout) view.findViewById(R.id.tool4);
        MRec = (RecyclerView) view.findViewById(R.id.M_Rec);


        banner.setImages(images).setImageLoader(new ImageLoader() {
            @Override
            public void displayImage(Context context, Object o, ImageView imageView) {
                Glide.with(context).load(o).centerCrop().into(imageView);
            }
        });
        banner.setBannerTitles(messages);
        banner.setBannerStyle(BannerConfig.CIRCLE_INDICATOR_TITLE_INSIDE);
        banner.setOnBannerListener(new OnBannerListener() {
            @Override
            public void OnBannerClick(int i) {
                startActivity(new Intent(getContext(),SQDTActivity.class));
            }
        });
        banner.start();

        tool1.setOnClickListener(v -> startActivity(new Intent(getContext(), T1Activity.class)));
        tool2.setOnClickListener(v -> startActivity(new Intent(getContext(), T2Activity.class)));
        tool3.setOnClickListener(v -> startActivity(new Intent(getContext(), T3Activity.class)));
        tool4.setOnClickListener(v -> startActivity(new Intent(getContext(), T4Activity.class)));

        MRec.setLayoutManager(new LinearLayoutManager(getContext()));
        MRec.setAdapter(new MyAdapter(getContext(), R.layout.card_layout, cards) {
            @Override
            public void init(View view, int position) {
                XCard c = cards.get(position);
                ImageView cardImage = (ImageView) view.findViewById(R.id.card_image);
                TextView cardFtext = (TextView) view.findViewById(R.id.card_ftext);
                TextView cardStext = (TextView) view.findViewById(R.id.card_stext);
                TextView cardTtext = (TextView) view.findViewById(R.id.card_ttext);
                Glide.with(view).load(c.getfImage()).centerCrop().into(cardImage);
                cardFtext.setText(c.getFT());
                cardStext.setText(c.getSt());
                view.setOnClickListener(v -> startActivity(new Intent(getContext(),SYTGActivity.class)));
            }
        });

    }
}