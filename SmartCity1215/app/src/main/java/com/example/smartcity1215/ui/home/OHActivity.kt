package com.example.smartcity1215.ui.home

import android.os.Bundle
import com.example.smartcity1215.R
import com.example.smartcity1215.tool.BaseActivity

class OHActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_oh)

        title = "门诊预约"
    }
}