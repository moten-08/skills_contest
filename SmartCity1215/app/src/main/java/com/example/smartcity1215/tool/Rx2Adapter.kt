package com.example.smartcity1215.tool

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

abstract class Rx2Adapter(
    private val context: Context,
    private val resource: Int,
    private val list: List<Any>
) : RecyclerView.Adapter<Rx2Adapter.ViewHolder>() {
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(LayoutInflater.from(context).inflate(resource, parent, false))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        block(holder.itemView,position)
    }

    override fun getItemCount(): Int = list.size

    abstract fun block(root:View,position: Int)
}