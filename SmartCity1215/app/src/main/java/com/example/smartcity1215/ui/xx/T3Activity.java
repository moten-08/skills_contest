package com.example.smartcity1215.ui.xx;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.smartcity1215.R;
import com.example.smartcity1215.tool.BaseActivity;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class T3Activity extends BaseActivity {
    public static List<XCard> cardList = new ArrayList<>();
    XCard[] cards = {new XCard(R.mipmap.ic_tou_xiang, "李同学", "英雄不容亵渎 烈士不容诋毁", R.mipmap.t3_1),
            new XCard(R.mipmap.ic_tou_xiang, "张同学", "努力学习，为国家的建设贡献力量", R.mipmap.t3_2),
            new XCard(R.mipmap.ic_tou_xiang, "刘老师", "地铁十号线十号线招安保员\n" +
                    "女：18-38 165cm以上\n" +
                    "男：18-38 175cm以上\n" +
                    "底薪4600.加班费一小时16 公寓房两人房四人房 管吃管住 买五险\n" +
                    "电话：13213434319\n",0),
            new XCard(R.mipmap.ic_tou_xiang, "张同学", "理性爱国首先是合法爱国，而不是违法爱国。理性爱国是指在日本右翼势力抛出购岛言论后，激起各地进行反日游行活动后，一些不法分子冒充爱国青年混杂在游行、抗议队伍中进行打砸抢等活动。", R.mipmap.t3_2)};
    private RecyclerView T3Rec;
    private ImageButton imageButton;

    @Override
    protected void onRestart() {
        super.onRestart();
        T3Rec.getAdapter().notifyDataSetChanged();
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_t3);

        Random random = new Random();
        for (int i = 0; i < 2; i++) {
            int j = random.nextInt(cards.length);
            cardList.add(cards[j]);
        }


        T3Rec = (RecyclerView) findViewById(R.id.T3_Rec);
        imageButton = (ImageButton) findViewById(R.id.imageButton);

        imageButton.setOnClickListener(v -> {
            Intent intent = new Intent(T3Activity.this, T3_1Activity.class);
            startActivity(intent);
        });


        T3Rec.setLayoutManager(new LinearLayoutManager(this));
        T3Rec.setAdapter(new MyAdapter(this, R.layout.t3_layout, cardList) {
            @Override
            public void init(View view, int position) {
                XCard c = cardList.get(position);
                ImageView cardImage = (ImageView) view.findViewById(R.id.card_image);
                TextView cardFtext = (TextView) view.findViewById(R.id.card_ftext);
                TextView cardStext = (TextView) view.findViewById(R.id.card_stext);
                ImageView cardImage2 = (ImageView) view.findViewById(R.id.card_image2);
                Glide.with(view).load(c.getfImage()).circleCrop().centerCrop().into(cardImage);
                if (c.getsImage() == 0){
                    cardImage2.setVisibility(View.GONE);
                }
                Glide.with(view).load(c.getsImage()).centerCrop().into(cardImage2);
                cardFtext.setText(c.getFT());
                cardStext.setText(c.getSt());
            }
        });
    }
}