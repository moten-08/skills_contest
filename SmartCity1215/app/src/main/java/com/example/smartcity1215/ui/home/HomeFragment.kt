package com.example.smartcity1215.ui.home

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.smartcity1215.MainActivity
import com.example.smartcity1215.R
import com.example.smartcity1215.result.BannerHome
import com.example.smartcity1215.result.NewsList
import com.example.smartcity1215.result.ServiceList
import com.example.smartcity1215.tool.RxAdapter
import com.example.smartcity1215.tool.glide
import com.example.smartcity1215.tool.toast
import com.example.smartcity1215.ui.news.NewsInfoActivity
import com.example.smartcity1215.ui.news.NewsListActivity
import com.youth.banner.Banner
import com.youth.banner.loader.ImageLoader
import kotlinx.android.synthetic.main.fragment_home.*

class HomeFragment : Fragment() {

    private lateinit var homeViewModel: HomeViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        homeViewModel =
            ViewModelProviders.of(this).get(HomeViewModel::class.java)
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        homeViewModel.apply {
            getBannerHome()
            bannerHome.observe(viewLifecycleOwner) {
                view.findViewById<Banner>(R.id.bannerHome).apply {
                    setImages(it.rows)
                    setImageLoader(imageLoader())
                    start()
                }
            }

            getService()
            allService.observe(viewLifecycleOwner) {
                view.findViewById<RecyclerView>(R.id.rvAll).apply {
                    layoutManager = GridLayoutManager(requireContext(), 5)
                    val list = it.rows.subList(0, 10)
                    list[8] = it.rows[10]
                    list[9].serviceName = "更多服务"
                    list[9].imgUrl = R.drawable.ic_baseline_more_horiz_24
                    adapter = allRxAdapter(list)
                }
            }

            getNews()
            newsList.observe(viewLifecycleOwner) {
                view.findViewById<RecyclerView>(R.id.rvHot).apply {
                    layoutManager = GridLayoutManager(requireContext(), 2)
                    adapter = newsRxAdapter(it)
                }
            }
        }
        search.setOnEditorActionListener { _, actionId, _ ->
            val searchStr = search.text.toString()
            if (searchStr.trim().isEmpty()) {
                "搜索内容不能为空".toast(requireContext())
                return@setOnEditorActionListener false
            }
            search.isCursorVisible = false
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                sear(searchStr)
            }
            true
        }
    }

    private fun newsRxAdapter(it: NewsList) =
        RxAdapter(requireContext(), R.layout.item_card, it.rows) { root, position ->
            root.findViewById<ImageView>(R.id.cover).glide(it.rows[position].cover)
            root.findViewById<TextView>(R.id.title).text = it.rows[position].title
            root.setOnClickListener { _ -> runNewsInfo(it.rows[position].id) }
        }

    private fun runNewsInfo(id: Int) {
        Intent(requireContext(), NewsInfoActivity::class.java).apply {
            putExtra("newsId", id)
            startActivity(this)
        }
    }

    private fun allRxAdapter(list: MutableList<ServiceList.RowsDTO>) =
        RxAdapter(requireContext(), R.layout.item_service, list) { root, position ->
            root.findViewById<ImageView>(R.id.serviceImg)
                .glide(list[position].imgUrl, local = (position == 9))
            root.findViewById<TextView>(R.id.serviceName).text =
                list[position].serviceName
            root.setOnClickListener { serviceOnclick(list, position) }
        }

    private fun serviceOnclick(
        list: MutableList<ServiceList.RowsDTO>,
        position: Int
    ) {
        when (list[position].serviceName) {
            "更多服务" -> (requireActivity() as MainActivity).swith()
            "活动管理" -> startActivity(
                Intent(
                    requireContext(),
                    AIActivity::class.java
                )
            )
            "智慧交管" -> startActivity(
                Intent(
                    requireContext(),
                    STActivity::class.java
                )
            )
            "门诊预约" -> startActivity(
                Intent(
                    requireContext(),
                    OHActivity::class.java
                )
            )
            else -> "敬请期待".toast(requireContext())
        }
    }

    private fun sear(string: String) {
        Intent(requireContext(), NewsListActivity::class.java).apply {
            putExtra("search", string)
            putExtra("type", "newsTitle")
            startActivity(this)
        }
    }

    private fun imageLoader() = object : ImageLoader() {
        override fun displayImage(p0: Context, p1: Any, p2: ImageView) {
            val data = p1 as BannerHome.RowsDTO
            p2.glide(data.advImg)
            p2.setOnClickListener { runNewsInfo(data.id) }

        }
    }
}