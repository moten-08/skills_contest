package com.example.smartcity1215.ui.home

import android.os.Bundle
import com.example.smartcity1215.R
import com.example.smartcity1215.tool.BaseActivity

class STActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_st)

        title = "违章查询"
    }
}