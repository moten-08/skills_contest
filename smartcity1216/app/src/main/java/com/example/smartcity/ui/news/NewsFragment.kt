package com.example.smartcity.ui.news

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import com.example.smartcity.R
import com.example.smartcity.result.NewsList
import com.example.smartcity.tools.*
import com.google.android.material.tabs.TabLayout

class NewsFragment : Fragment() {

    private lateinit var newsViewModel: NewsViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        newsViewModel =
            ViewModelProviders.of(this).get(NewsViewModel::class.java)
        return inflater.inflate(R.layout.fragment_news, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val views = ArrayList<View>()

        val tabNews = view.findViewById<TabLayout>(R.id.tabNews)
        val vpNews = view.findViewById<ViewPager>(R.id.vpNews)

        newsViewModel.apply {
            getCategory()
            categoryList.observe(viewLifecycleOwner) {
                it.data.forEach { index ->
                    val view = View.inflate(requireContext(), R.layout.item_list, null)
                    getNewsList(view.findViewById(R.id.listR), index.id)
                    views.add(view)
                }
                vpNews.adapter = pagerAdapter(views)
                tabNews.setupWithViewPager(vpNews)
                it.data.forEachIndexed { index, dataDTO ->
                    tabNews.getTabAt(index)?.text = dataDTO.name
                }
            }
        }
    }

    private fun getNewsList(view: RecyclerView, id: Int) {
        HttpUtil.service.get(
            "press/press/list",
            map = mapOf("type" to id.toString())
        ).enqueue(MyCallback {
            view.layoutManager = LinearLayoutManager(requireContext())
            view.adapter = rxAdapter(HttpUtil.jsonToModel(it, NewsList::class.java))
        })
    }

    private fun rxAdapter(data: NewsList) =
        RxAdapter(requireContext(), R.layout.item_news_i, data.rows) { r, p ->
            r.findViewById<TextView>(R.id.titleNews).text = data.rows[p].title
            r.findViewById<ImageView>(R.id.coverNews).glide(data.rows[p].cover)
            r.findViewById<TextView>(R.id.contentNews).text =
                data.rows[p].content.removeHTML()
            r.findViewById<TextView>(R.id.dateNews).text =
                "发布时间:${data.rows[p].publishDate}"
            r.findViewById<TextView>(R.id.textNews).text =
                "评论总数:${data.rows[p].commentNum}"
            r.setOnClickListener {
                Intent(requireContext(), NewsInfoActivity::class.java).apply {
                    putExtra("newsId", data.rows[p].id)
                    startActivity(this)

                }
            }
        }
}
