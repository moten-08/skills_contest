package com.example.smartcity.ui.news

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.smartcity.result.CategoryList
import com.example.smartcity.tools.HttpUtil
import com.example.smartcity.tools.MyCallback

class NewsViewModel : ViewModel() {

    val categoryList = MutableLiveData<CategoryList>()
    fun getCategory() {
        HttpUtil.service.get(
            "press/category/list"
        ).enqueue(MyCallback {
            categoryList.value = HttpUtil.jsonToModel(it, CategoryList::class.java)
        })
    }
}
