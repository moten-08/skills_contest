package com.example.smartcity.ui.home

import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.smartcity.R
import com.example.smartcity.result.BannerHome
import com.example.smartcity.tools.RxAdapter
import com.example.smartcity.tools.glide
import com.example.smartcity.tools.serviceRxAdapter
import com.example.smartcity.tools.toast
import com.example.smartcity.ui.news.NewsInfoActivity
import com.youth.banner.Banner
import com.youth.banner.loader.ImageLoader

class HomeFragment : Fragment() {

    private lateinit var homeViewModel: HomeViewModel
    private lateinit var searchHome: EditText
    private lateinit var bannerHome: Banner
    private lateinit var title1Home: TextView
    private lateinit var serviceHome: RecyclerView
    private lateinit var title2Home: TextView
    private lateinit var hotHome: RecyclerView
    private lateinit var title3Home: TextView

    private var serviceN = 5
    private var hotN = 2

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        homeViewModel =
            ViewModelProviders.of(this).get(HomeViewModel::class.java)
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView(view)
        initData()

        searchHome.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                val searStr = searchHome.text.toString()
                if (searStr.trim().isEmpty()) {
                    "搜索内容不能为空".toast(requireContext())
                    return@setOnEditorActionListener false
                }
                search(searStr)
            }

            true
        }
    }

    private fun initData() {
        homeViewModel.apply {
            getBanner()
            bannerLive.observe(viewLifecycleOwner) {
                bannerHome.apply {
                    setImages(it.rows)
                    setImageLoader(imageLoader())
                    start()
                }
            }
            getServices()
            servicesLive.observe(viewLifecycleOwner) {
                val list = it.rows.subList(0, serviceN * 2)
                list[serviceN * 2 - 1].serviceName = "更多服务"
                list[serviceN * 2 - 1].imgUrl = R.drawable.ic_baseline_more_horiz_24
                serviceHome.adapter = serviceRxAdapter(requireContext(), list)
            }
            getHotNews()
            newsList.observe(viewLifecycleOwner) {
                hotHome.adapter = RxAdapter(requireContext(), R.layout.item_news_h, it.rows) { r, p ->
                    r.findViewById<ImageView>(R.id.coverNews).glide(it.rows[p].cover)
                    r.findViewById<TextView>(R.id.titleNews).text = it.rows[p].title
                    r.setOnClickListener { _ ->
                        Intent(requireContext(), NewsInfoActivity::class.java).apply {
                            putExtra("newsId", it.rows[p].id)
                            startActivity(this)
                        }
                    }
                }
            }
        }
    }

    private fun search(searStr: String) {
        Intent(requireContext(), SearchActivity::class.java).apply {
            putExtra("search", searStr)
            startActivity(this)
        }
    }

    private fun imageLoader() = object : ImageLoader() {
        override fun displayImage(p0: Context, p1: Any, p2: ImageView) {
            with(p1 as BannerHome.RowsDTO) {
                p2.glide(advImg)
                p2.setOnClickListener {
                    Intent(requireContext(), NewsInfoActivity::class.java).apply {
                        putExtra("newsId", id)
                        startActivity(this)
                    }
                }
            }
        }
    }

    private fun initView(view: View) {
        searchHome = view.findViewById(R.id.search_home)
        bannerHome = view.findViewById(R.id.banner_home)
        title1Home = view.findViewById(R.id.title1_home)
        serviceHome = view.findViewById(R.id.service_home)
        title2Home = view.findViewById(R.id.title2_home)
        hotHome = view.findViewById(R.id.hot_home)
        title3Home = view.findViewById(R.id.title3_home)

        if (isTable()) {
            serviceN = 6
            hotN = 4
        }
        serviceHome.layoutManager = GridLayoutManager(requireContext(), serviceN)
        hotHome.layoutManager = GridLayoutManager(requireContext(), hotN)
    }

    private fun isTable(): Boolean {
        val s = requireContext().resources.configuration.screenLayout
        val size = Configuration.SCREENLAYOUT_SIZE_LARGE
        return (s and Configuration.SCREENLAYOUT_SIZE_MASK) >= size
    }
}
