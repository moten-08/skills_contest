package com.example.smartcity.ui.home

import android.os.Bundle
import com.example.smartcity.R
import com.example.smartcity.tools.BaseActivity

class SearchActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)

        val searchStr = intent.getStringExtra("search") ?: ""
        title = "\"$searchStr\"的搜索结果"
    }
}