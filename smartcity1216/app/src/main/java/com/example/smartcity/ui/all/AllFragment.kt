package com.example.smartcity.ui.all

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import androidx.constraintlayout.widget.Guideline
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.smartcity.R
import com.example.smartcity.result.ServiceHome
import com.example.smartcity.tools.RxAdapter
import com.example.smartcity.tools.serviceRxAdapter
import com.example.smartcity.ui.home.HomeViewModel


class AllFragment : Fragment() {

    private lateinit var viewModel: HomeViewModel
    private lateinit var line: Guideline
    private lateinit var searchAll: EditText
    private lateinit var types: RecyclerView
    private lateinit var services: RecyclerView

    private val index = MutableLiveData<Int>().apply { value = 0 }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        viewModel =
            ViewModelProviders.of(this).get(HomeViewModel::class.java)
        return inflater.inflate(R.layout.fragment_all, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView(view)

        val set = mutableSetOf<String>()
        val list = ArrayList<String>()
        viewModel.apply {
            getServices()
            servicesLive.observe(viewLifecycleOwner) {
                it.rows.forEach { item ->
                    set.add(item.serviceType)
                }
                list.addAll(set.toList())
                types.adapter = RxAdapter(
                    requireContext(),
                    R.layout.item_type,
                    set.toList()
                ) { r, p ->
                    r.findViewById<TextView>(R.id.textIT).apply {
                        text = set.toList()[p]
                    }
                    r.setOnClickListener { index.value = p }
                }
                index.observe(viewLifecycleOwner) { i ->
                    val list2 = ArrayList<ServiceHome.RowsDTO>()
                    it.rows.forEach { item ->
                        if (item.serviceType == list[i]) list2.add(item)
                    }
                    services.adapter = serviceRxAdapter(requireContext(), list2)
                }
            }
        }
    }

    private fun initView(view: View) {
        line = view.findViewById(R.id.line) as Guideline
        searchAll = view.findViewById(R.id.searchAll) as EditText
        types = view.findViewById(R.id.types) as RecyclerView
        services = view.findViewById(R.id.services) as RecyclerView

        types.layoutManager = LinearLayoutManager(requireContext())
        services.layoutManager = GridLayoutManager(requireContext(), 3)
    }
}