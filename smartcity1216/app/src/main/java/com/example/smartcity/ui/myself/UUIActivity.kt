package com.example.smartcity.ui.myself

import android.content.Intent
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.widget.*
import com.example.smartcity.R
import com.example.smartcity.request.UserUpdate
import com.example.smartcity.result.BaseResult
import com.example.smartcity.result.FileLoad
import com.example.smartcity.result.UserInfo
import com.example.smartcity.tools.*

class UUIActivity : BaseActivity() {

    private val avatarU: ImageView by lazy { findViewById(R.id.avatar) }
    private val upload: Button by lazy { findViewById(R.id.upload) }
    private val tip: TextView by lazy { findViewById(R.id.tip) }
    private val username: TextView by lazy { findViewById(R.id.username) }
    private val nickEd: EditText by lazy { findViewById(R.id.nickEd) }
    private val sex0: RadioButton by lazy { findViewById(R.id.sex0) }
    private val sex1: RadioButton by lazy { findViewById(R.id.sex1) }
    private val phoneEd: EditText by lazy { findViewById(R.id.phoneEd) }
    private val idCardU: TextView by lazy { findViewById(R.id.idCard) }
    private val emailU: TextView by lazy { findViewById(R.id.email) }
    private val balanceU: TextView by lazy { findViewById(R.id.balance) }
    private val scoreU: TextView by lazy { findViewById(R.id.score) }
    private val save: Button by lazy { findViewById(R.id.save) }

    private lateinit var idCardStr: String
    private lateinit var filePath: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_uui)

        title = "个人信息"

        val userInfo = intent.getSerializableExtra("userInfo") as UserInfo.UserDTO
        userInfo.apply {
            avatarU.glide(if ("/prod-api" in avatar) avatar else "/prod-api$avatar")
            filePath = avatar
            username.text = "用户名:$userName"
            nickEd.setText(nickName)
            sex0.isChecked = (sex == "0")
            sex1.isChecked = (sex == "1")
            phoneEd.setText(phonenumber)
            idCardStr = if ((idCard ?: "").trim().isEmpty()) {
                "暂未上传证件"
            } else {
                val idc = StringBuffer(idCard)
                idc.replace(2, idc.length - 4, "*".repeat(idc.length - 6)).toString()
            }
            idCardU.text = "证件号:$idCardStr"
            emailU.text = "邮箱:${email ?: "暂无"}"
            balanceU.text = "账户余额:$balance"
            scoreU.text = "用户积分:$score"

            save.setOnClickListener {
                val nicknameR = nickEd.text.toString()
                val phoneR = phoneEd.text.toString()
                val sexR = if (sex0.isChecked) "0" else "1"
                val avatarR = filePath

                common(UserUpdate(nicknameR, phoneR, sexR, avatarR))
            }
            avatarU.setOnClickListener {
                Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI).apply {
                    startActivityForResult(this, 123)
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK) {
            if (requestCode == 123) {
                val uri = data?.data
                avatarU.glide(uri, local = true)
                if (uri != null) {
                    val inputStream = contentResolver.openInputStream(uri)
                    val bytes = ByteArray(inputStream!!.available())
                    inputStream.read(bytes)
                    tip.visibility = View.VISIBLE
                    upload.setOnClickListener { upFile(bytes) }

                }
            }
        }
    }

    private fun upFile(bytes: ByteArray) {
        HttpUtil.uploadTo(token, bytes)
            .enqueue(MyCallback {
                Log.e("name",Thread.currentThread().name)
                HttpUtil.jsonToModel(it, FileLoad::class.java).apply {
                    (msg ?: "错误").toast(this@UUIActivity)
                    if (code == 200) {
                        tip.visibility = View.GONE
                        filePath = fileName
                    }
                }
            })
    }

    private fun common(userUpdate: UserUpdate) {
        HttpUtil.putTo(
            "api/common/user", token, userUpdate
        ).enqueue(MyCallback {
            val data = HttpUtil.jsonToModel(it, BaseResult::class.java)
            data.msg.toast(this)
            if (data.code == 200) finish()
        })
    }
}
