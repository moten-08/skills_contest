package com.example.smartcity.request

data class Feedback(val titleStr: String, val feedback: String)
