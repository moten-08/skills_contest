package com.example.smartcity.tools

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.viewpager.widget.PagerAdapter
import com.bumptech.glide.Glide
import com.example.smartcity.MainActivity
import com.example.smartcity.R
import com.example.smartcity.result.ServiceHome
import com.example.smartcity.ui.live.LEActivity
import com.example.smartcity.ui.stop.SWActivity
import com.google.android.material.snackbar.Snackbar
import java.util.regex.Pattern

fun ImageView.glide(url: Any? = null, local: Boolean = false, circle: Boolean = false) {
    if (circle) {
        Glide.with(this.context)
            .load(if (local) url else HttpUtil.BASEURL + url)
            .circleCrop()
            .into(this)
    } else {
        Glide.with(this.context)
            .load(if (local) url else HttpUtil.BASEURL + url)
            .into(this)
    }
}

fun SharedPreferences.open(block: SharedPreferences.Editor.() -> Unit) {
    val edit = this.edit()
    edit.block()
    edit.apply()
}

fun String.toast(context: Context) {
    Toast.makeText(context, this, Toast.LENGTH_SHORT).show()
}

fun String.snack(view: View, action: String? = null, block: () -> Unit = {}) {
    Snackbar.make(view, this, Snackbar.LENGTH_SHORT)
        .setAction(action) { block() }
        .show()
}

fun TextView.toText(): String = this.text.toString()

fun String.removeHTML(): String = Pattern.compile("<[^>]+>")
    .matcher(this)
    .replaceAll("")
    .replace("&nbsp;", "\n")
    .replace("&ensp;", "")
    .replace("&emsp;", "")

fun pagerAdapter(viewList: ArrayList<View>) =
    object : PagerAdapter() {
        override fun getCount(): Int = viewList.size

        override fun isViewFromObject(view: View, `object`: Any): Boolean =
            `object` == view

        override fun instantiateItem(container: ViewGroup, position: Int): Any {
            val view = viewList[position]
            container.addView(view)
            return view
        }

        override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
            container.removeView(viewList[position])
        }
    }

fun TextView.format(vararg arg: Any) {
    this.text = String.format(this.toText(), *arg)
}

fun Int.forSize(): String {
    return if (this > 9) "$this" else "0$this"
}

fun serviceRxAdapter(context: Context, list: MutableList<ServiceHome.RowsDTO>) =
    RxAdapter(context, R.layout.item_service, list) { r, p ->
        r.findViewById<ImageView>(R.id.imageS)
            .glide(list[p].imgUrl, local = (p == 9))
        r.findViewById<TextView>(R.id.nameS).text = list[p].serviceName
        r.setOnClickListener {
            when (list[p].serviceName) {
                "停哪儿" -> context.startActivity(Intent(context, SWActivity::class.java))
                "生活缴费" -> context.startActivity(Intent(context,LEActivity::class.java))
                "更多服务" -> (context as MainActivity).switch()
                else -> "敬请期待".toast(context)
            }
        }
    }
