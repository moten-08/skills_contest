package com.example.smartcity.ui.xx;

public class XCard {
    private int F_Image;
    private String F_Text;
    private String S_Text;
    private String T_Text;

    public XCard() {
    }

    public XCard(String f_Text, String s_Text) {
        F_Text = f_Text;
        S_Text = s_Text;
    }

    public int getF_Image() {
        return F_Image;
    }

    public String getF_Text() {
        return F_Text;
    }

    public String getS_Text() {
        return S_Text;
    }

    public void setF_Image(int f_Image) {
        F_Image = f_Image;
    }

    public void setF_Text(String f_Text) {
        F_Text = f_Text;
    }

    public void setS_Text(String s_Text) {
        S_Text = s_Text;
    }

    public String getT_Text() {
        return T_Text;
    }

    public void setT_Text(String t_Text) {
        T_Text = t_Text;
    }
}
