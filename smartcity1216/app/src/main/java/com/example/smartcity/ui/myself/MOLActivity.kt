package com.example.smartcity.ui.myself

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import com.example.smartcity.R
import com.example.smartcity.result.Dict
import com.example.smartcity.result.OrderInfo
import com.example.smartcity.result.OrderList
import com.example.smartcity.tools.*
import com.google.android.material.tabs.TabLayout

class MOLActivity : BaseActivity() {

    private val tabNews: TabLayout by lazy { findViewById(R.id.tabNews) }
    private val vpNews: ViewPager by lazy { findViewById(R.id.vpNews) }

    private val dictMap = MutableLiveData<Dict>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_news)

        title = "订单列表"

        val viewList = ArrayList<View>()
        getDict()
        dictMap.observe(this) {
            it.data.forEach { item ->
                val view = View.inflate(this, R.layout.item_list, null)
                view.findViewById<RecyclerView>(R.id.listR).getAllOrder(item.dictLabel)
                viewList.add(view)
            }
            vpNews.adapter = pagerAdapter(viewList)
            tabNews.setupWithViewPager(vpNews)
            it.data.forEachIndexed { index, item ->
                tabNews.getTabAt(index)?.text = item.dictLabel
            }
        }

    }

    private fun RecyclerView.getAllOrder(dictLabel: String) {
        HttpUtil.service.get(
            "api/allorder/list", token, mapOf("orderStatus" to dictLabel)
        ).enqueue(MyCallback {
            HttpUtil.jsonToModel(it, OrderList::class.java).apply {
                Log.d("TAG", "$this")
                layoutManager = LinearLayoutManager(this@MOLActivity)
                adapter = RxAdapter(this@MOLActivity, R.layout.item_order, rows) { r, p ->
                    r.findViewById<TextView>(R.id.orderNo).format(rows[p].orderNo)
                    r.findViewById<TextView>(R.id.orderType).format(rows[p].orderTypeName)
                    r.findViewById<TextView>(R.id.createTime).getCreateTime(rows[p].orderNo)
                    r.setOnClickListener {
                        Intent(this@MOLActivity, OrderInfoActivity::class.java).apply {
                            putExtra("orderNo", rows[p].orderNo)
                            startActivity(this)
                        }
                    }
                }
            }
        })
    }

    private fun TextView.getCreateTime(orderNo: String) {
        HttpUtil.service.get(
            "api/takeout/order/$orderNo", token
        ).enqueue(MyCallback {
            val data = HttpUtil.jsonToModel(it, OrderInfo::class.java)
            this.format(data.data.orderInfo.createTime)
        })
    }

    private fun getDict() {
        HttpUtil.service.get(
            "system/dict/data/type/tk_order_status"
        ).enqueue(MyCallback {
            dictMap.value = HttpUtil.jsonToModel(it, Dict::class.java)
        })
    }
}
