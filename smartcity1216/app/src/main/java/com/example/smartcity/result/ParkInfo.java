package com.example.smartcity.result;

public class ParkInfo {

    public String msg;
    public int code;
    public DataDTO data;

    public static class DataDTO {
        public Object searchValue;
        public Object createBy;
        public Object createTime;
        public Object updateBy;
        public Object updateTime;
        public Object remark;
        public ParamsDTO params;
        public int id;
        public String parkName;
        public String vacancy;
        public String priceCaps;
        public String imgUrl;
        public String rates;
        public String address;
        public String distance;
        public String allPark;
        public String open;

        public static class ParamsDTO {
        }
    }
}
