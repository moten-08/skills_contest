package com.example.smartcity.request

data class UserUpdate (
    var nickName: String,
    var phonenumber: String,
    var sex: String,
    var avatar: String
)