package com.example.smartcity.ui.xx;

import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.smartcity.R;
import com.example.smartcity.tools.BaseActivityX;
import com.google.android.material.tabs.TabLayout;

public class T3Activity extends BaseActivityX {
    private TextView textView4;
    private TabLayout T3Tab;
    private TextView T3TabButton;
    private TextView T3TabButton2;
    private TextView T3TabButton3;
    private TextView T3TabButton4;
    private TextView T3TabButton5;
    private TextView T3TabButton6;
    private TextView textView7;
    private EditText T3Edit1;
    private EditText T3Edit2;
    private EditText T3Edit3;
    private EditText T3Edit4;
    private TextView T3_button_c;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTitle("信息预留");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_t3);

        textView4 = (TextView) findViewById(R.id.textView4);
        T3Tab = (TabLayout) findViewById(R.id.T3_Tab);
        T3TabButton = (TextView) findViewById(R.id.T3_Tab_Button);
        T3TabButton2 = (TextView) findViewById(R.id.T3_Tab_Button2);
        T3TabButton3 = (TextView) findViewById(R.id.T3_Tab_Button3);
        T3TabButton4 = (TextView) findViewById(R.id.T3_Tab_Buttom4);
        T3TabButton5 = (TextView) findViewById(R.id.T3_Tab_Button5);
        T3TabButton6 = (TextView) findViewById(R.id.T3_Tab_Button6);
        textView7 = (TextView) findViewById(R.id.textView7);
        T3Edit1 = (EditText) findViewById(R.id.T3_Edit1);
        T3Edit2 = (EditText) findViewById(R.id.T3_Edit2);
        T3Edit3 = (EditText) findViewById(R.id.T3_Edit3);
        T3Edit4 = (EditText) findViewById(R.id.T3_Edit4);
        T3_button_c = (TextView) findViewById(R.id.T3_button_c);

        T3TabButton.setOnClickListener(view -> {
            T3TabButton.setTextColor(getColor(android.R.color.black));
            T3TabButton2.setTextColor(getColor(android.R.color.white));
            T3TabButton3.setTextColor(getColor(android.R.color.white));
            T3TabButton4.setTextColor(getColor(android.R.color.white));
            T3TabButton5.setTextColor(getColor(android.R.color.white));
            T3TabButton6.setTextColor(getColor(android.R.color.white));
        });
        T3TabButton2.setOnClickListener(view -> {
            T3TabButton2.setTextColor(getColor(android.R.color.black));
            T3TabButton.setTextColor(getColor(android.R.color.white));
            T3TabButton3.setTextColor(getColor(android.R.color.white));
            T3TabButton4.setTextColor(getColor(android.R.color.white));
            T3TabButton5.setTextColor(getColor(android.R.color.white));
            T3TabButton6.setTextColor(getColor(android.R.color.white));
        });
        T3TabButton3.setOnClickListener(view -> {
            T3TabButton3.setTextColor(getColor(android.R.color.black));
            T3TabButton2.setTextColor(getColor(android.R.color.white));
            T3TabButton.setTextColor(getColor(android.R.color.white));
            T3TabButton4.setTextColor(getColor(android.R.color.white));
            T3TabButton5.setTextColor(getColor(android.R.color.white));
            T3TabButton6.setTextColor(getColor(android.R.color.white));
        });
        T3TabButton4.setOnClickListener(view -> {
            T3TabButton4.setTextColor(getColor(android.R.color.black));
            T3TabButton2.setTextColor(getColor(android.R.color.white));
            T3TabButton3.setTextColor(getColor(android.R.color.white));
            T3TabButton.setTextColor(getColor(android.R.color.white));
            T3TabButton5.setTextColor(getColor(android.R.color.white));
            T3TabButton6.setTextColor(getColor(android.R.color.white));
        });
        T3TabButton5.setOnClickListener(view -> {
            T3TabButton5.setTextColor(getColor(android.R.color.black));
            T3TabButton2.setTextColor(getColor(android.R.color.white));
            T3TabButton3.setTextColor(getColor(android.R.color.white));
            T3TabButton4.setTextColor(getColor(android.R.color.white));
            T3TabButton.setTextColor(getColor(android.R.color.white));
            T3TabButton6.setTextColor(getColor(android.R.color.white));
        });
        T3TabButton6.setOnClickListener(view -> {
            T3TabButton6.setTextColor(getColor(android.R.color.black));
            T3TabButton2.setTextColor(getColor(android.R.color.white));
            T3TabButton3.setTextColor(getColor(android.R.color.white));
            T3TabButton4.setTextColor(getColor(android.R.color.white));
            T3TabButton5.setTextColor(getColor(android.R.color.white));
            T3TabButton.setTextColor(getColor(android.R.color.white));
        });

        T3_button_c.setOnClickListener(view -> {
            Toast.makeText(this,"保存成功",Toast.LENGTH_SHORT).show();
            finish();
        });
    }
}