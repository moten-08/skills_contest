package com.example.smartcity.tools

import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*

interface FitService {
    @Headers("Content-Type:application/json;charset=UTF-8")

    @POST("/prod-api/{url}")
    fun post(
        @Path("url") url: String,
        @Header("Authorization") token: String = "",
        @Body body: RequestBody
    ): Call<Any>

    @GET("/prod-api/{url}")
    fun get(
        @Path("url") url: String,
        @Header("Authorization") token: String = "",
        @QueryMap map: Map<String, String> = mapOf()
    ): Call<Any>

    @PUT("/prod-api/{url}")
    fun put(
        @Path("url") url: String,
        @Header("Authorization") token: String = "",
        @Body body: RequestBody
    ): Call<Any>

    @DELETE("/prod-api/{url}")
    fun delete(
        @Path("url") url: String,
        @Header("Authorization") token: String = ""
    ): Call<Any>

    @Multipart
    @POST("/prod-api/common/upload")
    fun upload(
        @Header("Authorization") token: String = "",
        @Part parts: List<MultipartBody.Part>
    ): Call<Any>
}