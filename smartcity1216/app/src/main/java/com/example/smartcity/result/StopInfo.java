package com.example.smartcity.result;

import java.util.List;

public class StopInfo {

    public int total;
    public List<RowsDTO> rows;
    public int code;
    public String msg;

    public static class RowsDTO {
        public Object searchValue;
        public Object createBy;
        public Object createTime;
        public Object updateBy;
        public Object updateTime;
        public Object remark;
        public ParamsDTO params;
        public int id;
        public int lotId;
        public String entryTime;
        public String outTime;
        public String plateNumber;
        public String monetary;
        public String parkName;
        public String parkNo;
        public String address;

        public static class ParamsDTO {
        }
    }
}
