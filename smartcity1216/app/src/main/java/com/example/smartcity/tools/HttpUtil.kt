package com.example.smartcity.tools

import com.google.gson.Gson
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.toRequestBody
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object HttpUtil {
    private const val baseUrl1 = "http://10.10.230.112:10001"
    private const val baseUrl2 = "http://124.93.196.45:10001"
    const val BASEURL = baseUrl2
    private val MEDIA_TYPE = "application/json".toMediaTypeOrNull()
    private val MEDIA_FILE = "multipart/form-data".toMediaTypeOrNull()

    private val retrofit: Retrofit = Retrofit.Builder()
        .baseUrl(BASEURL)
        .addConverterFactory(GsonConverterFactory.create())
        .build()
    val service: FitService = retrofit.create(FitService::class.java)

    private val gson = Gson()

    fun postTo(
        url: String,
        token: String = "",
        data: Any
    ) = service.post(url, token, gson.toJson(data).toRequestBody(MEDIA_TYPE))

    fun putTo(
        url: String,
        token: String = "",
        data: Any
    ) = service.put(url, token, gson.toJson(data).toRequestBody(MEDIA_TYPE))

    fun uploadTo(
        token: String,
        bytes: ByteArray,
    ) = service.upload(
        token, MultipartBody.Builder().setType(MultipartBody.FORM)
            .addFormDataPart("file", "file.jpeg", bytes.toRequestBody(MEDIA_FILE))
            .build().parts
    )

    fun <T> jsonToModel(data: Any, clazz: Class<T>): T = gson.fromJson(gson.toJson(data), clazz)
}
