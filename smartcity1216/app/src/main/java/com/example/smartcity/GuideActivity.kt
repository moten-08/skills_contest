package com.example.smartcity

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import com.example.smartcity.tools.*

class GuideActivity : AppCompatActivity() {
    private val index = MutableLiveData<Int>().apply {
        value = 0
    }
    private val sp by lazy { getSharedPreferences("data", MODE_PRIVATE) }

    private val vpG: ViewPager by lazy { findViewById<ViewPager>(R.id.vpG) }
    private val rvG: RecyclerView by lazy { findViewById<RecyclerView>(R.id.rvG) }
    private val goMain: Button by lazy { findViewById<Button>(R.id.goMain) }
    private val settingInternet: TextView by lazy { findViewById<TextView>(R.id.settingInternet) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_guide)

        val guides = listOf(
            R.mipmap.guide1,
            R.mipmap.guide2,
            R.mipmap.guide3,
            R.mipmap.guide4,
            R.mipmap.guide5,
        )
        val viewList = ArrayList<View>()
        repeat(guides.size) {
            val view = View.inflate(this, R.layout.item_img, null)
            view.findViewById<ImageView>(R.id.itemG).glide(guides[it], true)
            viewList.add(view)
        }

        vpG.apply {
            adapter = pagerAdapter(viewList)
            addOnPageChangeListener(onPageChangeListener())
        }
        rvG.layoutManager = LinearLayoutManager(this@GuideActivity, RecyclerView.HORIZONTAL, false)

        index.observe(this) {
            rvG.adapter = RxAdapter(this@GuideActivity, R.layout.item_pot, guides) { r, p ->
                r.findViewById<ImageView>(R.id.itemP).setImageResource(
                    if (p == it) R.drawable.ic_baseline_fiber_manual_record_24
                    else R.drawable.ic_baseline_fiber_manual_record
                )
                r.setOnClickListener {
                    index.value = p
                }
            }
            goMain.visibility = if (it == (guides.size - 1)) View.VISIBLE else View.GONE
            settingInternet.visibility = if (it == (guides.size - 1)) View.VISIBLE else View.GONE
            vpG.currentItem = it
        }

        val dialogView = View.inflate(this, R.layout.dialog, null)
        val dialog = AlertDialog.Builder(this)
            .setTitle("网络设置")
            .setView(dialogView)
            .create()
        var ip = sp.getString("ip", "") ?: ""
        var port = sp.getString("port", "") ?: ""
        dialogView.apply {
            val ipD = findViewById<EditText>(R.id.ipD)
            val portD = findViewById<EditText>(R.id.portD)
            val saveD = findViewById<Button>(R.id.saveD)
            ipD.setText(ip)
            portD.setText(port)
            saveD.setOnClickListener {
                val ipStr = ipD.text.toString()
                val portStr = portD.text.toString()
                sp.open {
                    putString("ip", ipStr)
                    putString("port", portStr)
                }
                dialog.dismiss()
            }
        }
        settingInternet.setOnClickListener { dialog.show() }
        goMain.setOnClickListener {
            ip = sp.getString("ip", "") ?: ""
            port = sp.getString("port", "") ?: ""
            if (ip.trim().isEmpty() or port.trim().isEmpty()) {
                "未设置IP地址或端口号".snack(vpG, "去设置") {
                    dialog.show()
                }
                return@setOnClickListener
            }
            sp.open {
                putBoolean("isFirst", false)
            }
            finish()
            startActivity(Intent(this, MainActivity::class.java))
        }
    }

    override fun onStart() {
        super.onStart()
        if (!sp.getBoolean("isFirst", true)) {
            finish()
            startActivity(Intent(this, MainActivity::class.java))
        }
    }

    private fun onPageChangeListener() = object : ViewPager.OnPageChangeListener {
        override fun onPageScrolled(
            position: Int,
            positionOffset: Float,
            positionOffsetPixels: Int
        ) = Unit

        override fun onPageSelected(position: Int) {
            index.value = position
        }

        override fun onPageScrollStateChanged(state: Int) = Unit

    }
}
