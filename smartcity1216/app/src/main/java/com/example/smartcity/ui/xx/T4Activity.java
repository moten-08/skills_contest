package com.example.smartcity.ui.xx;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.smartcity.R;
import com.example.smartcity.tools.BaseActivityX;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class T4Activity extends BaseActivityX {
    List<XCard> cardList = new ArrayList<>();
    int[] image = {R.mipmap.t0_2, R.mipmap.t0_3, R.mipmap.t0_1};
    String[] type = {"回收垃圾", "不可回收垃圾", "其他垃圾"};
    String[] name = {"A1", "A2", "A3", "A4"};
    String[] price = {"19.9/KG", "9.9/KG", "29.9/KG", "1.99/KG"};
    private RecyclerView T4Rec;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        Random random = new Random();
        for (int i = 0; i < 12; i++) {
            int j = random.nextInt(3);
            XCard c = new XCard();
            c.setF_Image(image[j]);
            c.setF_Text(type[j]);
            j = random.nextInt(name.length);
            c.setS_Text(name[j]);
            j = random.nextInt(price.length);
            c.setT_Text(price[j]);
            cardList.add(c);
        }
        setTitle("附近回收站");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_t4);

        T4Rec = (RecyclerView) findViewById(R.id.T4_Rec);

        T4Rec.setLayoutManager(new LinearLayoutManager(this));
        T4Rec.setAdapter(new XAdapter(this, R.layout.t2_layout, cardList) {
            @Override
            public void init(View view, int position) {
                XCard c = cardList.get(position);
                ImageView T4Image = (ImageView) view.findViewById(R.id.T4_image);
                TextView T4Type = (TextView) view.findViewById(R.id.T4_type);
                TextView T2FT = (TextView) view.findViewById(R.id.T2_F_T);
                TextView T2ST = (TextView) view.findViewById(R.id.T2_S_T);
                Glide.with(view).load(c.getF_Image()).centerCrop().into(T4Image);
                T4Type.setText(c.getF_Text());
                T2FT.setText("回收站："+c.getS_Text());
                T2ST.setText("回收价格"+c.getT_Text());
            }
        });
    }
}