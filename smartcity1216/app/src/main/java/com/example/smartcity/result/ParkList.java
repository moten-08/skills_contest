package com.example.smartcity.result;

import java.util.List;

public class ParkList {

    public int total;
    public List<RowsDTO> rows;
    public int code;
    public String msg;

    public static class RowsDTO {
        public Object searchValue;
        public Object createBy;
        public Object createTime;
        public Object updateBy;
        public Object updateTime;
        public Object remark;
        public ParamsDTO params;
        public int id;
        public String parkName;
        public String vacancy;
        public String priceCaps;
        public String imgUrl;
        public String rates;
        public String address;
        public String distance;
        public String allPark;
        public String open;

        public static class ParamsDTO {
        }
    }
}
