package com.example.smartcity.result;

public class LoginResult {

    public String msg;
    public int code;
    public String token;
}
