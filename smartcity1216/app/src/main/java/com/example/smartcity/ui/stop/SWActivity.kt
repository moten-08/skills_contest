package com.example.smartcity.ui.stop

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.smartcity.R
import com.example.smartcity.result.ParkList
import com.example.smartcity.tools.BaseActivity
import com.example.smartcity.tools.HttpUtil
import com.example.smartcity.tools.MyCallback
import com.example.smartcity.tools.RxAdapter
import java.util.*
import kotlin.Comparator
import kotlin.collections.ArrayList

class SWActivity : BaseActivity() {

    private val titleSW: TextView by lazy { findViewById<TextView>(R.id.titleSW) }
    private val seeHistory: TextView by lazy { findViewById<TextView>(R.id.seeHistory) }
    private val nearPark: RecyclerView by lazy { findViewById<RecyclerView>(R.id.nearPark) }
    private val showMore: Button by lazy { findViewById<Button>(R.id.showMore) }

    private val parkLive = MutableLiveData<ParkList>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sw)

        title = "停哪儿"

        nearPark.layoutManager = LinearLayoutManager(this)

        getNearList()
        val list = ArrayList<ParkList.RowsDTO>()
        parkLive.observe(this) {
            list.addAll(it.rows)
            isNear(list)
            val list2 = list.subList(0,6)
            nearPark.adapter = RxAdapter(this, R.layout.item_park, list2) { r, p ->
                r.findViewById<TextView>(R.id.parkName).text = list2[p].parkName
                r.findViewById<TextView>(R.id.vacancy).text = "空位数量:${list2[p].vacancy}"
                r.findViewById<TextView>(R.id.address).text = "地址:${list2[p].address}"
                r.findViewById<TextView>(R.id.distance).text = "距离:${list2[p].distance}km"
                r.findViewById<TextView>(R.id.rates).text = "收费费率:${list2[p].rates}元/小时"
                r.setOnClickListener {
                    Intent(this, ParkInfoActivity::class.java).apply {
                        putExtra("parkId", list[p].id)
                        startActivity(this)
                    }
                }
            }
            showMore.setOnClickListener { view ->
                list2.clear()
                list2.addAll(list)
                nearPark.adapter?.notifyItemChanged(0,list2.size)
                view.visibility = View.GONE
            }

        }
        seeHistory.setOnClickListener {
            startActivity(Intent(this, HistoryActivity::class.java))
        }
    }

    private fun isNear(list: ArrayList<ParkList.RowsDTO>) {
        // 距离排序
        Collections.sort(list, Comparator.comparing(ParkList.RowsDTO::distance))
    }

    private fun getNearList() {
        HttpUtil.service.get(
            "api/park/lot/list"
        ).enqueue(MyCallback {
            parkLive.value = HttpUtil.jsonToModel(it, ParkList::class.java)
        })
    }
}