package com.example.smartcity.ui.news

import android.os.Bundle
import android.webkit.WebView
import android.widget.ImageView
import androidx.lifecycle.MutableLiveData
import com.example.smartcity.R
import com.example.smartcity.result.NewsInfo
import com.example.smartcity.tools.BaseActivity
import com.example.smartcity.tools.HttpUtil
import com.example.smartcity.tools.MyCallback
import com.example.smartcity.tools.glide

class NewsInfoActivity : BaseActivity() {

    private val newsInfo = MutableLiveData<NewsInfo>()
    private val cover by lazy { findViewById<ImageView>(R.id.cover) }
    private val web by lazy { findViewById<WebView>(R.id.web) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_news_info)

        getNewsInfo(intent.getIntExtra("newsId", -1))
        newsInfo.observe(this) {
            title = it.data.title
            cover.glide(it.data.cover)
            val html = it.data.content
                .replace("/prod-api", "${HttpUtil.BASEURL}/prod-api")
                .replace("<img ", "<img style = \"max-width:100%;height:auto;\"")
            web.loadDataWithBaseURL(HttpUtil.BASEURL, html, "text/html", "utf-8", null)
        }

    }

    private fun getNewsInfo(id: Int) {
        HttpUtil.service.get("press/press/$id")
            .enqueue(MyCallback {
                newsInfo.value = HttpUtil.jsonToModel(it, NewsInfo::class.java)
            })
    }
}
