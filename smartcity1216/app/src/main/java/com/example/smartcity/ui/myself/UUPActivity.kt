package com.example.smartcity.ui.myself

import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import com.example.smartcity.R
import com.example.smartcity.request.UpPass
import com.example.smartcity.result.BaseResult
import com.example.smartcity.tools.BaseActivity
import com.example.smartcity.tools.HttpUtil
import com.example.smartcity.tools.MyCallback
import com.example.smartcity.tools.toast

class UUPActivity : BaseActivity() {

    private val ordPass: EditText by lazy { findViewById(R.id.ordPass) }
    private val newsPass: EditText by lazy { findViewById(R.id.newsPass) }
    private val newsPass2: EditText by lazy { findViewById(R.id.newsPass2) }
    private val upp: Button by lazy { findViewById(R.id.upp) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_uup)

        title = "修改密码"

        upp.setOnClickListener {
            val ops = ordPass.text.toString()
            val np1 = newsPass.text.toString()
            val np2 = newsPass2.text.toString()
            if (np1 != np2) {
                "两次输入的密码不一致".toast(this)
                return@setOnClickListener
            }
            upPass(UpPass(np2, ops))
        }
    }

    private fun upPass(upPass: UpPass) {
        HttpUtil.putTo(
            "api/common/user/resetPwd", token, upPass
        ).enqueue(MyCallback {
            HttpUtil.jsonToModel(it, BaseResult::class.java).apply {
                msg.toast(this@UUPActivity)
                if (code == 200) {
                    finish()
                }
            }
        })
    }
}
