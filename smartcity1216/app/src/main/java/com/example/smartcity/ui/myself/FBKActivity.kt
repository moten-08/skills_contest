package com.example.smartcity.ui.myself

import android.graphics.Color
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.MutableLiveData
import com.example.smartcity.R
import com.example.smartcity.request.Feedback
import com.example.smartcity.result.BaseResult
import com.example.smartcity.tools.*

class FBKActivity : BaseActivity() {

    private val edIndex = MutableLiveData<Int>().apply {
        value = 0
    }

    private val edTitle: EditText by lazy { findViewById<EditText>(R.id.title) }
    private val feedbackEd: EditText by lazy { findViewById<EditText>(R.id.feedbackEd) }
    private val edMax: TextView by lazy { findViewById<TextView>(R.id.edMax) }
    private val edNow: TextView by lazy { findViewById<TextView>(R.id.edNow) }
    private val submit: Button by lazy { findViewById<Button>(R.id.submit) }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fbk)

        title = "意见反馈"

        edIndex.observe(this) {
            edNow.text = it.toString()
            edNow.setTextColor(if (it > 150) Color.RED else Color.BLACK)
            submit.setOnClickListener { _ ->
                val titleStr = edTitle.toText()
                val feedback = feedbackEd.toText()
                if (titleStr.trim().isEmpty()) {
                    "标题不能为空".toast(this)
                    return@setOnClickListener
                }
                if (feedback.trim().isEmpty()) {
                    "内容不能为空".toast(this)
                    return@setOnClickListener
                }
                if (it > 150) {
                    "字数超出限制".toast(this)
                    return@setOnClickListener
                }
                common(Feedback(titleStr, feedback))
            }
        }
        feedbackEd.addTextChangedListener {
            edIndex.value = it.toString().length
        }
    }

    private fun common(feedback: Feedback) {
        HttpUtil.postTo(
            "api/common/feedback", token, feedback
        ).enqueue(MyCallback {
            val data = HttpUtil.jsonToModel(it, BaseResult::class.java)
            data.msg.toast(this)
            if (data.code == 200) finish()
        })
    }
}
