package com.example.smartcity.ui.stop

import android.os.Bundle
import android.widget.TextView
import androidx.lifecycle.MutableLiveData
import com.example.smartcity.R
import com.example.smartcity.result.ParkInfo
import com.example.smartcity.tools.BaseActivity
import com.example.smartcity.tools.HttpUtil
import com.example.smartcity.tools.MyCallback

class ParkInfoActivity : BaseActivity() {
    private val parkInfo = MutableLiveData<ParkInfo>()

    private val parkName: TextView by lazy { findViewById<TextView>(R.id.parkName) }
    private val vacancy: TextView by lazy { findViewById<TextView>(R.id.vacancy) }
    private val address: TextView by lazy { findViewById<TextView>(R.id.address) }
    private val idOpen: TextView by lazy { findViewById<TextView>(R.id.idOpen) }
    private val distance: TextView by lazy { findViewById<TextView>(R.id.distance) }
    private val rates: TextView by lazy { findViewById<TextView>(R.id.rates) }
    private val text: TextView by lazy { findViewById<TextView>(R.id.text) }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_park_info)

        val id = intent.getIntExtra("parkId", 0)
        title = "停车场详情"

        getParkInfo(id)
        parkInfo.observe(this) {
            parkName.text = it.data.parkName
            vacancy.text = "空位：${it.data.vacancy}"
            address.text = it.data.address
            idOpen.text = if (it.data.open == "Y") "对外开放" else "不对外开放"
            distance.text = "距离:${it.data.distance}km"
            rates.text = "${it.data.rates}元/小时"
            text.text = "收费参考：\n${rates.text}\n最高${it.data.priceCaps}元/天"
        }
    }

    private fun getParkInfo(id: Int) {
        HttpUtil.service.get(
            "api/park/lot/$id"
        ).enqueue(MyCallback {
            parkInfo.value = HttpUtil.jsonToModel(it, ParkInfo::class.java)
        })
    }
}