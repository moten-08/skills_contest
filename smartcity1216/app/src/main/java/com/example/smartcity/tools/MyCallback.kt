package com.example.smartcity.tools

import android.util.Log
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MyCallback(
    val block: (body: Any) -> Unit
) : Callback<Any> {
    override fun onResponse(p0: Call<Any>, p1: Response<Any>) {
        Log.e("Thread", Thread.currentThread().name)
        val body = p1.body()
        if (body == null) Log.e("TAG", "body is null")
        else block(body)
    }

    override fun onFailure(p0: Call<Any>, p1: Throwable) = p1.printStackTrace()
}
