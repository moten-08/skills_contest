package com.example.smartcity.result;

import java.util.List;

public class CategoryList {

    public String msg;
    public int code;
    public List<DataDTO> data;

    public static class DataDTO {
        public Object searchValue;
        public Object createBy;
        public Object createTime;
        public Object updateBy;
        public Object updateTime;
        public Object remark;
        public ParamsDTO params;
        public int id;
        public String appType;
        public String name;
        public int sort;
        public String status;
        public Object parentId;

        public static class ParamsDTO {
        }
    }
}
