package com.example.smartcity.ui.bus

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.smartcity.R
import com.example.smartcity.result.BusList
import com.example.smartcity.tools.BaseActivity
import com.example.smartcity.tools.HttpUtil
import com.example.smartcity.tools.MyCallback
import com.example.smartcity.tools.RxAdapter

class SBActivity : BaseActivity() {

    private val listR: RecyclerView by lazy { findViewById<RecyclerView>(R.id.listR) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.item_list)
        title = "智慧巴士"

        listR.layoutManager = LinearLayoutManager(this)
        getBusList()
    }

    private fun getBusList() {
        HttpUtil.service.get("api/bus/line/list")
            .enqueue(MyCallback {
                HttpUtil.jsonToModel(it, BusList::class.java).apply {
                    listR.adapter = RxAdapter(this@SBActivity, R.layout.item_bus, rows) { r, p ->

                    }
                }
            })
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_bus, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.busHistory) startActivity(Intent(this, BHActivity::class.java))
        return super.onOptionsItemSelected(item)
    }
}
