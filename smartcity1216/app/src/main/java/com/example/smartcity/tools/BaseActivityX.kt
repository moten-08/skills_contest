package com.example.smartcity.tools

import android.os.Bundle
import com.example.smartcity.R

open class BaseActivityX : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setTheme(R.style.AppTheme_xx)
    }
}