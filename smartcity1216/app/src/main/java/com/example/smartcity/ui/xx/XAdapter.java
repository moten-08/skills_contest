package com.example.smartcity.ui.xx;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public abstract class XAdapter extends RecyclerView.Adapter<XAdapter.ViewHolder> {
    private Context context;
    private int lay;
    private List list;

    public XAdapter(Context context, int lay, List list) {
        this.context = context;
        this.lay = lay;
        this.list = list;
    }

    @NonNull
    @Override
    public XAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(lay,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull XAdapter.ViewHolder holder, int position) {
        init(holder.itemView,position);
    }

    public abstract void init(View view, int position);

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}
