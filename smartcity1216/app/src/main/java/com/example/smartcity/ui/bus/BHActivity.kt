package com.example.smartcity.ui.bus

import android.os.Bundle
import androidx.recyclerview.widget.RecyclerView
import com.example.smartcity.R
import com.example.smartcity.tools.BaseActivity

class BHActivity : BaseActivity() {

    private val listR: RecyclerView by lazy { findViewById(R.id.listR) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.item_list)
    }
}
