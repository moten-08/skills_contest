package com.example.smartcity.ui.xx;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.smartcity.R;
import com.youth.banner.Banner;
import com.youth.banner.loader.ImageLoader;

import java.util.Arrays;
import java.util.List;

public class XxFragment extends Fragment {

    private Banner MBanner;
    private LinearLayout tool1;
    private LinearLayout tool2;
    private LinearLayout tool3;
    private LinearLayout tool4;

    List<Integer> bannerList = Arrays.asList(R.mipmap.banner1,R.mipmap.banner2,R.mipmap.banner3);

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_xx, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        MBanner = (Banner) view.findViewById(R.id.M_banner);
        tool1 = (LinearLayout) view.findViewById(R.id.tool1);
        tool2 = (LinearLayout) view.findViewById(R.id.tool2);
        tool3 = (LinearLayout) view.findViewById(R.id.tool3);
        tool4 = (LinearLayout) view.findViewById(R.id.tool4);

        tool1.setOnClickListener(view1 -> startActivity(new Intent(getContext(),T1Activity.class)));
        tool2.setOnClickListener(view1 -> startActivity(new Intent(getContext(),T2Activity.class)));
        tool3.setOnClickListener(view1 -> startActivity(new Intent(getContext(),T3Activity.class)));
        tool4.setOnClickListener(view1 -> startActivity(new Intent(getContext(),T4Activity.class)));

        MBanner.setImages(bannerList).setImageLoader(new ImageLoader() {
            @Override
            public void displayImage(Context context, Object o, ImageView imageView) {
                Glide.with(context).load(o).centerCrop().into(imageView);
            }
        }).start();
    }
}