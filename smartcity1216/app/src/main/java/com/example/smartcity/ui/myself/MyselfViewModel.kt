package com.example.smartcity.ui.myself

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.smartcity.result.UserInfo
import com.example.smartcity.tools.HttpUtil
import com.example.smartcity.tools.MyCallback

class MyselfViewModel : ViewModel() {

    val userInfoLive = MutableLiveData<UserInfo>()
    fun getUserInfo(token: String) {
        HttpUtil.service.get(
            "api/common/user/getInfo",
            token
        ).enqueue(MyCallback {
            userInfoLive.value = HttpUtil.jsonToModel(it, UserInfo::class.java)
        })
    }
}