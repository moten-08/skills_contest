package com.example.smartcity.result;

import java.io.Serializable;

public class UserInfo implements Serializable {

    public String msg;
    public int code;
    public UserDTO user;

    public static class UserDTO implements Serializable {
        public int userId;
        public String userName;
        public String nickName;
        public String email;
        public String phonenumber;
        public String sex;
        public String avatar;
        public String idCard;
        public float balance;
        public float score;
    }
}
