package com.example.smartcity.ui.stop

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.smartcity.R
import com.example.smartcity.result.StopInfo
import com.example.smartcity.tools.*
import java.util.Calendar.HOUR_OF_DAY
import java.util.Calendar.getInstance

class HistoryActivity : BaseActivity() {

    private val entryTimeD: TextView by lazy { findViewById(R.id.entryTimeD) }
    private val entryTimeT: TextView by lazy { findViewById(R.id.entryTimeT) }
    private val outTimeD: TextView by lazy { findViewById(R.id.outTimeD) }
    private val outTimeT: TextView by lazy { findViewById(R.id.outTimeT) }
    private val record: Button by lazy { findViewById(R.id.record) }
    private val showMore: Button by lazy { findViewById(R.id.showMore) }
    private val rvHistory: RecyclerView by lazy { findViewById(R.id.rvHistory) }

    private var minOutTime = 0L
    private var maxEntryTime = getInstance().timeInMillis

    private val stopInfo = MutableLiveData<StopInfo>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_history)

        title = "历史停车记录"
        entryTimeD.setOnClickListener { dateDialog(0L, maxEntryTime, entryTimeD) }
        outTimeD.setOnClickListener { dateDialog(minOutTime, getInstance().timeInMillis, outTimeD) }
        entryTimeT.setOnClickListener { timeDialog(entryTimeT) }
        outTimeT.setOnClickListener { timeDialog(outTimeT) }
        rvHistory.layoutManager = LinearLayoutManager(this)

        getStopTimes("", "")

        val list = ArrayList<StopInfo.RowsDTO>()
        stopInfo.observe(this) {
            showMore.visibility = if (it.rows.size > 6) View.VISIBLE else View.GONE
            list.clear()
            list.addAll(it.rows)
            val list2 = list.subList(0, if (list.size > 6) 6 else list.size)
            rvHistory.adapter = RxAdapter(this, R.layout.item_history, list2) { r, p ->
                r.findViewById<TextView>(R.id.plateNumber).text = list2[p].plateNumber
                r.findViewById<TextView>(R.id.monetary).text = "收费:${list2[p].monetary}元"
                r.findViewById<TextView>(R.id.entryTime).text = "入场时间:${list2[p].entryTime}"
                r.findViewById<TextView>(R.id.outTime).text = "出场时间:${list2[p].outTime}"
            }
            showMore.setOnClickListener { it2 ->
                it2.visibility = View.GONE
                list2.clear()
                list2.addAll(it.rows)
                rvHistory.adapter?.notifyItemChanged(0, list2.size)
            }
        }
        record.setOnClickListener {
            val entryTime = entryTimeD.toText() + entryTimeT.toText()
            val outTime = outTimeD.toText() + outTimeT.toText()
            getStopTimes(entryTime, outTime)
        }
    }

    private fun getStopTimes(entryTime: String, outTime: String) {
        HttpUtil.service.get(
            "api/park/lot/record/list",
            map = mapOf("entryTime" to entryTime, "outTime" to outTime)
        ).enqueue(MyCallback {
            stopInfo.value = HttpUtil.jsonToModel(it, StopInfo::class.java)
            "共${stopInfo.value?.total}条".toast(this)
        })
    }

    private fun dateDialog(minTime: Long, maxTime: Long, textView: TextView) {
        val now = getInstance()
        val dialog = DatePickerDialog(this, if (now.get(HOUR_OF_DAY) > 19) 2 else 3)
        dialog.setOnDateSetListener { _, year, month, dayOfMonth ->
            val month2 = (month + 1).forSize()
            val day2 = dayOfMonth.forSize()
            textView.text = String.format("%s-%s-%s ", year, month2, day2)
            val c = getInstance()
            c.set(year, month, dayOfMonth)
            if (textView == entryTimeD) {
                minOutTime = c.timeInMillis
            } else {
                maxEntryTime = c.timeInMillis
            }
        }
        dialog.datePicker.minDate = minTime
        dialog.datePicker.maxDate = maxTime
        dialog.show()
    }

    private fun timeDialog(textView: TextView) {
        val now = getInstance()
        TimePickerDialog(
            this, if (now.get(HOUR_OF_DAY) > 19) 2 else 3,
            { _, hourOfDay, minute ->
                val hour = hourOfDay.forSize()
                val min = minute.forSize()
                textView.text = String.format("%s:%s", hour, min)
            },
            0, 0, true
        ).show()
    }
}
