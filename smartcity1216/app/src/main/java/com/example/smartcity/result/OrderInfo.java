package com.example.smartcity.result;

import java.util.List;

public class OrderInfo {

    public String msg;
    public int code;
    public DataDTO data;

    public static class DataDTO {
        public SellerInfoDTO sellerInfo;
        public OrderInfoDTO orderInfo;

        public static class SellerInfoDTO {
            public Object searchValue;
            public Object createBy;
            public String createTime;
            public Object updateBy;
            public String updateTime;
            public Object remark;
            public ParamsDTO params;
            public int id;
            public String name;
            public String address;
            public String introduction;
            public int themeId;
            public double score;
            public int saleQuantity;
            public int deliveryTime;
            public String imgUrl;
            public double avgCost;
            public Object other;
            public String recommend;
            public double distance;
            public int saleNum3hour;

            public static class ParamsDTO {
            }
        }

        public static class OrderInfoDTO {
            public Object searchValue;
            public Object createBy;
            public String createTime;
            public Object updateBy;
            public Object updateTime;
            public Object remark;
            public ParamsDTO params;
            public int id;
            public String orderNo;
            public int userId;
            public int sellerId;
            public double amount;
            public Object postage;
            public String status;
            public Object paymentType;
            public Object payTime;
            public Object sendTime;
            public String receiverName;
            public String receiverPhone;
            public String receiverAddress;
            public String receiverLabel;
            public Object houseNumber;
            public List<OrderItemListDTO> orderItemList;

            public static class ParamsDTO {
            }

            public static class OrderItemListDTO {
                public Object searchValue;
                public Object createBy;
                public String createTime;
                public Object updateBy;
                public Object updateTime;
                public Object remark;
                public ParamsDTO params;
                public int id;
                public int userId;
                public String orderNo;
                public int productId;
                public String productName;
                public String productImage;
                public double productPrice;
                public int quantity;
                public double totalPrice;

                public static class ParamsDTO {
                }
            }
        }
    }
}
