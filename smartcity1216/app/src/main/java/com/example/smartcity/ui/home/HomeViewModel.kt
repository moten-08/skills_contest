package com.example.smartcity.ui.home

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.smartcity.result.BannerHome
import com.example.smartcity.result.NewsList
import com.example.smartcity.result.ServiceHome
import com.example.smartcity.tools.HttpUtil
import com.example.smartcity.tools.MyCallback

class HomeViewModel : ViewModel() {

    val bannerLive = MutableLiveData<BannerHome>()
    fun getBanner() {
        HttpUtil.service.get(
            "api/rotation/list",
            map = mapOf("type" to "2")
        ).enqueue(MyCallback {
            bannerLive.value = HttpUtil.jsonToModel(it, BannerHome::class.java)
        })
    }

    val servicesLive = MutableLiveData<ServiceHome>()
    fun getServices(searchStr:String = "",type:String="") {
        HttpUtil.service.get(
            "api/service/list",map = mapOf("serviceName" to searchStr,"serviceType" to type)
        ).enqueue(MyCallback {
            servicesLive.value = HttpUtil.jsonToModel(it, ServiceHome::class.java)
        })
    }

    val newsList = MutableLiveData<NewsList>()
    fun getHotNews() {
        HttpUtil.service.get(
            "press/press/list",
            map = mapOf("hot" to "Y")
        ).enqueue(MyCallback {
            newsList.value = HttpUtil.jsonToModel(it, NewsList::class.java)
        })
    }
}