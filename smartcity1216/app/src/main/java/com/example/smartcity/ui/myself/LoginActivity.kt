package com.example.smartcity.ui.myself

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import com.example.smartcity.R
import com.example.smartcity.request.LoginRequest
import com.example.smartcity.result.LoginResult
import com.example.smartcity.tools.*

class LoginActivity : BaseActivity() {

    private val sp by lazy { getSharedPreferences("data", MODE_PRIVATE) }
    private lateinit var loading: ProgressBar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        title = "用户登录"

        setContentView(R.layout.activity_login)

        val username = findViewById<EditText>(R.id.username)
        val password = findViewById<EditText>(R.id.password)
        val login = findViewById<Button>(R.id.login)
        loading = findViewById(R.id.loading)

        login.setOnClickListener {
            val userStr = username.text.toString()
            val passStr = password.text.toString()
            loading.visibility = View.VISIBLE
            login(LoginRequest(userStr, passStr))
        }

    }

    private fun login(loginRequest: LoginRequest) {
        HttpUtil.postTo(
            "api/login",
            data = loginRequest
        ).enqueue(MyCallback {
            HttpUtil.jsonToModel(it, LoginResult::class.java).apply {
                msg.toast(this@LoginActivity)
                if (code == 200) {
                    finish()
                    sp.open {
                        putString("token", token)
                    }
                }
                loading.visibility = View.GONE
            }
        })
    }
}
