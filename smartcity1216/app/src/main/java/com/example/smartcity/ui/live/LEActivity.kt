package com.example.smartcity.ui.live

import android.os.Bundle
import com.example.smartcity.R
import com.example.smartcity.tools.BaseActivity

class LEActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_le)

        title = "生活缴费"
    }
}
