package com.example.smartcity.result;

import java.util.List;

public class BannerHome {

    public int total;
    public List<RowsDTO> rows;
    public int code;
    public String msg;

    public static class RowsDTO {
        public Object searchValue;
        public String createBy;
        public String createTime;
        public String updateBy;
        public String updateTime;
        public Object remark;
        public ParamsDTO params;
        public int id;
        public String appType;
        public String status;
        public int sort;
        public String advTitle;
        public String advImg;
        public String servModule;
        public int targetId;
        public String type;

        public static class ParamsDTO {
        }
    }
}
