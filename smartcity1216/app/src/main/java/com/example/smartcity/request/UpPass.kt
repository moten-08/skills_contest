package com.example.smartcity.request

data class UpPass(var newPassword: String, var oldPassword: String)