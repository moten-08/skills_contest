package com.example.smartcity.ui.myself

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.example.smartcity.MainActivity
import com.example.smartcity.R
import com.example.smartcity.tools.HttpUtil
import com.example.smartcity.tools.glide
import com.example.smartcity.tools.open
import com.example.smartcity.tools.snack


class MyselfFragment : Fragment() {

    private lateinit var viewModel: MyselfViewModel

    private lateinit var userAvatar: ImageView
    private lateinit var nickName: TextView
    private lateinit var people: TextView
    private lateinit var myOrders: TextView
    private lateinit var upPassword: TextView
    private lateinit var feedback: TextView
    private lateinit var logout: TextView

    private lateinit var sp: SharedPreferences
    private lateinit var token: String

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        viewModel = ViewModelProviders.of(this).get(MyselfViewModel::class.java)
        sp = requireContext().getSharedPreferences("data", Context.MODE_PRIVATE)
        return inflater.inflate(R.layout.fragment_myself, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initView(view)
        viewModel.userInfoLive.observe(viewLifecycleOwner) {
            if (it.code == 200) {
                Log.d("余额", "${it.user.balance}")
                val avatar =
                    if ("/prod-api" in it.user.avatar) it.user.avatar else "/prod-api${it.user.avatar}"
                userAvatar.glide(avatar,circle = true)
                nickName.text = "用户名:${it.user.nickName}"
            } else if (token.trim().isNotEmpty()) {
                "登录失效".snack(logout, "去登录") {
                    startActivity(Intent(requireContext(), LoginActivity::class.java))
                }
            }
            logout.setOnClickListener { onClick(null) }
            people.setOnClickListener { _ ->
                if (verifyToken()) return@setOnClickListener
                Intent(requireContext(), UUIActivity::class.java).apply {
                    putExtra("userInfo", it.user)
                    startActivity(this)
                }
            }
            myOrders.setOnClickListener { onClick(MOLActivity::class.java) }
            upPassword.setOnClickListener { onClick(UUPActivity::class.java) }
            feedback.setOnClickListener { onClick(FBKActivity::class.java) }
        }
    }


    override fun onResume() {
        super.onResume()
        token = sp.getString("token", "") ?: ""
        verifyToken()
        viewModel.getUserInfo(token)

    }

    private fun initView(view: View) {
        userAvatar = view.findViewById(R.id.Tou_Xiang) as ImageView
        nickName = view.findViewById(R.id.Yong_Hu_Ming) as TextView
        people = view.findViewById(R.id.Ge_Ren_Se_Zi) as TextView
        myOrders = view.findViewById(R.id.Wo_De_Ding_Dan) as TextView
        upPassword = view.findViewById(R.id.Xiu_Gai_Mi_Ma) as TextView
        feedback = view.findViewById(R.id.Yi_Jian_Fan_Kui) as TextView
        logout = view.findViewById(R.id.logout) as TextView
    }

    private fun onClick(clazz: Class<*>?) {
        if (verifyToken()) return
        if (clazz == null) {
            sp.open {
                remove("token")
                (requireActivity() as MainActivity).switch(R.id.navigation_myself)
            }
            return
        }
        startActivity(Intent(requireContext(), clazz))

    }

    private fun verifyToken(): Boolean {
        if (token.trim().isEmpty()) {
            "未登录".snack(logout, "去登录") {
                startActivity(Intent(requireContext(), LoginActivity::class.java))
            }
            return true
        }
        return false
    }
}
