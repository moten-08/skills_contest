package com.example.smartcity.result;

import java.util.List;

public class BusList {

    public int total;
    public List<RowsDTO> rows;
    public int code;
    public String msg;

    public static class RowsDTO {
        public Object searchValue;
        public Object createBy;
        public String createTime;
        public Object updateBy;
        public String updateTime;
        public Object remark;
        public ParamsDTO params;
        public int id;
        public String name;
        public String first;
        public String end;
        public String startTime;
        public String endTime;
        public double price;
        public String mileage;

        public static class ParamsDTO {
        }
    }
}
