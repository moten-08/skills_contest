package com.example.smartcity.ui.xx;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.example.smartcity.R;
import com.example.smartcity.tools.BaseActivityX;
import com.example.smartcity.tools.RxAdapter;
import kotlin.jvm.functions.Function2;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class T2Activity extends BaseActivityX {
    List<XCard> cardList = new ArrayList<>();
    XCard[] cards = {new XCard("地点：珠海市金湾区红旗镇XXXX", "时间：2021.12.16\t\t16:00"),
            new XCard("地点：珠海市金湾区广科干学院", "时间：2021.12.16\t\t19:00")};

    public static void RANDOMLIST(List list, XCard[] cards, int size) {
        Random random = new Random();
        for (int i = 0; i < size; i++) {
            int j = random.nextInt(cards.length);
            list.add(cards[j]);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTitle("预约历史");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_t2);
        RANDOMLIST(cardList, cards, 7);
        RecyclerView t2Rec = findViewById(R.id.T2_Rec);

        t2Rec.setLayoutManager(new LinearLayoutManager(this));
        RxAdapter rxAdapter = new RxAdapter(this, R.layout.t2_layout, cardList, getFunction2());
        t2Rec.setAdapter(rxAdapter);

    }

    @NotNull
    private Function2 getFunction2() {
        return (view, position) -> {
            XCard c = cardList.get((int) position);
            TextView T2FT = ((View) view).findViewById(R.id.T2_F_T);
            TextView T2ST = ((View) view).findViewById(R.id.T2_S_T);
            T2FT.setText(c.getF_Text());
            T2ST.setText(c.getS_Text());
            return null;
        };
    }

}