package com.example.smartcity.ui.xx;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.smartcity.R;
import com.example.smartcity.tools.BaseActivityX;

public class T1Activity extends BaseActivityX {
    private TextView textView4;
    private TextView textView;
    private TextView textView8;
    private TextView T3TabButton;
    private TextView T3TabButton2;
    private TextView T3TabButton3;
    private TextView T3TabButton4;
    private TextView T3TabButton5;
    private TextView T3TabButton6;
    private TextView textView7;
    private EditText editTextTextPersonName;
    private TextView T3ButtonC;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_t1);

        textView4 = (TextView) findViewById(R.id.textView4);
        textView = (TextView) findViewById(R.id.textView);
        textView8 = (TextView) findViewById(R.id.textView8);
        T3TabButton = (TextView) findViewById(R.id.T3_Tab_Button);
        T3TabButton2 = (TextView) findViewById(R.id.T3_Tab_Button2);
        T3TabButton3 = (TextView) findViewById(R.id.T3_Tab_Button3);
        T3TabButton4 = (TextView) findViewById(R.id.T3_Tab_Buttom4);
        T3TabButton5 = (TextView) findViewById(R.id.T3_Tab_Button5);
        T3TabButton6 = (TextView) findViewById(R.id.T3_Tab_Button6);
        textView7 = (TextView) findViewById(R.id.textView7);
        editTextTextPersonName = (EditText) findViewById(R.id.editTextTextPersonName);
        T3ButtonC = (TextView) findViewById(R.id.T3_button_c);

        T3TabButton.setOnClickListener(view -> {
            T3TabButton.setTextColor(getColor(android.R.color.black));
            T3TabButton2.setTextColor(getColor(android.R.color.white));
            T3TabButton3.setTextColor(getColor(android.R.color.white));
            T3TabButton4.setTextColor(getColor(android.R.color.white));
            T3TabButton5.setTextColor(getColor(android.R.color.white));
            T3TabButton6.setTextColor(getColor(android.R.color.white));
        });
        T3TabButton2.setOnClickListener(view -> {
            T3TabButton2.setTextColor(getColor(android.R.color.black));
            T3TabButton.setTextColor(getColor(android.R.color.white));
            T3TabButton3.setTextColor(getColor(android.R.color.white));
            T3TabButton4.setTextColor(getColor(android.R.color.white));
            T3TabButton5.setTextColor(getColor(android.R.color.white));
            T3TabButton6.setTextColor(getColor(android.R.color.white));
        });
        T3TabButton3.setOnClickListener(view -> {
            T3TabButton3.setTextColor(getColor(android.R.color.black));
            T3TabButton2.setTextColor(getColor(android.R.color.white));
            T3TabButton.setTextColor(getColor(android.R.color.white));
            T3TabButton4.setTextColor(getColor(android.R.color.white));
            T3TabButton5.setTextColor(getColor(android.R.color.white));
            T3TabButton6.setTextColor(getColor(android.R.color.white));
        });
        T3TabButton4.setOnClickListener(view -> {
            T3TabButton4.setTextColor(getColor(android.R.color.black));
            T3TabButton2.setTextColor(getColor(android.R.color.white));
            T3TabButton3.setTextColor(getColor(android.R.color.white));
            T3TabButton.setTextColor(getColor(android.R.color.white));
            T3TabButton5.setTextColor(getColor(android.R.color.white));
            T3TabButton6.setTextColor(getColor(android.R.color.white));
        });
        T3TabButton5.setOnClickListener(view -> {
            T3TabButton5.setTextColor(getColor(android.R.color.black));
            T3TabButton2.setTextColor(getColor(android.R.color.white));
            T3TabButton3.setTextColor(getColor(android.R.color.white));
            T3TabButton4.setTextColor(getColor(android.R.color.white));
            T3TabButton.setTextColor(getColor(android.R.color.white));
            T3TabButton6.setTextColor(getColor(android.R.color.white));
        });
        T3TabButton6.setOnClickListener(view -> {
            T3TabButton6.setTextColor(getColor(android.R.color.black));
            T3TabButton2.setTextColor(getColor(android.R.color.white));
            T3TabButton3.setTextColor(getColor(android.R.color.white));
            T3TabButton4.setTextColor(getColor(android.R.color.white));
            T3TabButton5.setTextColor(getColor(android.R.color.white));
            T3TabButton.setTextColor(getColor(android.R.color.white));
        });


        editTextTextPersonName.setSingleLine(false);

        T3ButtonC.setOnClickListener(view -> {
            Toast.makeText(this,"保存成功",Toast.LENGTH_SHORT).show();
            finish();
        });
    }



}