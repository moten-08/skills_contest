package com.example.smartcity.ui.myself

import android.os.Bundle
import android.widget.TextView
import androidx.lifecycle.MutableLiveData
import com.example.smartcity.R
import com.example.smartcity.result.OrderInfo
import com.example.smartcity.tools.BaseActivity
import com.example.smartcity.tools.HttpUtil
import com.example.smartcity.tools.MyCallback
import com.example.smartcity.tools.toast

class OrderInfoActivity : BaseActivity() {

    private val orderInfo = MutableLiveData<OrderInfo>()
    private val noT: TextView by lazy { findViewById(R.id.no) }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_oi)

        val no = intent.getStringExtra("orderNo")
        title = "订单详情"
        getOrderInfo(no ?: "0")
        orderInfo.observe(this) {
            it.msg.toast(this)
            if (it.code == 200) noT.text = "$no\n${it.data.sellerInfo.name}"
        }

    }

    private fun getOrderInfo(orderNo: String) {
        HttpUtil.service.get(
            "api/takeout/order/$orderNo", token
        ).enqueue(MyCallback {
            orderInfo.value = HttpUtil.jsonToModel(it, OrderInfo::class.java)
        })
    }
}
