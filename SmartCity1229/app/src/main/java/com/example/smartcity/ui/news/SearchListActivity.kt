package com.example.smartcity.ui.news

import android.os.Bundle
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.smartcity.R
import com.example.smartcity.tools.*
import com.example.smartcity.ui.home.HomeViewModel
import kotlinx.android.synthetic.main.item_list.*


/**
 * 搜索结果
 */
class SearchListActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.item_list)

        val t = intent.getStringExtra("title") ?: ""
        title = "\"$t\"的搜索结果"

        val viewModel = ViewModelProviders.of(this).get(HomeViewModel::class.java)
        viewModel.apply {
            getNews(title = t)
            newsLive.observe(this@SearchListActivity) {
                itemList.apply {
                    layoutManager = LinearLayoutManager(this@SearchListActivity)
                    adapter = newsRxAdapter(it.rows)
                }
            }
        }
    }
}