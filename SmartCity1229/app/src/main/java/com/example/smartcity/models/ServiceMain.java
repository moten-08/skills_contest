package com.example.smartcity.models;

import java.util.List;

public class ServiceMain {

    public int total;
    public List<Rows> rows;
    public int code;
    public String msg;

    public static class Rows {
        public Object searchValue;
        public Object createBy;
        public String createTime;
        public Object updateBy;
        public String updateTime;
        public Object remark;
        public Params params;
        public int id;
        public String serviceName;
        public String serviceDesc;
        public String serviceType;
        public Object imgUrl;
        public int pid;
        public String link;
        public int sort;
        public String isRecommend;

        public static class Params {
        }
    }
}
