package com.example.smartcity.tools

import com.google.gson.Gson
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.toRequestBody
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object HttpUtil {
    private const val baseUrl1 = "http://10.10.230.112:10001"
    private const val baseUrl2 = "http://124.93.196.45:10001"
    const val baseUrl = baseUrl2
    private val retrofit: Retrofit = Retrofit.Builder()
        .baseUrl(baseUrl)
        .addConverterFactory(GsonConverterFactory.create())
        .build()
    private val GSON = Gson()
    val service: FitService = retrofit.create(FitService::class.java)
    private val MEDIA_TYPE = "application/json".toMediaTypeOrNull()
    private val MEDIA_FILE = "multipart/form-data".toMediaTypeOrNull()

    fun postTo(url: String, token: String = "", data: Any) =
        service.post(url, token, GSON.toJson(data).toRequestBody(MEDIA_TYPE))

    fun putTo(url: String, token: String, data: Any) =
        service.put(url, token, GSON.toJson(data).toRequestBody(MEDIA_TYPE))

    fun upload(token: String, byteArray: ByteArray) =
        service.upload(
            token, MultipartBody.Builder()
                .addFormDataPart("file", "file1219.jpeg", byteArray.toRequestBody(MEDIA_FILE))
                .build().parts
        )

    fun <T> jTM(data: Any, clazz: Class<T>): T = GSON.fromJson(GSON.toJson(data), clazz) // 啥
}