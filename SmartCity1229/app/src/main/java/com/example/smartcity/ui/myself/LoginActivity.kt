package com.example.smartcity.ui.myself

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import com.example.smartcity.R
import com.example.smartcity.models.LoginResult
import com.example.smartcity.models.request.LoginRequest
import com.example.smartcity.tools.*

class LoginActivity : BaseActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_login)

        val username = findViewById<EditText>(R.id.username)
        val password = findViewById<EditText>(R.id.password)
        val login = findViewById<Button>(R.id.login)
        val loading = findViewById<ProgressBar>(R.id.loading)


        login.setOnClickListener {
            loading.visibility = View.VISIBLE
            HttpUtil.postTo(
                "api/login", data = LoginRequest(username.toText(), password.toText())
            ).enqueue(MyCallback {
                loading.visibility = View.GONE
                with(HttpUtil.jTM(it, LoginResult::class.java)) {
                    msg.toast(this@LoginActivity)
                    if (code == 200) {
                        sp.open {
                            putString("token", this@with.token)
                        }
                        finish()
                    }
                }
            })
        }

    }
}

