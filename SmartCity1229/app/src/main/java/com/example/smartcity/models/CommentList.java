package com.example.smartcity.models;

import java.util.List;

public class CommentList {

    public int total;
    public List<RowsDTO> rows;
    public int code;
    public String msg;

    public static class RowsDTO {
        public Object searchValue;
        public Object createBy;
        public Object createTime;
        public Object updateBy;
        public Object updateTime;
        public Object remark;
        public ParamsDTO params;
        public int id;
        public String appType;
        public int newsId;
        public String content;
        public String commentDate;
        public int userId;
        public int likeNum;
        public String userName;
        public String newsTitle;

        public static class ParamsDTO {
        }
    }
}
