package com.example.smartcity.models;

public class UserInfo {

    public String msg;
    public int code;
    public User user;

    public static class User {
        public int userId;
        public String userName;
        public String nickName;
        public String email;
        public String phonenumber;
        public String sex;
        public String avatar;
        public String idCard;
        public double balance;
        public int score;
    }
}
