package com.example.smartcity.ui.xx

import com.example.smartcity.ui.xx.XxViewModel
import android.view.LayoutInflater
import android.view.ViewGroup
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.example.smartcity.R
import androidx.lifecycle.ViewModelProviders
import com.example.smartcity.ui.xx.XxFragment

class XxFragment : Fragment() {
    private var mViewModel: XxViewModel? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.xx_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mViewModel = ViewModelProviders.of(this).get(XxViewModel::class.java)
        // TODO: Use the ViewModel
    }

    companion object {
        fun newInstance(): XxFragment {
            return XxFragment()
        }
    }
}