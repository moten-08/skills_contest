package com.example.smartcity.tools

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import com.bumptech.glide.Glide
import com.example.smartcity.R
import com.example.smartcity.models.NewsModel
import com.example.smartcity.ui.news.NewsInfoActivity
import com.google.android.material.snackbar.Snackbar
import java.util.regex.Pattern

fun String.toast(context: Context) = Toast.makeText(context, this, Toast.LENGTH_SHORT).show()

fun String.snackbar(view: View, action: String = "", block: () -> Unit = {}) {
    Snackbar.make(view, this, Snackbar.LENGTH_SHORT)
        .setAction(action) { block() }
        .show()
}

fun ViewPager.pagerAdapter(viewList: List<View>) {
    this.adapter = object : PagerAdapter() {
        override fun getCount(): Int = viewList.size

        override fun isViewFromObject(view: View, `object`: Any): Boolean =
            `object` == view

        override fun instantiateItem(container: ViewGroup, position: Int): Any {
            val view = viewList[position]
            container.addView(view)
            return view
        }

        override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
            container.removeView(viewList[position])
        }
    }
}

fun SharedPreferences.open(block: SharedPreferences.Editor.() -> Unit) {
    val edit = this.edit()
    edit.block()
    edit.apply()
}

fun TextView.toText(): String = this.text.toString()

fun ImageView.glide(url: Any, local: Boolean = false, circle: Boolean = false) {
    if (circle) {
        Glide.with(this.context)
            .load(if (local) url else HttpUtil.baseUrl + url)
            .circleCrop()
            .into(this)
    } else {
        Glide.with(this.context)
            .load(if (local) url else HttpUtil.baseUrl + url)
            .into(this)
    }
}

fun String.removeHTML() =
    Pattern.compile("<[^>]+>")
        .matcher(this)
        .replaceAll("")
        .replace("&nbsp;", "\n")
        .replace("&ensp;", " ")
        .replace("&emsp;", "")

fun pagerAdapter(views: List<View>) = object : PagerAdapter() {
    override fun getCount(): Int = views.size

    override fun isViewFromObject(view: View, `object`: Any): Boolean =
        `object` == view

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val view = views[position]
        container.addView(view)
        return view
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(views[position])
    }
}

fun Context.newsRxAdapter(it: List<NewsModel.Rows>) =
    RxAdapter(this, R.layout.item_news, it) { r, p ->

        val itemNewsCover: ImageView = r.findViewById(R.id.itemNewsCover)
        val itemNewsTitle: TextView = r.findViewById(R.id.itemNewsTitle)
        val itemNewsContent: TextView = r.findViewById(R.id.itemNewsContent)
        val itemPublishDate: TextView = r.findViewById(R.id.itemPublishDate)
        val itemCommentNum: TextView = r.findViewById(R.id.itemCommentNum)
        val itemLikeNumber: TextView = r.findViewById(R.id.itemLikeNumber)
        val itemReadNumber: TextView = r.findViewById(R.id.itemReadNumber)

        itemNewsCover.glide(it[p].cover ?: "")
        itemNewsTitle.text = it[p].title
        itemNewsContent.text = it[p].content.removeHTML()
        itemPublishDate.text = String.format("发布时间:%s", it[p].publishDate)
        itemCommentNum.text = String.format("评论总数:%s", it[p].commentNum)
        itemLikeNumber.text = String.format("点赞人数:%s", it[p].likeNum)
        itemReadNumber.text = String.format("观看人数:%s", it[p].readNum)

        r.setOnClickListener {_->
            Intent(this, NewsInfoActivity::class.java).apply {
                putExtra("id", it[p].id)
                startActivity(this)
            }
        }
    }