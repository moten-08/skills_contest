package com.example.smartcity.ui.news

import android.app.AlertDialog
import android.content.ClipData
import android.content.ClipboardManager
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.smartcity.R
import com.example.smartcity.models.CommentList
import com.example.smartcity.tools.*
import kotlinx.android.synthetic.main.item_list.*


class CommentListActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.item_list)

        val id = intent.getIntExtra("newsId", 0)
        title = "评论列表"

        itemList.apply {
            layoutManager = LinearLayoutManager(this@CommentListActivity)
            getComments(id)
        }
    }

    private fun RecyclerView.getComments(id: Int) {
        HttpUtil.service.get(
            "press/comments/list", map = mapOf("newsId" to id.toString())
        ).enqueue(MyCallback {
            val data = HttpUtil.jTM(it, CommentList::class.java)
            if (data.code != 200) {
                "出错了".snackbar(itemList, "查看详情") { dialog(data) }
                return@MyCallback
            }
            if (data.total > 0) {
                val list = data.rows
                this.adapter = RxAdapter(
                    this@CommentListActivity,
                    R.layout.item_comment, list
                ) { r, p ->
                    val commentCover: ImageView = r.findViewById(R.id.commentCover)
                    val userName: TextView = r.findViewById(R.id.userName)
                    val commentContent: TextView = r.findViewById(R.id.commentContent)
                    val commentDate: TextView = r.findViewById(R.id.commentDate)

                    commentCover.glide(R.mipmap.person, local = true, circle = true)
                    userName.text = list[p].userName
                    commentContent.text = list[p].content.removeHTML()
                    commentDate.text = list[p].commentDate
                }
            } else {
                "暂无评论".toast(this@CommentListActivity)
            }

        })
    }

    private fun dialog(data: CommentList) =
        AlertDialog.Builder(this@CommentListActivity)
            .setTitle("报错信息")
            .setView(TextView(this@CommentListActivity).apply {
                text = data.msg
                setTextIsSelectable(true)
            })
            .setNegativeButton("取消") { v, _ -> v.dismiss() }
            .setNeutralButton("copy") { _, _ ->
                (getSystemService(CLIPBOARD_SERVICE) as ClipboardManager).setPrimaryClip(
                    ClipData.newPlainText("label", data.msg)
                )
            }
            .setPositiveButton("确认") { v, _ -> v.dismiss() }
            .create()
            .show()

}