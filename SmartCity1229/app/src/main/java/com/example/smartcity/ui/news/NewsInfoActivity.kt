package com.example.smartcity.ui.news

import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.smartcity.R
import com.example.smartcity.models.BaseResult
import com.example.smartcity.models.NewsInfo
import com.example.smartcity.models.NewsModel
import com.example.smartcity.models.request.AddComment
import com.example.smartcity.tools.*
import com.example.smartcity.ui.myself.LoginActivity
import kotlinx.android.synthetic.main.activity_news_info.*

class NewsInfoActivity : BaseActivity() {
    private val newsId by lazy { intent.getIntExtra("id", 0) }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_news_info)


        getNews(newsId)
        topNews.apply {
            layoutManager = LinearLayoutManager(this@NewsInfoActivity)
            rxAdapter()
        }
        addCommentBtn.setOnClickListener {
            val comment = addCommentEd.toText()
            if (comment.trim().isEmpty()) {
                "内容不能为空".toast(this)
                return@setOnClickListener
            }
            addComment(comment, token)
        }
        seeComment.setOnClickListener {
            Intent(this@NewsInfoActivity, CommentListActivity::class.java).apply {
                putExtra("newsId", newsId)
                startActivity(this)
            }
        }
    }

    private fun addComment(comment: String, token: String) {
        HttpUtil.postTo(
            "press/pressComment", token, AddComment(newsId, comment)
        ).enqueue(MyCallback {
            HttpUtil.jTM(it, BaseResult::class.java).apply {
                if (code == 200) {
                    msg.snackbar(addCommentBtn, "查看评论列表") {
                        Intent(this@NewsInfoActivity, CommentListActivity::class.java).apply {
                            putExtra("newsId", newsId)
                            startActivity(this)
                        }
                    }
                    addCommentEd.text.clear()

                } else {
                    "未登录或登录失效".snackbar(seeComment, "去登录") {
                        Intent(this@NewsInfoActivity, LoginActivity::class.java).apply {
                            startActivity(this)
                        }
                    }
                    msg.toast(this@NewsInfoActivity)
                }
            }
        })
    }

    private fun RecyclerView.rxAdapter() {
        HttpUtil.service.get(
            "press/press/list"
        ).enqueue(MyCallback {
            val list = HttpUtil.jTM(it, NewsModel::class.java).rows
            val listR = ArrayList<NewsModel.Rows>()
            repeat(3) {
                listR.add(list.random())
            }
            adapter = this@NewsInfoActivity.newsRxAdapter(listR)
        })
    }

    private fun getNews(newsId: Int) {
        HttpUtil.service.get(
            "press/press/$newsId"
        ).enqueue(MyCallback {
            (HttpUtil.jTM(it, NewsInfo::class.java).data).let { data ->
                title = data.title
                newsCover.glide(data.cover ?: "")
                val html = data.content
                    .replace("/prod-api", "${HttpUtil.baseUrl}/prod-api")
                    .replace("<img", "<img style=\"max-width:100%;height:auto;\"")
                newsContent.loadData(html, "text/html", "utf-8")
            }
        })
    }
}