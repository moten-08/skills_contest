package com.example.smartcity.ui.home

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import com.example.smartcity.MainActivity
import com.example.smartcity.R
import com.example.smartcity.models.BannerMain
import com.example.smartcity.tools.RxAdapter
import com.example.smartcity.tools.glide
import com.example.smartcity.tools.toText
import com.example.smartcity.tools.toast
import com.example.smartcity.ui.news.NewsInfoActivity
import com.example.smartcity.ui.news.SearchListActivity
import com.youth.banner.loader.ImageLoader
import kotlinx.android.synthetic.main.fragment_home.*

class HomeFragment : Fragment() {

    private lateinit var homeViewModel: HomeViewModel
    private var serviceNumber = 5
    private var themeNumber = 2

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        homeViewModel =
            ViewModelProviders.of(this).get(HomeViewModel::class.java)
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        with(homeViewModel) {
            banner()
            services()
            hotTheme()
            search()
        }

    }

    private fun search() {
        searchMain.setOnEditorActionListener { v, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                if (v.toText().trim().isEmpty()) "搜索内容不能为空".toast(requireContext())
                else Intent(requireContext(), SearchListActivity::class.java).apply {
                    putExtra("title", v.toText())
                    startActivity(this)
                }
            }
            true
        }
    }

    private fun HomeViewModel.hotTheme() {
        newsLive.observe(viewLifecycleOwner) {
            themeMain.apply {
                layoutManager = GridLayoutManager(requireContext(), themeNumber)
                adapter = RxAdapter(requireContext(), R.layout.item_theme, it.rows) { r, p ->
                    r.findViewById<ImageView>(R.id.itemTImg).glide(it.rows[p].cover)
                    r.findViewById<TextView>(R.id.itemTTitle).text = it.rows[p].title
                    r.setOnClickListener { _ -> toNewsInfo(it.rows[p].id) }
                }
            }
        }
        getNews(hot = "Y")
    }

    private fun toNewsInfo(p: Int) {
        Intent(requireContext(), NewsInfoActivity::class.java).apply {
            putExtra("id", p)
            startActivity(this)
        }
    }

    private fun HomeViewModel.services() {
        serviceLive.observe(viewLifecycleOwner) {
            servicesMain.apply {
                layoutManager = GridLayoutManager(requireContext(), serviceNumber)
                it.rows.subList(0, 10).apply {
                    get(9).serviceName = "更多服务"
                    get(9).imgUrl = R.mipmap.all_service
                }.let { rows ->
                    adapter = RxAdapter(requireContext(), R.layout.item_service, rows) { r, p ->
                        r.findViewById<ImageView>(R.id.itemSImg)
                            .glide(rows[p].imgUrl, local = (p == 9))
                        r.findViewById<TextView>(R.id.itemSText).text = rows[p].serviceName
                        if (p == 9) {
                            r.setOnClickListener {
                                (requireActivity() as MainActivity).switch()
                            }
                        }
                    }
                }

            }
        }
        getService()
    }

    private fun HomeViewModel.banner() {
        bannerLive.observe(viewLifecycleOwner) {
            bannerMain.apply {
                setImages(it.rows)
                setImageLoader(imageLoader())
                start()
            }
        }
        getBanner()
    }

    private fun imageLoader() = object : ImageLoader() {
        override fun displayImage(p0: Context, p1: Any, p2: ImageView) {
            with(p1 as BannerMain.Rows) {
                p2.glide(advImg)
                p2.setOnClickListener { toNewsInfo(id) }
            }
        }
    }
}