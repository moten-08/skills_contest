package com.example.smartcity.models;

public class NewsInfo {

    public String msg;
    public int code;
    public Data data;

    public static class Data {
        public Object searchValue;
        public String createBy;
        public String createTime;
        public Object updateBy;
        public Object updateTime;
        public Object remark;
        public Params params;
        public int id;
        public String appType;
        public String cover;
        public String title;
        public Object subTitle;
        public String content;
        public String status;
        public String publishDate;
        public Object tags;
        public Object commentNum;
        public int likeNum;
        public int readNum;
        public String type;
        public String top;
        public String hot;

        public static class Params {
        }
    }
}
