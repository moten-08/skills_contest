package com.example.smartcity.ui.news

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.smartcity.R
import com.example.smartcity.models.NewsModel
import com.example.smartcity.models.NewsTypes
import com.example.smartcity.tools.HttpUtil
import com.example.smartcity.tools.MyCallback
import com.example.smartcity.tools.pagerAdapter
import com.example.smartcity.tools.newsRxAdapter
import kotlinx.android.synthetic.main.fragment_news.*

class NewsFragment : Fragment() {

    private val views = ArrayList<View>()
    private val typeLive = MutableLiveData<NewsTypes>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_news, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getNewsTitles()
        typeLive.observe(viewLifecycleOwner) {
            it.data.forEach { _ -> views.add(View.inflate(requireContext(), R.layout.item_list, null)) }
            viewPager.adapter = pagerAdapter(views)
            tabNews.setupWithViewPager(viewPager)
            it.data.forEachIndexed { index, data ->
                tabNews.getTabAt(index)?.text = data.name
                getNewsList(data.id.toString(), views[index].findViewById(R.id.itemList))
            }
        }
    }

    private fun getNewsTitles() = HttpUtil.service.get(
        "press/category/list"
    ).enqueue(MyCallback {
        typeLive.value = HttpUtil.jTM(it, NewsTypes::class.java)
    })

    private fun getNewsList(type: String, recyclerView: RecyclerView) = HttpUtil.service.get(
        "press/press/list", map = mapOf("type" to type)
    ).enqueue(MyCallback {
        val data = HttpUtil.jTM(it, NewsModel::class.java)
        recyclerView.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = requireContext().newsRxAdapter(data.rows)
        }
    })
}