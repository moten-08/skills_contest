package com.example.smartcity.models;

import java.util.List;

public class BannerMain {

    public int total;
    public List<Rows> rows;
    public int code;
    public String msg;

    public static class Rows {
        public Object searchValue;
        public String createBy;
        public String createTime;
        public String updateBy;
        public String updateTime;
        public Object remark;
        public Params params;
        public int id;
        public String appType;
        public String status;
        public int sort;
        public String advTitle;
        public String advImg;
        public String servModule;
        public int targetId;
        public String type;

        public static class Params {
        }
    }
}
