package com.example.smartcity.ui

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import com.example.smartcity.MainActivity
import com.example.smartcity.R
import com.example.smartcity.tools.*


class GuideActivity : AppCompatActivity() {

    private val vpG: ViewPager by lazy { findViewById(R.id.vpG) }
    private val rvG: RecyclerView by lazy { findViewById(R.id.rvG) }
    private val buttonG: Button by lazy { findViewById(R.id.buttonG) }
    private val textG: TextView by lazy { findViewById(R.id.textG) }

    private val sp by lazy { getSharedPreferences("data", MODE_PRIVATE) }
    private val notFirst by lazy { sp.getBoolean("notFirst", false) }

    private val index = MutableLiveData<Int>().apply { value = 0 }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_guide)

        if (notFirst) {
            startActivity(Intent(this, MainActivity::class.java))
            finish()
            return
        }

        val guides = listOf(
            R.mipmap.guide1,
            R.mipmap.guide2,
            R.mipmap.guide3,
            R.mipmap.guide4,
            R.mipmap.guide5,
        )
        val viewList = ArrayList<View>()

        guides.forEach {
            val view = ImageView(this)
            view.setImageResource(it)
            view.scaleType = ImageView.ScaleType.CENTER_CROP
            viewList.add(view)
        }

        vpG.pagerAdapter(viewList)
        vpG.addOnPageChangeListener(onPageChangeListener())

        setData(guides)

        rvG.layoutManager = LinearLayoutManager(this@GuideActivity, RecyclerView.HORIZONTAL, false)

        val view = View.inflate(this, R.layout.dialog_g, null)

        val dialog = AlertDialog.Builder(this)
            .setTitle("网络设置")
            .setView(view)
            .create()
        dialog.dialog(view)
        textG.setOnClickListener { dialog.show() }
        buttonG.setOnClickListener {
            val ip = sp.getString("ip", "") ?: ""
            val port = sp.getString("port", "") ?: ""
            Log.d("TAG", "$ip:$port")
            if (ip.trim().isEmpty()) {
                "IP地址未设置".snackbar(rvG, "去设置", dialog::show)
                return@setOnClickListener
            }
            if (port.trim().isEmpty()) {
                "端口未设置".snackbar(rvG, "去设置", dialog::show)
                return@setOnClickListener
            }
            sp.open {
                putBoolean("notFirst", true)
            }
            startActivity(Intent(this, MainActivity::class.java))
            finish()
        }
    }

    private fun Dialog.dialog(view: View) {
        val ipEd = view.findViewById(R.id.ipEd) as EditText
        val portEd = view.findViewById(R.id.portEd) as EditText
        val cancelD = view.findViewById(R.id.cancelD) as Button
        val saveD = view.findViewById(R.id.saveD) as Button

        val ip = sp.getString("ip", "") ?: ""
        val port = sp.getString("port", "") ?: ""

        ipEd.setText(ip)
        portEd.setText(port)
        saveD.setOnClickListener {
            sp.open {
                putString("ip", ipEd.toText())
                putString("port", portEd.toText())
            }
            this.dismiss()
        }
        cancelD.setOnClickListener { this.dismiss() }
    }

    private fun setData(guides: List<Int>) {
        index.observe(this) {
            buttonG.visibility = if (it != (guides.size - 1)) View.GONE else View.VISIBLE
            textG.visibility = if (it != (guides.size - 1)) View.GONE else View.VISIBLE
            rvG.adapter = RxAdapter(this@GuideActivity, R.layout.item_p, guides) { r, p ->
                r.findViewById<ImageView>(R.id.itemP).apply {
                    setImageResource(
                        if (p == it) R.drawable.ic_baseline_fiber_manual_record_24
                        else R.drawable.ic_baseline_fiber_manual_record_25
                    )
                    setOnClickListener { vpG.currentItem = p }
                }
            }
        }
    }

    private fun onPageChangeListener() = object : ViewPager.OnPageChangeListener {
        override fun onPageScrolled(
            position: Int,
            positionOffset: Float,
            positionOffsetPixels: Int
        ) = Unit

        override fun onPageSelected(position: Int) {
            index.value = position
        }

        override fun onPageScrollStateChanged(state: Int) = Unit
    }
}