package com.example.smartcity.ui.home

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.smartcity.models.BannerMain
import com.example.smartcity.models.NewsModel
import com.example.smartcity.models.ServiceMain
import com.example.smartcity.tools.HttpUtil
import com.example.smartcity.tools.MyCallback

class HomeViewModel : ViewModel() {
    val bannerLive = MutableLiveData<BannerMain>()
    fun getBanner() = HttpUtil.service.get(
        "api/rotation/list", map = mapOf("type" to "2")
    ).enqueue(MyCallback {
        bannerLive.value = HttpUtil.jTM(it, BannerMain::class.java)
    })

    val serviceLive = MutableLiveData<ServiceMain>()
    fun getService() = HttpUtil.service.get(
        "api/service/list"
    ).enqueue(MyCallback {
        serviceLive.value = HttpUtil.jTM(it, ServiceMain::class.java)
    })

    val newsLive = MutableLiveData<NewsModel>()
    fun getNews(hot: String = "", top: String = "", title: String = "") = HttpUtil.service.get(
        "press/press/list", map = mapOf("top" to top, "hot" to hot, "title" to title)
    ).enqueue(MyCallback {
        newsLive.value = HttpUtil.jTM(it, NewsModel::class.java)
    })
}