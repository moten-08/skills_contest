package com.example.smartcity.models;

public class LoginResult {

    public String msg;
    public int code;
    public String token;
}
