package com.example.smartcity.tools

import android.util.Log
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MyCallback(private val block:(body:Any)->Unit):Callback<Any> {
    override fun onResponse(p0: Call<Any>, p1: Response<Any>) {
        val body = p1.body()
        if(body == null) Log.e("ERROR", "body is null")
        else block(body)
    }

    override fun onFailure(p0: Call<Any>, p1: Throwable)  = p1.printStackTrace()
}