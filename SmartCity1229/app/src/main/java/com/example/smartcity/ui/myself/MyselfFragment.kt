package com.example.smartcity.ui.myself

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.example.smartcity.R

class MyselfFragment : Fragment() {

    private lateinit var viewModel: MyselfViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProviders.of(this).get(MyselfViewModel::class.java)
        return inflater.inflate(R.layout.fragment_myself, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val sp = requireContext().getSharedPreferences("data", Context.MODE_PRIVATE)
        val token = sp.getString("token", "") ?: ""
        viewModel.apply {
            getUserInfo(token)
            userInfoLive.observe(viewLifecycleOwner) {

            }
        }
    }

}