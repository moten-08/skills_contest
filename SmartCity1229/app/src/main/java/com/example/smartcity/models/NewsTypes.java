package com.example.smartcity.models;

import java.util.List;

public class NewsTypes {

    public String msg;
    public int code;
    public List<Data> data;

    public static class Data {
        public Object searchValue;
        public Object createBy;
        public Object createTime;
        public Object updateBy;
        public Object updateTime;
        public Object remark;
        public Params params;
        public int id;
        public String appType;
        public String name;
        public int sort;
        public String status;
        public Object parentId;

        public static class Params {
        }
    }
}
