package com.example.smartcity.models;

import java.util.List;

public class NewsModel {

    public int total;
    public List<Rows> rows;
    public int code;
    public String msg;

    public static class Rows {
        public Object searchValue;
        public String createBy;
        public String createTime;
        public String updateBy;
        public String updateTime;
        public Object remark;
        public Params params;
        public int id;
        public String appType;
        public String cover;
        public String title;
        public Object subTitle;
        public String content;
        public String status;
        public String publishDate;
        public Object tags;
        public int commentNum;
        public int likeNum;
        public int readNum;
        public String type;
        public String top;
        public String hot;

        public static class Params {
        }
    }
}
