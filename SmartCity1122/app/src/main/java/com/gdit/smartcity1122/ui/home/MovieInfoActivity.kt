package com.gdit.smartcity1122.ui.home

import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import com.gdit.smartcity1122.R
import com.gdit.smartcity1122.models.results.MovieInfo
import com.gdit.smartcity1122.tools.BaseActivity
import com.gdit.smartcity1122.tools.HttpUtil
import com.gdit.smartcity1122.tools.MyCallback

class MovieInfoActivity : BaseActivity() {
    private val movieData = MutableLiveData<MovieInfo.DataDTO>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie_info)

        getMovieData(intent.getIntExtra("movieId", 0))

        movieData.observe(this) {
            title = it.name
        }
    }

    private fun getMovieData(id: Int) {
        if (id == 0) {
            finish()
            Toast.makeText(applicationContext, "暂无信息", Toast.LENGTH_SHORT).show()
            return
        }
        HttpUtil.get(
            "/api/movie/film/detail/$id",
            callback = MyCallback {
                movieData.value = HttpUtil.jsonToModel(it, MovieInfo::class.java).data
            }
        )
    }
}