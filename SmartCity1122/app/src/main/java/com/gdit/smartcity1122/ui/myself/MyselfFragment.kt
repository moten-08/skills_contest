package com.gdit.smartcity1122.ui.myself

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.gdit.smartcity1122.R

class MyselfFragment : Fragment() {

    private val viewModel: MyselfViewModel by lazy { ViewModelProvider(this).get(MyselfViewModel::class.java) }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.myself_fragment, container, false)
    }

}