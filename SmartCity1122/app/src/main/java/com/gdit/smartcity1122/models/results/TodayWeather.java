package com.gdit.smartcity1122.models.results;

/**
 * 今日天气
 */
public class TodayWeather {

    public String msg;
    public int code;
    public DataDTO data;

    public static class DataDTO {
        public String maxTemperature;
        public String uv;
        public String minTemperature;
        public String temperature;
        public String weather;
        public String humidity;
        public String air;
        public String apparentTemperature;
        public String label;
        public int day;
    }
}
