package com.gdit.smartcity1122.models.results;

/**
 * 地理位置
 */
public class Location {

    public String msg;
    public int code;
    public DataDTO data;

    public static class DataDTO {
        public String area;
        public String province;
        public String city;
        public String location;
    }
}
