package com.gdit.smartcity1122.tools

import android.util.Log
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.google.gson.Gson
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.OkHttpClient
import okhttp3.RequestBody.Companion.toRequestBody
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object HttpUtil {
    private val client = OkHttpClient.Builder().build()
    private const val baseUrl = "http://124.93.196.45:10001"
//    private const val baseUrl = "http://10.10.230.112:10001"
    private val retrofit: Retrofit = Retrofit.Builder()
        .baseUrl(baseUrl)
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    val GSON = Gson()
    private val MEDIA_TYPE = "application/json".toMediaTypeOrNull()
    private const val TAG = "HttpUtil"
    private val service = retrofit.create(FitService::class.java)

    fun ImageView.glide(url:String){
        Glide.with(this.context).load(baseUrl+url).into(this)
    }

    fun get(
        url: String,
        token: String? = "",
        map: Map<String, String>? = mapOf(),
        callback: MyCallback
    ) {
        val result = service.getTo(url, token, map).enqueue(callback)
        Log.d(TAG, "get: ${url}/${map}:\n\t$result")
    }

    fun put(
        url: String,
        token: String? = "",
        map: Map<String, String>? = mapOf(),
        callback: MyCallback
    ) {
        val result = service.putTo(url, token, map).enqueue(callback)
        Log.d(TAG, "put: ${url}/${map}:\n\t$result")
    }

    fun post(
        url: String,
        token: String? = "",
        request: Any,
        callback: MyCallback
    ) {
        val body = GSON.toJson(request)
        val result = service.postTo(url, body.toRequestBody(MEDIA_TYPE), token).enqueue(callback)
        Log.d(TAG, "put: ${url}:\n\t$result")
    }

    fun delete(
        url: String,
        token: String? = "",
        map: Map<String, String>? = mapOf(),
        callback: MyCallback
    ) {
        val result = service.deleteTo(url, token, map).enqueue(callback)
        Log.d(TAG, "put: ${url}:\n\t$result")
    }


}