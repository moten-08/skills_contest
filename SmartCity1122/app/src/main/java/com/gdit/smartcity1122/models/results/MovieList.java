package com.gdit.smartcity1122.models.results;

import java.util.List;

/**
 * 电影列表
 */
public class MovieList {

    public int total;
    public List<RowsDTO> rows;
    public int code;
    public String msg;

    public static class RowsDTO {
        public Object searchValue;
        public Object createBy;
        public Object createTime;
        public Object updateBy;
        public Object updateTime;
        public Object remark;
        public ParamsDTO params;
        public int id;
        public String name;
        public String category;
        public String cover;
        public String playType;
        public int score;
        public int duration;
        public String playDate;
        public int likeNum;
        public int favoriteNum;
        public String language;
        public String introduction;
        public Object other;
        public String recommend;

        // 预告多一个视频，少的不解析，问题不大
        public String video;

        public static class ParamsDTO {
        }
    }
}
