package com.gdit.smartcity1122.ui.home

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import com.gdit.smartcity1122.R
import com.gdit.smartcity1122.databinding.FragmentHomeBinding
import com.gdit.smartcity1122.databinding.ItemServicesBinding
import com.gdit.smartcity1122.models.RotationModel
import com.gdit.smartcity1122.tools.HttpUtil.glide
import com.gdit.smartcity1122.tools.RvAdapter
import com.youth.banner.BannerConfig
import com.youth.banner.loader.ImageLoader

class HomeFragment : Fragment() {

    private lateinit var homeViewModel: HomeViewModel
    private var _binding: FragmentHomeBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        homeViewModel =
            ViewModelProvider(this).get(HomeViewModel::class.java)

        _binding = FragmentHomeBinding.inflate(inflater, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initData()
        dataBindingView()

    }

    private fun dataBindingView() {
        homeViewModel.bannerData.observe(viewLifecycleOwner) {
            binding.banner.apply {
                setImages(it)
                setImageLoader(object : ImageLoader() {
                    override fun displayImage(p0: Context, p1: Any, p2: ImageView) {
                        val dto = p1 as RotationModel.RowsDTO
                        p2.glide(dto.advImg)
                    }
                })
                setOnBannerListener { position ->
                    Log.d("TAG", "onViewCreated: $position")
                }
                setIndicatorGravity(BannerConfig.CENTER)
                start()
            }
        }
        homeViewModel.services.observe(viewLifecycleOwner) {
            binding.recycler.adapter =
                RvAdapter(it.subList(0,10), requireContext(), R.layout.item_services) { root, position ->
                    val rBinding: ItemServicesBinding = ItemServicesBinding.bind(root)
                    val data = it[position]
                    rBinding.apply {
                        if (position == 9) {
                            image.setImageResource(R.drawable.ic_more)
                            text.text = "全部服务"
                        } else {
                            image.glide(data.imgUrl)
                            text.text = data.serviceName
                        }
                    }
                }
        }
    }

    private fun initData() {
        binding.recycler.layoutManager = GridLayoutManager(requireContext(), 5)

        homeViewModel.apply {
            getBanner()
            getServices()
        }
    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}