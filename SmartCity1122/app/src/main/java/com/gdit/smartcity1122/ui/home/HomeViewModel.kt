package com.gdit.smartcity1122.ui.home

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.gdit.smartcity1122.models.RotationModel
import com.gdit.smartcity1122.models.ServicesModel
import com.gdit.smartcity1122.tools.HttpUtil
import com.gdit.smartcity1122.tools.MyCallback

class HomeViewModel : ViewModel() {

    private val _bannerData = MutableLiveData<List<RotationModel.RowsDTO>>()
    val bannerData: LiveData<List<RotationModel.RowsDTO>> = _bannerData
    fun getBanner() {
        HttpUtil.get(
            "/api/rotation/list",
            map = mapOf("type" to "2"),
            callback = MyCallback {
                val data = HttpUtil.GSON.fromJson(
                    HttpUtil.GSON.toJson(it),
                    RotationModel::class.java
                )
                _bannerData.value = data.rows
                Log.d("TAG", "getBanner: ${bannerData.value}")
            }
        )
    }

    private val _services: MutableLiveData<List<ServicesModel.RowsDTO>> = MutableLiveData()
    val services: MutableLiveData<List<ServicesModel.RowsDTO>> = _services
    fun getServices() {
        HttpUtil.get("/api/service/list",
            callback = MyCallback {
                val data = HttpUtil.GSON.fromJson(
                    HttpUtil.GSON.toJson(it), ServicesModel::class.java
                )
                _services.value = data.rows
            })
    }
}