package com.gdit.smartcity1122.tools

import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*

interface FitService {

    @Headers("Content-Type: application/json;charset=UTF-8")
    @POST("/prod-api/{url}")
    fun postTo(
        @Path("url") url: String,
        @Body body: RequestBody,
        @Header("Authorization") token: String? = ""
    ): Call<Any>

    @GET("/prod-api/{url}")
    fun getTo(
        @Path("url") url: String,
        @Header("Authorization") token: String? = "",
        @QueryMap map: Map<String, String>?
    ): Call<Any>

    @PUT("/prod-api/{url}")
    fun putTo(
        @Path("url") url: String,
        @Header("Authorization") token: String? = "",
        @QueryMap map: Map<String, String>?
    ): Call<Any>

    @DELETE("/prod-api/{url}")
    fun deleteTo(
        @Path("url") url: String,
        @Header("Authorization") token: String? = "",
        @QueryMap map: Map<String, String>?
    ): Call<Any>

}