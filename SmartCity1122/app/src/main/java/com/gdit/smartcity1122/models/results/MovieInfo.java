package com.gdit.smartcity1122.models.results;

/**
 * 影片详情
 */
public class MovieInfo {
    public String msg;
    public int code;
    public DataDTO data;

    public static class DataDTO {
        public Object searchValue;
        public Object createBy;
        public Object createTime;
        public Object updateBy;
        public Object updateTime;
        public Object remark;
        public ParamsDTO params;
        public int id;
        public String name;
        public String category;
        public String cover;
        public String playType;
        public int score;
        public int duration;
        public String playDate;
        public int likeNum;
        public int favoriteNum;
        public String language;
        public String introduction;
        public Object other;
        public String recommend;

        public static class ParamsDTO {
        }
    }
}
