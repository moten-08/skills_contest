package com.gdit.smartcity1122.tools

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

class RvAdapter(
    private val list: List<Any>,
    val context: Context,
    private val lay: Int,
    val block: (root: View, position: Int) -> Unit
) : RecyclerView.Adapter<RvAdapter.ViewHolder>() {
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(LayoutInflater.from(context).inflate(lay, parent, false))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        block(holder.itemView, position)
    }

    override fun getItemCount(): Int = list.size
}