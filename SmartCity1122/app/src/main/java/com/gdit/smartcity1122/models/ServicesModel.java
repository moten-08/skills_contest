package com.gdit.smartcity1122.models;

import java.util.List;

public class ServicesModel {

    public int total;
    public List<RowsDTO> rows;
    public int code;
    public String msg;

    public static class RowsDTO {
        public Object searchValue;
        public Object createBy;
        public String createTime;
        public Object updateBy;
        public String updateTime;
        public Object remark;
        public ParamsDTO params;
        public int id;
        public String serviceName;
        public String serviceDesc;
        public String serviceType;
        public String imgUrl;
        public int pid;
        public String link;
        public int sort;
        public String isRecommend;

        public static class ParamsDTO {
        }
    }
}
